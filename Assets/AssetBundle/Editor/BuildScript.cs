﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine.UI;
using System.Reflection;
using System.Linq;
using Object = UnityEngine.Object;
using UnityEditor;

namespace AssetBundles
{
    public class BuildScript
    {
        [MenuItem("Assets/Change Asset bundle name")]
        public static void ChangeAssetBundleName()
        {
            string[] assets = AssetDatabase.GetAllAssetBundleNames();
            for (int i = 0; i < assets.Length; i++)
            {
                try
                {
                    int name = int.Parse(assets[i]);
                    Debug.Log(name);
                    foreach (var VARIABLE in AssetDatabase.GetAssetPathsFromAssetBundle(assets[i]))
                    {
                        Debug.Log(VARIABLE);
                        AssetImporter.GetAtPath(VARIABLE).SetAssetBundleNameAndVariant("GamePlayObject/" + name, "");
                    }

                }
                catch (Exception)
                {

                }
            }
        }
        public static string overloadedDevelopmentServerURL = "";

        public static void BuildAssetBundles()
        {
            //if (ClearLog() == false)
            //{
            //    return;
            //}
            string[] assets = AssetDatabase.GetAllAssetBundleNames();
            Dictionary<string, List<string>> depend = new Dictionary<string, List<string>>();
            foreach (string assetBundleName in assets)
            {

                depend.Add(assetBundleName, new List<string>(AssetDatabase.GetAssetPathsFromAssetBundle(assetBundleName)));
            }
            foreach (KeyValuePair<string, List<string>> item in depend)
            {
                foreach (string assetPath in item.Value)
                {
                    GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                    if (obj == null)
                    {
                        continue;
                    }

                    //List<ObjectLoadAssetbundle> newListFindWhenRun = new List<ObjectLoadAssetbundle>();
                    //getAssetInDontAutoLoadFolder(obj, ref newListFindWhenRun);
                    //AutoLoadAssetbundle oldListInGameObject = obj.GetComponent<AutoLoadAssetbundle>();
                    //if (oldListInGameObject != null)//check modifire
                    //{

                    //    if (newListFindWhenRun.Count > 0)
                    //    {
                    //        if (newListFindWhenRun.Count != oldListInGameObject.list.Count)
                    //        {
                    //            Object.DestroyImmediate(oldListInGameObject, true);
                    //            //AutoLoadAssetbundle tmpAutoLoad = obj.AddComponent<AutoLoadAssetbundle>();//thay the autoload script khac
                    //            tmpAutoLoad.list = newListFindWhenRun;
                    //        }
                    //        else
                    //        {
                    //            for (int i = 0; i < newListFindWhenRun.Count; i++)
                    //            {
                    //                if (newListFindWhenRun[i].Equals(oldListInGameObject.list[i]) == false)
                    //                {

                    //                    Object.DestroyImmediate(oldListInGameObject, true);
                    //                    AutoLoadAssetbundle tmpAutoLoad = obj.AddComponent<AutoLoadAssetbundle>();//thay the autoload script khac
                    //                    tmpAutoLoad.list = newListFindWhenRun;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        Object.DestroyImmediate(oldListInGameObject, true);
                    //    }
                    //}
                    //else //make new
                    //{
                    //    if (newListFindWhenRun.Count > 0)
                    //    {
                    //        oldListInGameObject = obj.AddComponent<AutoLoadAssetbundle>();
                    //        oldListInGameObject.list = newListFindWhenRun;
                    //    }
                    //}



                }
            }
            Debug.Log("Begin build");
            // Choose the output path according to the build target.
            string outputPath = Path.Combine(Utility.AssetBundlesOutputPath, Utility.GetPlatformName());
            if (!Directory.Exists(outputPath))
                Directory.CreateDirectory(outputPath);

            //@TODO: use append hash... (Make sure pipeline works correctly with it.)
            BuildPipeline.BuildAssetBundles(outputPath, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);

            string[] assetsName = new string[assets.Length + 1];
            for (int i = 0; i < assets.Length; i++)
            {
                assetsName[i] = assets[i];
            }
            assetsName[assets.Length] = Utility.GetPlatformName();

            checkBundleVersion(assetsName);
            //IOController.showFolder(outputPath);
        }

        public static void DeleteAssetbundle()
        {
            string[] assets = AssetDatabase.GetAllAssetBundleNames();
            foreach (string asset in assets)
            {
                foreach (string assetPath in AssetDatabase.GetAssetPathsFromAssetBundle(asset))
                {
                    GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                    if (obj != null)
                    {
                        deleteAllDepenesInGameObject(obj);
                    }
                    else
                    {
                        Debug.Log("delete " + assetPath);
                        AssetDatabase.DeleteAsset(assetPath);
                    }
                }
            }
        }
        private static void deleteAllDepenesInGameObject(GameObject obj)
        {
            foreach (var c in obj.GetComponents<Component>())
            {
                if (c != null)
                {
                    SerializedObject so = new SerializedObject(c);
                    var sp = so.GetIterator(); //tung field
                    while (sp.NextVisible(true) && sp.editable == true)
                    {
                        string assetPath = AssetDatabase.GetAssetPath(sp.objectReferenceInstanceIDValue);

                        string[] depend = AssetDatabase.GetDependencies(assetPath);
                        foreach (string dep in depend)
                        {
                            bool checkInEffect = dep.Contains("Assets/effect") == true && dep.Contains(".cs") == false && ((dep.Contains(".js") == true && dep.Contains(".json") == false) == false);
                            bool checkInResource = dep.Contains("Assets/Resources") == true && dep.Contains(".cs") == false && ((dep.Contains(".js") == true && dep.Contains(".json") == false) == false);
                            if (checkInEffect == true || checkInResource == true)
                            {
                                Debug.Log("delete " + dep);

                                AssetDatabase.DeleteAsset(dep);
                            }
                            else
                            {
                                Debug.LogError(checkInEffect + "    " + checkInResource + "khong delete: " + dep);
                            }
                        }

                    }
                }
            }
            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(obj.GetInstanceID()));
        }

        private static void checkBundleVersion(string[] assetbundlesName)
        {
            //string outputPath = Path.Combine(Utility.AssetBundlesOutputPath, Utility.GetPlatformName());
            //string outputChangedAsset = Path.Combine(Utility.AssetBundlesOutputChangedPath, Utility.GetPlatformName());

            //List<string> lstAssetChanged = new List<string>();
            //IOController.checkAndCreateFolder(outputChangedAsset);

            //if (Directory.Exists(outputPath) == true)
            //{
            //    string config = Path.Combine(outputPath, AssetbundleConfig.CONFIG_XML);
            //    Debug.Log("config: " + config);
            //    BundleVersionContainer currVersion = null;
            //    if (File.Exists(config) == true)
            //    {
            //        currVersion = BundleVersionContainer.Load(config);
            //    }
            //    List<BundleVersion> tmpList = new List<BundleVersion>();
            //    bool changed = false;
            //    if (currVersion == null)
            //    {
            //        changed = true;
            //        for (int i = 0; i < assetbundlesName.Length; i++)
            //        {
            //            coppyFileToChangedFolder(outputPath + "/" + assetbundlesName[i], outputChangedAsset + "/" + assetbundlesName[i], true, ref lstAssetChanged);
            //            coppyFileToChangedFolder(outputPath + "/" + assetbundlesName[i] + ".manifest", outputChangedAsset + "/" + assetbundlesName[i] + ".manifest", true, ref lstAssetChanged);
            //            long size = new FileInfo(Path.Combine(outputPath, assetbundlesName[i])).Length;
            //            tmpList.Add(new BundleVersion(assetbundlesName[i], 1.0f, checkSum(Path.Combine(outputPath, assetbundlesName[i])), size));
            //        }

            //        Debug.LogWarning("Create ============ NEW FILE  =============  config");
            //    }
            //    else
            //    {
            //        for (int i = 0; i < assetbundlesName.Length; i++)
            //        {
            //            bool found = false;
            //            string checksum = checkSum(Path.Combine(outputPath, assetbundlesName[i]));
            //            for (int j = 0; j < currVersion.bundleVersions.Count; j++)
            //            {
            //                if (currVersion.bundleVersions[j].name == assetbundlesName[i])
            //                {
            //                    found = true;
            //                    if (currVersion.bundleVersions[j].currHash == checksum)
            //                    {
            //                        long size = new FileInfo(Path.Combine(outputPath, assetbundlesName[i])).Length;
            //                        currVersion.bundleVersions[j].sizeOnDisk = size;
            //                        tmpList.Add(currVersion.bundleVersions[j]);

            //                    }
            //                    else
            //                    {


            //                        coppyFileToChangedFolder(outputPath + "/" + assetbundlesName[i], outputChangedAsset + "/" + assetbundlesName[i], true, ref lstAssetChanged);
            //                        coppyFileToChangedFolder(outputPath + "/" + assetbundlesName[i] + ".manifest", outputChangedAsset + "/" + assetbundlesName[i] + ".manifest", true, ref lstAssetChanged);

            //                        Debug.Log("Changed bundle: " + assetbundlesName[i] + " from version: " + currVersion.bundleVersions[j].version + " to version:" + ((float)(Mathf.RoundToInt((currVersion.bundleVersions[j].version + 0.1f) * 10)) / 10));
            //                        long size = new FileInfo(Path.Combine(outputPath, assetbundlesName[i])).Length;
            //                        tmpList.Add(new BundleVersion(assetbundlesName[i], (float)(Mathf.RoundToInt((currVersion.bundleVersions[j].version + 0.1f) * 10)) / 10, checksum, size));
            //                        changed = true;
            //                    }
            //                }
            //            }
            //            if (found == false)//khong tim thay bundle nay o file bundle config truoc thi add new
            //            {
            //                coppyFileToChangedFolder(outputPath + "/" + assetbundlesName[i], outputChangedAsset + "/" + assetbundlesName[i], true, ref lstAssetChanged);
            //                coppyFileToChangedFolder(outputPath + "/" + assetbundlesName[i] + ".manifest", outputChangedAsset + "/" + assetbundlesName[i] + ".manifest", true, ref lstAssetChanged);
            //                Debug.LogWarning("add new config bundle: " + assetbundlesName[i]);
            //                long size = new FileInfo(Path.Combine(outputPath, assetbundlesName[i])).Length;
            //                tmpList.Add(new BundleVersion(assetbundlesName[i], 1.0f, checkSum(Path.Combine(outputPath, assetbundlesName[i])), size));
            //                changed = true;
            //            }

            //        }
            //    }

            //    if (changed == true)
            //    {
            //        //File.Copy(outputPath + "/config.xml", outputChangedAsset + "/config.xml", true);


            //        currVersion = new BundleVersionContainer();
            //        currVersion.bundleVersions = tmpList;
            //        currVersion.Save(config);
            //        coppyFileToChangedFolder(outputPath + "/config.xml", outputChangedAsset + "/config.xml", true, ref lstAssetChanged);
            //        Debug.Log("Update file config");
            //    }

            //    //delete old file
            //    //foreach (var item in lstAssetChanged)
            //    //{
            //    //    Debug.Log("file change: " + item);

            //    //}
            //    for (int i = 0; i < lstAssetChanged.Count; i++)
            //    {
            //        lstAssetChanged[i] = Path.GetFullPath(lstAssetChanged[i]);
            //    }
            //    string[] files = IOController.getAllFileInDirectory(outputChangedAsset).ToArray();
            //    for (int i = files.Length - 1; i >= 0; i--)
            //    {
            //        if (lstAssetChanged.Contains(Path.GetFullPath(files[i])) == false)
            //        {
            //            Debug.Log("xoa file khong thay doi" + files[i]);
            //            File.Delete(files[i]);
            //        }
            //    }
            //}
        }
        private static void coppyFileToChangedFolder(string fileName, string dirCoppyTo, bool overwrite, ref List<string> lstAssetChanged)
        {
            //lstAssetChanged.Add(dirCoppyTo);
            //IOController.checkAndCreateFolder(Path.GetDirectoryName(dirCoppyTo));
            //Debug.Log(dirCoppyTo + "   " + fileName);
            //File.Copy(fileName, dirCoppyTo, overwrite);
        }
        private static string checkSum(string filename)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    return System.BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty).ToLower();
                }
            }
        }
        //note: check de qui con trong game object, component image va sprite
        //private static void getAssetInDontAutoLoadFolder(GameObject obj, ref List<ObjectLoadAssetbundle> tempList)
        //{
        //    var components = obj.GetComponents<Component>();
        //    foreach (var c in components)
        //    {
        //        if (c != null && (c.GetType() == typeof(SpriteRenderer) || c.GetType() == typeof(Image)))
        //        {
        //            SerializedObject so = new SerializedObject(c);
        //            var sp = so.GetIterator(); //tung field
        //            while (sp.NextVisible(true)/* && sp.editable == true*/)
        //            {
        //                //if (obj.name == "Card")
        //                //{
        //                //    Debug.Log("co ne troi " + obj.name + "   "  + AssetDatabase.GetAssetPath(sp.objectReferenceValue));
        //                //}
        //                //Debug.LogWarning(sp.type);
        //                if (sp.type.Contains("PPtr<") && AssetDatabase.GetAssetPath(sp.objectReferenceInstanceIDValue).Contains(AssetbundleConfig.DontAutoLoadFolder) == true)
        //                {

        //                    //Debug.Log("co ne troi 1 " + obj.name);
        //                    string path = AssetDatabase.GetAssetPath(sp.objectReferenceInstanceIDValue);

        //                    //this block check multiple sprite
        //                    {
        //                        path = path.Replace(AssetbundleConfig.ResoucePath + "/", string.Empty);
        //                        string[] arrs = path.Split('/');

        //                        string[] newName = arrs[arrs.Length - 1].Split('.');
        //                        string assetbundlePath = string.Empty;
        //                        for (int i = 0; i < arrs.Length - 1; i++)
        //                        {
        //                            assetbundlePath += (arrs[i] + "/");
        //                        }

        //                        assetbundlePath = assetbundlePath.Substring(0, assetbundlePath.Length - 1);
        //                        assetbundlePath = assetbundlePath.ToLower();

        //                        ObjectLoadAssetbundle.KeepType keepType = ObjectLoadAssetbundle.KeepType.Image;
        //                        if (c.GetType().Name == "Image")
        //                        {
        //                            keepType = ObjectLoadAssetbundle.KeepType.Image;
        //                        }
        //                        else if (c.GetType().Name == "SpriteRenderer")
        //                        {
        //                            keepType = ObjectLoadAssetbundle.KeepType.SpriteRenderer;
        //                        }
        //                        if (sp.objectReferenceValue != null)
        //                        {
        //                            if (newName[0] == sp.objectReferenceValue.name) // single sprite
        //                            {
        //                                ObjectLoadAssetbundle objectLoad = new ObjectLoadAssetbundle(keepType, assetbundlePath, newName[0], obj);
        //                                tempList.Add(objectLoad);
        //                            }
        //                            else//multiple sprite
        //                            {
        //                                ObjectLoadAssetbundle objectLoad = new ObjectLoadAssetbundle(keepType, assetbundlePath, newName[0], sp.objectReferenceValue.name, obj);
        //                                tempList.Add(objectLoad);
        //                            }
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    //de qui child
        //    if (obj.transform.childCount > 0)
        //    {
        //        for (int i = 0; i < obj.transform.childCount; i++)
        //        {
        //            getAssetInDontAutoLoadFolder(obj.transform.GetChild(i).gameObject, ref tempList);
        //        }
        //    }
        //}

        public static bool ClearLog()
        {
            var assembly = Assembly.GetAssembly(typeof(UnityEditor.ActiveEditorTracker));
            var type = assembly.GetType("UnityEditorInternal.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);

            int count = (int)type.GetMethod("GetCount").Invoke(new object(), null);

            if (count > 0)
            {
                Debug.LogError("**************************Cannot build asset because have compile errors************************");
                return false;
            }
            Debug.Log("Clear log sussec");
            return true;

        }
        public static void WriteServerURL()
        {
            string downloadURL;
            if (string.IsNullOrEmpty(overloadedDevelopmentServerURL) == false)
            {
                downloadURL = overloadedDevelopmentServerURL;
            }
            else
            {
                IPHostEntry host;
                string localIP = "";
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        localIP = ip.ToString();
                        break;
                    }
                }
                downloadURL = "http://" + localIP + ":7888/";
            }

            string assetBundleManagerResourcesDirectory = "Assets/AssetBundleManager/Resources";
            string assetBundleUrlPath = Path.Combine(assetBundleManagerResourcesDirectory, "AssetBundleServerURL.bytes");
            Directory.CreateDirectory(assetBundleManagerResourcesDirectory);
            File.WriteAllText(assetBundleUrlPath, downloadURL);
            AssetDatabase.Refresh();
        }

        public static void BuildPlayer()
        {
            var outputPath = EditorUtility.SaveFolderPanel("Choose Location of the Built Game", "", "");
            if (outputPath.Length == 0)
                return;

            string[] levels = GetLevelsFromBuildSettings();
            if (levels.Length == 0)
            {
                Debug.Log("Nothing to build.");
                return;
            }

            string targetName = GetBuildTargetName(EditorUserBuildSettings.activeBuildTarget);
            if (targetName == null)
                return;

            // Build and copy AssetBundles.
            BuildScript.BuildAssetBundles();
            WriteServerURL();

            BuildOptions option = EditorUserBuildSettings.development ? BuildOptions.Development : BuildOptions.None;
            BuildPipeline.BuildPlayer(levels, outputPath + targetName, EditorUserBuildSettings.activeBuildTarget, option);
        }

        public static void BuildStandalonePlayer()
        {
            var outputPath = EditorUtility.SaveFolderPanel("Choose Location of the Built Game", "", "");
            if (outputPath.Length == 0)
                return;

            string[] levels = GetLevelsFromBuildSettings();
            if (levels.Length == 0)
            {
                Debug.Log("Nothing to build.");
                return;
            }

            string targetName = GetBuildTargetName(EditorUserBuildSettings.activeBuildTarget);
            if (targetName == null)
                return;

            // Build and copy AssetBundles.
            BuildScript.BuildAssetBundles();
            BuildScript.CopyAssetBundlesTo(Path.Combine(Application.streamingAssetsPath, Utility.AssetBundlesOutputPath));
            AssetDatabase.Refresh();

            BuildOptions option = EditorUserBuildSettings.development ? BuildOptions.Development : BuildOptions.None;
            BuildPipeline.BuildPlayer(levels, outputPath + targetName, EditorUserBuildSettings.activeBuildTarget, option);
        }

        public static string GetBuildTargetName(BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.Android:
                    return "/test.apk";
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    return "/test.exe";
                case BuildTarget.WebGL:
                    return "";
                // Add more build targets for your own.
                default:
                    Debug.Log("Target not implemented.");
                    return null;
            }
        }

        static void CopyAssetBundlesTo(string outputPath)
        {
            // Clear streaming assets folder.
            FileUtil.DeleteFileOrDirectory(Application.streamingAssetsPath);
            Directory.CreateDirectory(outputPath);

            string outputFolder = Utility.GetPlatformName();

            // Setup the source folder for assetbundles.
            var source = Path.Combine(Path.Combine(System.Environment.CurrentDirectory, Utility.AssetBundlesOutputPath), outputFolder);
            if (!System.IO.Directory.Exists(source))
                Debug.Log("No assetBundle output folder, try to build the assetBundles first.");

            // Setup the destination folder for assetbundles.
            var destination = System.IO.Path.Combine(outputPath, outputFolder);
            if (System.IO.Directory.Exists(destination))
                FileUtil.DeleteFileOrDirectory(destination);

            FileUtil.CopyFileOrDirectory(source, destination);
        }

        static string[] GetLevelsFromBuildSettings()
        {
            List<string> levels = new List<string>();
            for (int i = 0; i < EditorBuildSettings.scenes.Length; ++i)
            {
                if (EditorBuildSettings.scenes[i].enabled)
                    levels.Add(EditorBuildSettings.scenes[i].path);
            }

            return levels.ToArray();
        }
    }
    public class Utility
    {
        public const string AssetBundlesOutputPath = "AssetBundles";
        public const string AssetBundlesOutputChangedPath = "AssetBundlesChanged";

        public static string GetPlatformName()
        {
#if UNITY_EDITOR_WIN || UNITY_EDITOR_OSX
            return GetPlatformForAssetBundles(EditorUserBuildSettings.activeBuildTarget);
#else
   return GetPlatformForAssetBundles(Application.platform);
#endif
        }

#if UNITY_EDITOR
        private static string GetPlatformForAssetBundles(BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.Android:
                    return "Android";
                case BuildTarget.iOS:
                    return "iOS";
                case BuildTarget.WebGL:
                    return "WebGL";
                case BuildTarget.StandaloneOSX:
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    return "Windows";
                // Add more build targets for your own.
                // If you add more targets, don't forget to add the same platforms to GetPlatformForAssetBundles(RuntimePlatform) function.
                default:
                    return null;
            }
        }
#endif

        private static string GetPlatformForAssetBundles(RuntimePlatform platform)
        {
            switch (platform)
            {
                case RuntimePlatform.Android:
                    return "Android";
                case RuntimePlatform.IPhonePlayer:
                    return "iOS";
                case RuntimePlatform.WebGLPlayer:
                    return "WebGL";
                case RuntimePlatform.WindowsPlayer:
                    return "Windows";
                case RuntimePlatform.OSXPlayer:
                    return "OSX";
                // Add more build targets for your own.
                // If you add more targets, don't forget to add the same platforms to GetPlatformForAssetBundles(RuntimePlatform) function.
                default:
                    return null;
            }
        }
    }
}
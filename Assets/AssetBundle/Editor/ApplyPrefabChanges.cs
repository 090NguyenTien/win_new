﻿using UnityEngine;
using UnityEditor;
using System.Linq;

public class ApplyPrefabChanges : EditorWindow
{
    [MenuItem("Tools/Apply Prefab Changes %#w")]
    static public void applyPrefabChange()
    {
        GameObject[] gameObjs = Selection.gameObjects;

        if (gameObjs.Length > 0)
        {
            for (int i = 0; i < gameObjs.Length; i++)
            {
                GameObject gameObj = gameObjs[i];

                var prefab_root = PrefabUtility.FindPrefabRoot(gameObj);
                var prefab_src = PrefabUtility.GetCorrespondingObjectFromSource(prefab_root);
                if (prefab_src != null)
                {
                    PrefabUtility.ReplacePrefab(prefab_root, prefab_src, ReplacePrefabOptions.ConnectToPrefab);
                    Debug.Log("Updating prefab : " + AssetDatabase.GetAssetPath(prefab_src));
                }
                else
                {
                    Debug.Log(gameObj.name + " has no prefab");
                }

            }
        }
        else
        {
            Debug.Log("Apply Prefab!  --------------------------   no Prefab Selected!!!!!!!!!!");
        }
        
        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoKhongXuatHienModule : MonoBehaviour {
    public Text Number_1_Txt, Number_2_Txt;
    private ItemNotAppearNumber item;
    public void Init(ItemNotAppearNumber item)
    {
        this.item = item;
    }
    public void Show(string num_1, string num_2)
    {       
        Number_1_Txt.text = num_1;
        Number_2_Txt.text = num_2;    
    }
}

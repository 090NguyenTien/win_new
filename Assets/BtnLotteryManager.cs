﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnLotteryManager : MonoBehaviour {

    private GameObject Notification;
    private Animator Anim;
    public bool IsHaveResult = false;

	// Use this for initialization
	void Start () {
       
	}

    private void Awake()
    {
        Notification = gameObject.transform.GetChild(0).gameObject;
        Anim = Notification.GetComponent<Animator>();
    }

    // Update is called once per frame
 //   void Update () {
 //       Anim.SetBool("Result", IsHaveResult);
	//}

    public void OnNotification()
    {
        IsHaveResult = true;
        Notification.SetActive(true);
    }

    public void OffNotification()
    {
        IsHaveResult = false;
        Notification.SetActive(false);
    }
}

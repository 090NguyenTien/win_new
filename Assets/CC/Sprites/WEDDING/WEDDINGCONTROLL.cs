﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class WEDDINGCONTROLL : MonoBehaviour
{
    [SerializeField]
    GameObject PanelGioiTinh, BtnNam, BtnNu;
    [SerializeField]
    Image ImgUser, ImgUser_PanneGioiTinh;
    [SerializeField]
    Sprite NhanVat_MacDinh, SprNam, SprNu, SprBtnChon, SprBtnAn;


    [SerializeField]
    GameObject PanelCauHon, Pan_DanhSachBanBe, Pan_ChonNhan, Pan_TamThu, Pan_XacNhan;
    [SerializeField]
    GameObject Content_DanhSachBanBe, ItemBanBeCauHon;
    [SerializeField]
    GameObject Content_ChonNhan, ItemNhan, imgAn;
    [SerializeField]
    Text TamThu;
    [SerializeField]
    Text XN_tamthu, XN_ChiPhi, XN_GuiToi, XN_PhiNhaHang, XN_PhiTong;
    [SerializeField]
    Image XN_imgNhan;
    //[SerializeField]
    //PopupAlertManager popupAlert;
    [SerializeField]
    GameObject Pan_XacNhan_2;
    [SerializeField]
    Text Txt_XacNhanCauHon;


    long NhaHang = 5000000000;


    [SerializeField]
    GameObject PanelThuNgo, Pan_DanhSachThu, Pan_Thu, Pan_ConFrim, Pan_XacNhan_DongY;
    [SerializeField]
    GameObject Content_DanhSachThu, ItemThuNgo;
    [SerializeField]
    Text ThuNgo;
    [SerializeField]
    GameObject ImgClock;
    [SerializeField]
    Image ImgNhan_;
    [SerializeField]
    Text XN_Cuoi_tamthu, XN_TxtNgCauHon;
    [SerializeField]
    Image XN_imgNhan_Cuoi;
    [SerializeField]
    Text TxtConfrim;
    [SerializeField]
    Button BtnConfrim, BtnBack_Confrim;
    [SerializeField]
    GameObject Pan_XacNhan_3;
    [SerializeField]
    Text Txt_XacNhan_DongY_3;




    string ID_QUANHE = "";
    string ID_CK, ID_VK, TEN_CK, TEN_VK, ID_NHAN_CUOI, TIN_NHAN;
    [SerializeField]
    Image NguoiTinh;
    [SerializeField]
    Text TxtTenUser, TxtTenNguoiTinh;
    [SerializeField]
    GameObject obj_LoiNguyetLao;
    [SerializeField]
    Text TxtNguyetLao;
    [SerializeField]
    Button BtnLyHon;
    [SerializeField]
    Button BtnCauHon;
    [SerializeField]
    GameObject ClockBtnCauHon, ClockBtnThuNgo;


    public int ID_GioiTinh;


    public void Init()
    {
        LayDuLieu_TinhTrangHonNhan();
        LayDuLieu_DanhSachNhan();

        //  LayDuLieu_DanhSach_BanBe();
    }

    public void Close_Wedding()
    {
        TxtNguyetLao.text = "";
        obj_LoiNguyetLao.SetActive(false);
        this.gameObject.SetActive(false);
        Destroy(gameObject);
    }

    #region GIOI TINH

    void checkGioiTinh()
    {
        if (ID_GioiTinh == 1) // nam
        {
            ImgUser.sprite = SprNam;
        }
        else if (ID_GioiTinh == 2) // nu
        {
            ImgUser.sprite = SprNu;
        }
        else
        {
            BtnNamClick();
            PanelGioiTinh.SetActive(true);
        }
        MyInfo.ID_GioiTinh = ID_GioiTinh;
    }

    void CheckGiaDinh()
    {
        if (ID_QUANHE != "")
        {
            BtnLyHon.gameObject.SetActive(true);
            Debug.LogError("------------------------");
            if (MyInfo.ID == ID_CK) // userr la chong
            {
                NguoiTinh.sprite = SprNu;
                TxtTenUser.text = TEN_CK;
                TxtTenNguoiTinh.text = TEN_VK;
            }
            else
            {
                NguoiTinh.sprite = SprNam;
                TxtTenUser.text = TEN_VK;
                TxtTenNguoiTinh.text = TEN_CK;
            }

            MyInfo.TEN_NGUOITINH = TxtTenNguoiTinh.text;

            NguoiTinh.gameObject.SetActive(true);
            TxtNguyetLao.text = "Hello hai vợ chồng " + MyInfo.NAME + " my name Nguyệt Lão\n hai bạn thật đẹp đôi.";
            obj_LoiNguyetLao.SetActive(true);
        }
        else
        {
            BtnLyHon.gameObject.SetActive(false);
            Debug.LogError("++++++++++++++++++++");
            NguoiTinh.gameObject.SetActive(false);
            TxtTenUser.text = MyInfo.NAME;
            TxtNguyetLao.text = "Hello " + MyInfo.NAME + " my name Nguyệt Lão\n ta sẽ se duyên cho bạn.";
            obj_LoiNguyetLao.SetActive(true);
        }
    }



    public void BtnNamClick()
    {
        BtnNam.GetComponent<Image>().sprite = SprBtnChon;
        BtnNu.GetComponent<Image>().sprite = SprBtnAn;
        ImgUser_PanneGioiTinh.sprite = SprNam;
        ImgUser.sprite = SprNam;
        ID_GioiTinh = 1;
    }

    public void BtnNuClick()
    {
        BtnNu.GetComponent<Image>().sprite = SprBtnChon;
        BtnNam.GetComponent<Image>().sprite = SprBtnAn;
        ImgUser_PanneGioiTinh.sprite = SprNu;
        ImgUser.sprite = SprNu;
        ID_GioiTinh = 2;
    }

    public void BtnXacNhanGioiTinhClick()
    {
        MyInfo.ID_GioiTinh = ID_GioiTinh;
        CapNhatGioiTinh();
        PanelGioiTinh.SetActive(false);
    }

    public void BtnGioiTinhClick()
    {
        if (ID_QUANHE == "")
        {
            if (ID_GioiTinh == 1)
            {
                BtnNamClick();
            }
            else if (ID_GioiTinh == 2)
            {
                BtnNuClick();
            }
            PanelGioiTinh.SetActive(true);
        }
        else
        {
            TxtNguyetLao.text = "Bạn đã có gia đình nên không thể thay đổi giới tính.";
            obj_LoiNguyetLao.SetActive(true);
            StartCoroutine(Wait_TatLoiNguyetLao(5f));
        }

    }

    #endregion

    #region CAU HON

    string ID_DOITUONG = "";
    string TEN_DOITUONG = "";

    string ID_CAUHON = "";
    string ID_NHAN = "";

    string TEN_NHAN = "";
    long GIA_NHAN = 0;
    string str_GIA_NHAN = "";
    string URL_NHAN = "";
    string TAMTHU = "";

    int SoLuongBanBe = 0;


    public Dictionary<int, ItemNhan> DicNhan = new Dictionary<int, ItemNhan>();

    //  public Dictionary<string, string> DicUrlNhan = new Dictionary<string, string>();


    public void Btn_CauHonClick()
    {
        Debug.LogError("Btn_CauHonClick ne " + ID_QUANHE);
        if (ID_QUANHE == "")
        {
            ClockBtnCauHon.SetActive(true);
            LayDuLieu_DanhSach_BanBe();

        }
        else
        {
            TxtNguyetLao.text = "Bạn đã có gia đình nên không thể cầu hôn người khác.";
            obj_LoiNguyetLao.SetActive(true);
            StartCoroutine(Wait_TatLoiNguyetLao(5f));
        }
    }




    IEnumerator Wait_TatLoiNguyetLao(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        TxtNguyetLao.text = "";
        obj_LoiNguyetLao.SetActive(false);
    }




    public void RspDanhSachBanBe(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.LogError("DU LIEU BAN BE  --- " + _json);
        BtnCauHon.enabled = true;
        int cn = Content_DanhSachBanBe.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = Content_DanhSachBanBe.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
        // LẤY DỮ LIỆU CẦU HÔN Ở ĐÂY NÈ

        int cou = node.Count;
        if (cou > 0)
        {
            for (int i = 0; i < cou; i++)
            {
                string id_CauHon = node[i]["_id"]["$id"].Value;
                string id_user = node[i]["user_id"]["$id"].Value;
                string ten = node[i]["user_name"].Value;
                InitItemCauHon(id_CauHon, id_user, ten);
            }
            Pan_DanhSachBanBe.SetActive(true);
            PanelCauHon.SetActive(true);
        }
        else
        {
            TxtNguyetLao.text = "Rât tiêc! Bạn chưa kết bạn với ai nên\n Ta chưa thể se duyên cho bạn được.";
            ClockBtnCauHon.SetActive(false);
            obj_LoiNguyetLao.SetActive(true);
            StartCoroutine(Wait_TatLoiNguyetLao(5f));
        }

    }

    public void Close_DanhSachBanBe()
    {
        int cn = Content_DanhSachBanBe.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = Content_DanhSachBanBe.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
        ID_DOITUONG = "";
        ID_CAUHON = "";
        TEN_DOITUONG = "";
        ClockBtnCauHon.SetActive(false);
        Pan_DanhSachBanBe.SetActive(false);
        PanelCauHon.SetActive(false);
    }


    void InitItemCauHon(string id_CauHon, string id, string ten)
    {
        GameObject Obj = Instantiate(ItemBanBeCauHon) as GameObject;
        Obj.GetComponent<RectTransform>().SetParent(Content_DanhSachBanBe.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        ItemCauHon MyItem = Obj.GetComponent<ItemCauHon>();
        MyItem.Init(id_CauHon, id, ten, gameObject.GetComponent<WEDDINGCONTROLL>());
    }

    public void ChonDc_DoiTuongCauHon(string id_cauhon, string id_DoiTuong, string Ten_DoiTuong)
    {
        ID_DOITUONG = id_DoiTuong;
        TEN_DOITUONG = Ten_DoiTuong;
        ID_CAUHON = id_cauhon;

        Debug.LogError("ID_DOITUONG  --- " + ID_DOITUONG + "    TEN_DOITUONG------  " + TEN_DOITUONG + "    ID_CAUHON------  " + ID_CAUHON);

        Pan_DanhSachBanBe.SetActive(false);
        // CHON NHẪN

        imgAn.SetActive(true);
        Pan_ChonNhan.SetActive(true);
        // RspNhan();
    }

    public void Init_ScrollNhan()
    {

    }



    public void Close_ChonNhan()
    {
        //int cn = Content_ChonNhan.transform.GetChildCount();
        //for (int i = 0; i < cn; i++)
        //{
        //    GameObject it = Content_ChonNhan.transform.GetChild(i).gameObject as GameObject;
        //    Destroy(it);
        //}

        TEN_NHAN = "";
        GIA_NHAN = 0;
        str_GIA_NHAN = "";
        URL_NHAN = "";
        ID_NHAN = "";
        imgAn.SetActive(true);
        //  DicNhan.Clear();
        Pan_ChonNhan.SetActive(false);
        Pan_DanhSachBanBe.SetActive(true);
    }


    void RspNhan(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.LogError("DU LIEU Nhan   --- " + _json);
        // DicUrlNhan.Clear();

        int cou = node.Count;
        for (int i = 0; i < cou; i++)
        {
            string id = node[i]["_id"]["$id"].Value;
            string ten = node[i]["name"].Value;
            long GIA = long.Parse(node[i]["price"].Value);
            string IMG = node[i]["image"].Value;
            // DicUrlNhan.Add(id, IMG);
            InitItemNhan(id, ten, GIA, IMG, i);
        }
    }


    void InitItemNhan(string id_nhan, string tennhan, long gia, string img, int id_indic)
    {
        GameObject Obj = Instantiate(ItemNhan) as GameObject;
        Obj.GetComponent<RectTransform>().SetParent(Content_ChonNhan.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1.4f, 1.4f, 1);
        ItemNhan MyItem = Obj.GetComponent<ItemNhan>();
        MyItem.Init(id_nhan, tennhan, gia, img, id_indic, gameObject.GetComponent<WEDDINGCONTROLL>());
        DicNhan.Add(id_indic, MyItem);
    }


    public void ChonDc_Nhan(string id_nhan, string tennhan, long gianhan, string str_gianhan, string url, int id_inDic)
    {
        TEN_NHAN = tennhan;
        GIA_NHAN = gianhan;
        str_GIA_NHAN = str_gianhan;
        URL_NHAN = url;
        ID_NHAN = id_nhan;

        Debug.LogError("ID_nhan --- " + id_inDic);
        foreach (var item in DicNhan)
        {
            if (item.Key == id_inDic)
            {
                item.Value.isSelec();
            }
            else
            {
                item.Value.isHide();
            }
        }
        imgAn.SetActive(false);
    }

    public void OpenTamThu()
    {
        Pan_TamThu.SetActive(true);
    }

    public void CloseThamThu()
    {
        TamThu.text = "";
        TAMTHU = "";
        Pan_TamThu.SetActive(false);
    }

    public void OpenXacNhan()
    {
        TAMTHU = TamThu.text;
        Pan_TamThu.SetActive(false);
        Pan_ChonNhan.SetActive(false);
        InitXacNhan();
        Pan_XacNhan.SetActive(true);
    }

    public void CloseXacNhan()
    {
        XN_tamthu.text = "";
        XN_ChiPhi.text = "";
        XN_PhiNhaHang.text = "";
        XN_PhiTong.text = "";
        XN_imgNhan.sprite = null;
        Pan_TamThu.SetActive(true);
        Pan_ChonNhan.SetActive(true);
        Pan_XacNhan.SetActive(false);
    }


    public void InitXacNhan()
    {
        XN_tamthu.text = TAMTHU;
        XN_ChiPhi.text = str_GIA_NHAN;
        XN_PhiNhaHang.text = Utilities.GetStringMoneyByLong(NhaHang);

        long tong = GIA_NHAN + NhaHang;
        XN_PhiTong.text = Utilities.GetStringMoneyByLong(tong);

        XN_GuiToi.text = "Gửi tới: " + TEN_DOITUONG;
        API.Instance.Load_Spr(URL_NHAN, XN_imgNhan);
    }

    public void GuiCauHon()
    {
        // GỬI REQUEST 
        LayDuLieu_CauHon();
        Pan_XacNhan_2.SetActive(false);
    }


    public void Open_Pan_XacNhan_2()
    {
        if (ID_GioiTinh == 1)
        {
            Txt_XacNhanCauHon.text = "Con có đồng ý lấy " + TEN_DOITUONG + " làm vợ không? Dù mạnh khỏe hay ốm đau, dù giàu có hay nghèo khổ, con vẫn sẽ bên cạnh chăm sóc và yêu thương cô ấy không?";
        }
        else
        {
            Txt_XacNhanCauHon.text = "Con có đồng ý lấy " + TEN_DOITUONG + " làm chồng không? Dù mạnh khỏe hay ốm đau, dù giàu có hay nghèo khổ, con vẫn sẽ bên cạnh chăm sóc và yêu thương anh ấy không?";
        }
        Pan_XacNhan.SetActive(false);
        Pan_XacNhan_2.SetActive(true);
    }

    public void Close_Pan_XacNhan_2()
    {
        Pan_XacNhan_2.SetActive(false);
        Pan_XacNhan.SetActive(true);
    }


    void Rsp_CAUHON(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.LogError("DU LIEU Nhan   --- " + _json);

        string sta = node["status"].Value;
        if (sta == "1") // thanh cong
        {
            long chip = long.Parse(node["chip"].Value);
            MyInfo.CHIP = chip;
            string ms = node["msg"].Value;
            CloseAll_CauHon();
            AlertController.api.showAlert(ms);
            //popupAlert.Show(ms, popupAlert.Hide);
        }
        else
        {
            string ms = node["msg"].Value;
            //Debug.LogError("popupAlert " + popupAlert);
            CloseAll_CauHon();
            AlertController.api.showAlert(ms);
            // popupAlert.Show(ms, popupAlert.Hide);
        }
    }



    void ClearData()
    {
        ID_DOITUONG = "";
        ID_CAUHON = "";
        TEN_DOITUONG = "";
        TEN_NHAN = "";
        GIA_NHAN = 0;
        str_GIA_NHAN = "";
        URL_NHAN = "";
        TAMTHU = "";
    }

    void CloseAll_CauHon()
    {
        XN_tamthu.text = "";
        XN_ChiPhi.text = "";
        XN_imgNhan.sprite = null;

        Pan_XacNhan.SetActive(false);
        Pan_TamThu.SetActive(false);
        Close_ChonNhan();
        Close_DanhSachBanBe();

        PanelCauHon.SetActive(false);
    }


    #endregion

    #region LOI CAU HON

    string ID_NG_CAUHON = "";
    string TEN_NG_CAUHON = "";
    string TEN_NHAN_CAUHON = "";
    string URL_NHAN_CAUHON = "";
    string TAMTHU_CAUHON = "";
    int REPLY = 0;

    string IdThuXoa = "";


    Dictionary<string, ItemThuNgo> DicThuNgo = new Dictionary<string, ItemThuNgo>();

    public void Btn_LoiNGo_Click()
    {
        ClockBtnThuNgo.SetActive(true);
        LayDuLieu_DanhSach_ThuCauHon();
    }


    void RspDanhSachThuCauHon(string _json)
    {
        // LẤY DỮ LIỆU CẦU HÔN Ở ĐÂY NÈ
        JSONNode node = JSONNode.Parse(_json);
        Debug.LogError("DU LIEU DANH SACCH THU CAU HON   --- " + _json);


        int cou = node.Count;

        if (cou > 0)
        {
            for (int i = 0; i < cou; i++)
            {
                string id = node[i]["_id"]["$id"].Value;

                string id_ck = node[i]["ck"]["$id"].Value;
                string id_vk = node[i]["vk"]["$id"].Value;
                string id_gui = node[i]["user_request"]["$id"].Value;

                string ten_ck = node[i]["ck_name"].Value;
                string ten_vk = node[i]["vk_name"].Value;

                string ten_ng_gui = "";

                if (id_gui == id_ck)
                {
                    ten_ng_gui = ten_ck;
                }
                else
                {
                    ten_ng_gui = ten_vk;
                }

                string tinnhan = node[i]["message"].Value;

                string id_nhan = node[i]["ring_id"]["$id"].Value;

                //   string url = DicUrlNhan[id_nhan];

                string url = node[i]["ring_url"].Value;

                InitItem_Thu_CauHon(id, ten_ng_gui, tinnhan, url);
            }
            Pan_DanhSachThu.SetActive(true);
            PanelThuNgo.SetActive(true);
        }
        else
        {
            TxtNguyetLao.text = "Hiện tại bạn chưa nhận được lời cầu hôn nào.";
            obj_LoiNguyetLao.SetActive(true);
            ClockBtnThuNgo.SetActive(false);
            StartCoroutine(Wait_TatLoiNguyetLao(5f));
        }


    }


    public void Close_DanhSach_ThuCauHon()
    {
        int cn = Content_DanhSachThu.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = Content_DanhSachThu.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
        DicThuNgo.Clear();
        Pan_DanhSachThu.SetActive(false);
        PanelThuNgo.SetActive(false);
        ClockBtnThuNgo.SetActive(false);
    }


    void InitItem_Thu_CauHon(string id_user, string tenuser, string tinnhan, string url)
    {
        GameObject Obj = Instantiate(ItemThuNgo) as GameObject;
        Obj.GetComponent<RectTransform>().SetParent(Content_DanhSachThu.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        ItemThuNgo MyItem = Obj.GetComponent<ItemThuNgo>();
        MyItem.Init(id_user, tenuser, tinnhan, url);

        DicThuNgo.Add(id_user, MyItem);
    }



    public void XoaThu(string id_Ng_CauHon)
    {
        IdThuXoa = id_Ng_CauHon;
        Comfrim_XoaThu();
    }


    public void Comfrim_XoaThu()
    {
        TxtConfrim.text = "Bạn muốn xóa thư này?";

        BtnConfrim.onClick.RemoveAllListeners();
        BtnConfrim.onClick.AddListener(ThucHienXoaThu);

        BtnBack_Confrim.onClick.RemoveAllListeners();
        BtnBack_Confrim.onClick.AddListener(CloseConfrim_ThuNgo);

        Pan_ConFrim.SetActive(true);
    }

    void ThucHienXoaThu()
    {
        DicThuNgo.Remove(IdThuXoa);
        int cn = Content_DanhSachThu.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = Content_DanhSachThu.transform.GetChild(i).gameObject as GameObject;
            ItemThuNgo item = it.GetComponent<ItemThuNgo>();
            if (item.ID_USER == IdThuXoa)
            {
                REPLY = -1;
                ID_NG_CAUHON = IdThuXoa;
                Destroy(it);
                TxtConfrim.text = "";
                IdThuXoa = "";
                Pan_ConFrim.SetActive(false);


                Debug.LogError("id_cauhon -- " + ID_NG_CAUHON + "   ---REPLY -- " + REPLY);

                LayDuLieu_TraLoiKetHon();
                return;
            }

        }
    }

    public void CloseConfrim_ThuNgo()
    {
        TxtConfrim.text = "";
        Pan_ConFrim.SetActive(false);
    }


    public void DocThu(string id_Ng_CauHon)
    {

        ThuNgo.text = DicThuNgo[id_Ng_CauHon].TIN_NHAN;
        string url = DicThuNgo[id_Ng_CauHon].URL;
        string URL = API.Instance.DOMAIN + url;
        //    Debug.LogError("DOMAIN --   " + API.Instance.DOMAIN + "   img -- " + img);
        Debug.LogError("URL --   " + URL);

        if (ID_QUANHE == "")
        {
            ImgClock.SetActive(false);
        }
        else
        {
            ImgClock.SetActive(true);
        }

        API.Instance.Load_Spr(URL, ImgNhan_);

        ID_NG_CAUHON = id_Ng_CauHon;
        Pan_Thu.SetActive(true);
    }

    public void Comfrim_TuChoi()
    {
        TxtConfrim.text = "Bạn muốn từ chối bạn này này?";

        BtnConfrim.onClick.RemoveAllListeners();
        BtnConfrim.onClick.AddListener(Close_TamThu_TuChoi);
        Pan_ConFrim.SetActive(true);
    }



    public void BackTamThu()
    {
        ThuNgo.text = "";
        ImgNhan_.sprite = null;
        Pan_Thu.SetActive(false);
    }

    void Close_TamThu_TuChoi()
    {
        ThuNgo.text = "";
        ImgNhan_.sprite = null;


        IdThuXoa = ID_NG_CAUHON;
        //  ID_NG_CAUHON = "";
        ThucHienXoaThu();
        Pan_Thu.SetActive(false);
    }


    public void XacNhanKetHon()
    {
        TEN_NG_CAUHON = DicThuNgo[ID_NG_CAUHON].TEN_USER;
        TAMTHU_CAUHON = DicThuNgo[ID_NG_CAUHON].TIN_NHAN;
        URL_NHAN_CAUHON = API.Instance.DOMAIN + DicThuNgo[ID_NG_CAUHON].URL;



        XN_TxtNgCauHon.text = "Bạn đồng ý kết hôn cùng " + TEN_NG_CAUHON;
        XN_Cuoi_tamthu.text = "Lời hứa: " + TAMTHU_CAUHON;
        API.Instance.Load_Spr(URL_NHAN_CAUHON, XN_imgNhan_Cuoi);
        Pan_DanhSachThu.SetActive(false);
        Pan_Thu.SetActive(false);
        Pan_XacNhan_DongY.SetActive(true);
    }





    public void Open_Pan_XacNhan_3()
    {
        if (ID_GioiTinh == 1)
        {
            Txt_XacNhan_DongY_3.text = "Con có đồng ý lấy " + TEN_DOITUONG + " làm vợ không? Dù mạnh khỏe hay ốm đau, dù giàu có hay nghèo khổ, con vẫn sẽ bên cạnh chăm sóc và yêu thương cô ấy không?";
        }
        else
        {
            Txt_XacNhan_DongY_3.text = "Con có đồng ý lấy " + TEN_DOITUONG + " làm chồng không? Dù mạnh khỏe hay ốm đau, dù giàu có hay nghèo khổ, con vẫn sẽ bên cạnh chăm sóc và yêu thương anh ấy không?";
        }
        Pan_XacNhan_DongY.SetActive(false);
        Pan_XacNhan_3.SetActive(true);
    }

    public void Close_Pan_XacNhan_3()
    {
        Pan_XacNhan_3.SetActive(false);
        Pan_XacNhan_DongY.SetActive(true);
    }






    public void XacNhanDongYKetHon()
    {
        REPLY = 1;
        LayDuLieu_TraLoiKetHon();
        TAMTHU_CAUHON = "";
        XN_TxtNgCauHon.text = "";
        XN_Cuoi_tamthu.text = "";
        XN_imgNhan_Cuoi.sprite = null;
        Pan_XacNhan_DongY.SetActive(false);
        PanelThuNgo.SetActive(false);

        Close_DanhSach_ThuCauHon();
        Pan_XacNhan_3.SetActive(false);

    }


    public void XacNhanKetHon_Back()
    {
        TAMTHU_CAUHON = "";
        XN_TxtNgCauHon.text = "";
        XN_Cuoi_tamthu.text = "";
        XN_imgNhan_Cuoi.sprite = null;
        Pan_XacNhan_DongY.SetActive(false);
        Pan_Thu.SetActive(true);
        Pan_DanhSachThu.SetActive(true);
    }



    #endregion



    //  API.Instance.RequestTopTaiSan(RspTopTaiSan);

    public void LayDuLieu_DanhSachNhan()
    {
        API.Instance.RequestGet_DanhSachNhan(RspNhan);
    }

    public void LayDuLieu_CauHon()
    {
        API.Instance.RequestGet_CauHon(ID_CAUHON, ID_NHAN, TAMTHU, Rsp_CAUHON);
    }

    public void CapNhatGioiTinh()
    {
        API.Instance.RequestGet_ThayDoi_GioiTinh(ID_GioiTinh, RspCapNhatGioiTinh);
    }


    public void LayDuLieu_DanhSach_BanBe()
    {
        BtnCauHon.enabled = false;
        Debug.LogError("LayDuLieu_DanhSach_BanBe ne");
        API.Instance.RequestGet_DanhSach_BanBe(RspDanhSachBanBe);
    }

    public void LayDuLieu_DanhSach_ThuCauHon()
    {
        API.Instance.RequestGet_DanhSach_ThuCauHon(RspDanhSachThuCauHon);
    }

    public void LayDuLieu_TraLoiKetHon()
    {

        API.Instance.RequestGet_TraLoi_CauHon(ID_NG_CAUHON, REPLY, RspTraLoi_CauHon);
    }

    public void LayDuLieu_TinhTrangHonNhan()
    {
        API.Instance.RequestGet_TinhTrangHonNhan(MyInfo.ID, RspLayTinhTrangHonNhan);
    }


    void RspLayTinhTrangHonNhan(string _json)
    {
        Debug.LogError("RspTinhTrangHonNhan " + _json);
        JSONNode node = JSONNode.Parse(_json);

        int idgioitinh = int.Parse(node["gender"].Value);
        ID_GioiTinh = idgioitinh;
        Debug.LogError("nodemarried.Count------------- " + node["married"].Count);

        if (node["married"].Count > 0)
        {
            ID_QUANHE = node["married"][0]["_id"]["$id"].Value;
            ID_CK = node["married"][0]["ck"]["$id"].Value;
            ID_VK = node["married"][0]["vk"]["$id"].Value;
            TEN_CK = node["married"][0]["ck_name"].Value;
            TEN_VK = node["married"][0]["vk_name"].Value;
            ID_NHAN_CUOI = node["married"][0]["ring_id"]["$id"].Value;
            TIN_NHAN = node["married"][0]["message"].Value;
        }

        MyInfo.ID_QUANHE = ID_QUANHE;
        checkGioiTinh();
        CheckGiaDinh();
        this.gameObject.SetActive(true);
    }


    public void ConFrim_LyHon()
    {

        TxtConfrim.text = "Bạn thật sự muốn ly hôn?";

        BtnConfrim.onClick.RemoveAllListeners();
        BtnConfrim.onClick.AddListener(LayDuLieu_GuiLyHon);

        BtnBack_Confrim.onClick.RemoveAllListeners();
        BtnBack_Confrim.onClick.AddListener(Close_ConFrim_LyHon);

        Pan_ConFrim.SetActive(true);
        PanelThuNgo.SetActive(true);
    }



    void Close_ConFrim_LyHon()
    {
        TxtConfrim.text = "";
        Pan_ConFrim.SetActive(false);
        PanelThuNgo.SetActive(false);
    }


    public void LayDuLieu_GuiLyHon()
    {
        API.Instance.RequestGet_GuiLyHon(RspLyHon);
    }


    void RspLyHon(string _json)
    {
        Debug.LogError("RspLyHon " + _json);
        JSONNode node = JSONNode.Parse(_json);

        string ms = node["msg"];
        //popupAlert.Show(ms, popupAlert.Hide);
        AlertController.api.showAlert(ms);
        Close_ConFrim_LyHon();
    }







    void RspCapNhatGioiTinh(string _json)
    {
        Debug.LogError("RspCapNhatGioiTinh " + _json);
        JSONNode node = JSONNode.Parse(_json);


        int sta = int.Parse(node["status"].Value);
        if (sta == -1)
        {
            string msg = node["msg"].Value;
            //popupAlert.Show(msg, popupAlert.Hide);
            AlertController.api.showAlert(msg);
        }
    }

    void RspTraLoi_CauHon(string _json)
    {
        Debug.LogError("RspTraLoiCauHon " + _json);
        JSONNode node = JSONNode.Parse(_json);


        int sta = int.Parse(node["status"].Value);
        if (sta == 1)
        {
            string msg = node["msg"].Value;
            //popupAlert.Show(msg, popupAlert.Hide);
            AlertController.api.showAlert(msg);
        }

    }











}

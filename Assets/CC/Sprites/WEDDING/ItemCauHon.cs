﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCauHon : MonoBehaviour
{
    string id_CauHon;
    string idUser;
    string tenUser;

    [SerializeField]
    Text Txt_Ten;
    [SerializeField]
    Button BtnCauHon;
    [SerializeField]
    WEDDINGCONTROLL Wedding;

    void Start()
    {
       // Wedding = GameObject.Find("PanelWedding").GetComponent<WEDDINGCONTROLL>();
    }

    public void Init(string idCauHon, string id, string ten, WEDDINGCONTROLL _wedding)
    {
        idUser = id;
        tenUser = ten;
        id_CauHon = idCauHon;
        Txt_Ten.text = ten;
        //Wedding = GameObject.Find("PanelWedding").GetComponent<WEDDINGCONTROLL>();
        Wedding = _wedding;
        BtnCauHon.onClick.RemoveAllListeners();
        BtnCauHon.onClick.AddListener(BtnClick);
    }

    void BtnClick()
    {
        Wedding.ChonDc_DoiTuongCauHon(id_CauHon, idUser, tenUser);
    }

}

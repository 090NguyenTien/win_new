﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemTopWinView : MonoBehaviour {

	GameObject bg;
	Text txtOrder, txtName, txtWin, txtLose, txtDraw;
	Image imgOrder;

	public void Init()
	{
		bg = transform.GetChild (0).gameObject;
		txtOrder = transform.GetChild (1).GetComponent<Text> ();
		txtName = transform.GetChild (2).GetComponent<Text> ();
		txtWin = transform.GetChild (3).GetComponent<Text> ();
		txtLose = transform.GetChild (4).GetComponent<Text> ();
		txtDraw = transform.GetChild (5).GetComponent<Text> ();
		imgOrder = transform.GetChild (6).GetComponent<Image> ();
	}

	public void Show(int _index, string _name, string _win, int _lose, int _draw)
	{
		gameObject.SetActive (true);
//		bg.SetActive (_index % 2 == 0);
		bg.GetComponent<Image> ().color = new Color (0, 0, 0, _index % 2 == 0 ? 0.12f : 0f);
		if (_index < 3) {
			txtOrder.gameObject.SetActive (false);
			imgOrder.gameObject.SetActive (true);

			imgOrder.sprite = DataHelper.GetIconAchive (_index + 1);
		} else {
			txtOrder.gameObject.SetActive (true);
			imgOrder.gameObject.SetActive (false);

			txtOrder.text = (_index + 1).ToString ();
		}

		txtName.text = _name;
		txtWin.text = _win.ToString ();
		txtLose.text = _lose.ToString ();
		txtDraw.text = _draw.ToString ();
	}
}

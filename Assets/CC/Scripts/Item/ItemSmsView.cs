﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class ItemSmsView : MonoBehaviour {
	
	Text txtCost, txtChip, txtGold;
	Image imgMain;
	Button btnItem;

	onCallBackString itemClick;

	string cost = "";

	public void Init(int _index, string _cost, string _chip, string _gold, string _provider, onCallBackString _itemClick)
	{
		GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;

		txtChip = transform.Find ("TxtValueChip").GetComponent<Text> ();
		txtGold = transform.Find ("TxtValueGold").GetComponent<Text> ();

		txtCost = transform.Find ("TxtValueCost").GetComponent<Text> ();

		imgMain = transform.Find ("ImgIconMain").GetComponent<Image> ();

		btnItem = GetComponent<Button> ();
		btnItem.onClick.AddListener (ItemOnClick);

		cost = _cost;

		txtChip.text = Utilities.GetStringMoneyByLong (long.Parse (_chip));
		txtGold.text = Utilities.GetStringMoneyByLong (long.Parse (_gold));

		txtCost.text = Utilities.GetStringMoneyByLong (long.Parse (_cost));

		itemClick = _itemClick;

		//imgMain.sprite = DataHelper.GetGoldItem (_index);
	}

	void ItemOnClick()
	{
		itemClick (cost);
	}

}

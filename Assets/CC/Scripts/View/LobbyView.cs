﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class LobbyView : MonoBehaviour {

    LobbyController Controller;
    [SerializeField]
    GameObject panelUI;
    [SerializeField]
    List<ItemRoomView> lstRoomFresher, lstRoomJunior;
    [SerializeField]
    private GameObject objItemRoom;
    [SerializeField]
    Text txtName;

    [SerializeField]
    Button btnQuickGame, btnCreateRoom, btnExit, btnNap;

    [SerializeField]
    Image imgBgRoomList;
    [SerializeField]
    GameObject panelRoomFresher, panelRoomJunior;
    [SerializeField]
    Transform trsfItemRoomFresherParent, trsfItemRoomJuniorParent;
    [SerializeField]
    Toggle toggleFresher, toggleJunior, toggleSenior;
    [SerializeField]
    Outline txtFresher, txtJunior, txtSenior;

    //Info User
    [SerializeField]
    Image imgAvatar, imgBorderAvt, imgBorderAvt_Df;
    [SerializeField]
    Text txtNameUser, txtChipUser, txtGemUser;

    [SerializeField]
    TMPro.TextMeshProUGUI txtNameUser_;

    //Join Room by RoomID
    [SerializeField]
    Button btnEnter;
    [SerializeField]
    InputField inputRoomIdJoinRoom;

    [SerializeField]
    Button btnRefresh, btnAchivement, btnShop, btnEvent, btnInbox, btnAvatar;

    [SerializeField]
    GameObject Loading;

    [SerializeField]
    Image ImgUnreadInbox;
    [SerializeField]
    Text TxtUnreadInbox, txtLevel;

    [SerializeField]
    private Toggle togRoomNumber = null, togHostName = null, togBet = null, togMember = null;

    [SerializeField]
    GameObject PanelShopBorAvatar, ScollBorderAvatar;
    [SerializeField]
    PopupInfomationManager InfomationManager;
    #region MODULE

    [SerializeField]
	Button btnModule;

    public static LobbyView api;
    private void Awake()
    {
        if (api != null)
        {
            return;
        }
        api = this;
    }
    public Text getTxtChipUser()
    {
        return txtChipUser;
    }
    public Text GetTxtLevel()
    {
        return txtLevel;
    }
    void ModuleOnClick()
	{
		Controller.ModuleOnClick ();
	}

    #endregion




    public void OpenPanelShopBorAvatar()
    {
        PanelShopBorAvatar.SetActive(true);
        ShopBorderAvatar sh = PanelShopBorAvatar.GetComponent<ShopBorderAvatar>();
        sh.PanelMua.SetActive(false);
        sh.InitPanelBuyBorAvatar();
        sh.InitTop();

    }

    public void ClosePanelShopBorAvatar()
    {
        if (ScollBorderAvatar.activeInHierarchy == true)
        {
            InfomationManager.CreateItemBorderAvatar();
        }


        PanelShopBorAvatar.SetActive(false);
        ShopBorderAvatar sh = PanelShopBorAvatar.GetComponent<ShopBorderAvatar>();
        sh.PanelMua.SetActive(false);
    }

    public void ShowBorAvatar(Sprite _spr)
    {
        if (_spr != null)
        {
            MyInfo.sprBorAvatar = _spr;
            imgBorderAvt.sprite = _spr;

            imgBorderAvt.gameObject.SetActive(true);
            imgBorderAvt_Df.gameObject.SetActive(false);

            imgBorderAvt.transform.localScale = new Vector2(1f, 1f);
        }

    }



    void NapOnClick()
    {
        Controller.NapOnClick();
    }

    public void Init(LobbyController _controller)
	{

		Controller = _controller;

		btnExit.onClick.AddListener (ExitOnClick);
		
		btnEnter.onClick.AddListener (BtnEnterOnClick);
		btnQuickGame.onClick.AddListener (QuickGameOnClick);
		btnCreateRoom.onClick.AddListener (CreateRoomOnClick);
//		btnEvent.onClick.AddListener (EventOnClick);
//		btnInbox.onClick.AddListener (InboxOnClick);
		btnShop.onClick.AddListener (ShopOnClick);

		btnRefresh.onClick.AddListener (BtnRefreshOnClick);

		toggleFresher.onValueChanged.AddListener (ToggleFresher);
		toggleJunior.onValueChanged.AddListener (ToggleJunior);
		toggleSenior.onValueChanged.AddListener (ToggleSenior);

		btnAvatar.onClick.AddListener (Controller.BtnAvatarOnClick);

//		btnAchivement.onClick.AddListener (BtnAchivementOnClick);

		btnModule.onClick.AddListener (ModuleOnClick);
        //btnNap.onClick.AddListener(NapOnClick);
        lstRoomFresher = new List<ItemRoomView>();
        lstRoomJunior = new List<ItemRoomView>();
       
        //				for (int i = 0; i < trsfItemRoomParent.childCount; i++) {
        //						GameObject.Destroy (trsfItemRoomParent.GetChild (i).gameObject);
        //				}


        InitLanguage ();

        togRoomNumber.onValueChanged.AddListener(value => sorByRoom(value));
        togHostName.onValueChanged.AddListener(value => sorByName(value));
        togBet.onValueChanged.AddListener(value => sorByBetMoney(value));
        togMember.onValueChanged.AddListener(value => sorByMember(value));
    }

    private void sorByRoom(bool value)
    {
        if (value == false)
        {
            return;
        }
        lstRoomFresher = lstRoomFresher.OrderBy(x => x.itemRoomInfo.ID).ToList();
        for (int i = 0; i < lstRoomFresher.Count; i++)
        {
            lstRoomFresher[i].transform.SetSiblingIndex(i);
        }

        lstRoomJunior = lstRoomJunior.OrderBy(x => x.itemRoomInfo.ID).ToList();
        for (int i = 0; i < lstRoomJunior.Count; i++)
        {
            lstRoomJunior[i].transform.SetSiblingIndex(i);
        }
      
    }
    private void sorByName(bool value)
    {
        if (value == false)
        {
            return;
        }
        lstRoomFresher = lstRoomFresher.OrderBy(x => x.itemRoomInfo.Host).ToList();
        for (int i = 0; i < lstRoomFresher.Count; i++)
        {
            lstRoomFresher[i].transform.SetSiblingIndex(i);
        }

        lstRoomJunior = lstRoomJunior.OrderBy(x => x.itemRoomInfo.Host).ToList();
        for (int i = 0; i < lstRoomJunior.Count; i++)
        {
            lstRoomJunior[i].transform.SetSiblingIndex(i);
        }
    }
    private void sorByBetMoney(bool value)
    {
        if (value == false)
        {
            return;
        }
        lstRoomFresher = lstRoomFresher.OrderBy(x => x.itemRoomInfo.Bet).ToList();
        for (int i = 0; i < lstRoomFresher.Count; i++)
        {
            lstRoomFresher[i].transform.SetSiblingIndex(i);
        }

        lstRoomJunior = lstRoomJunior.OrderBy(x => x.itemRoomInfo.Bet).ToList();
        for (int i = 0; i < lstRoomJunior.Count; i++)
        {
            lstRoomJunior[i].transform.SetSiblingIndex(i);
        }

    }
    private void sorByMember(bool value)
    {
        if (value == false)
        {
            return;
        }
        lstRoomFresher = lstRoomFresher.OrderBy(x => x.itemRoomInfo.PlayerCurrent).ToList();
        for (int i = 0; i < lstRoomFresher.Count; i++)
        {
            lstRoomFresher[i].transform.SetSiblingIndex(i);
        }

        lstRoomJunior = lstRoomJunior.OrderBy(x => x.itemRoomInfo.PlayerCurrent).ToList();
        for (int i = 0; i < lstRoomJunior.Count; i++)
        {
            lstRoomJunior[i].transform.SetSiblingIndex(i);
        }

    }

    public void ShopOnClick()
	{
		Controller.ShopOnClick ();
	}

    public void ShopOnClick_GEM()
    {
        Controller.ShopOnClick(true);
    }

    public bool LOADING{
		set{ Loading.SetActive (value); }
	}

	void ExitOnClick()
	{
		Controller.ExitOnClick ();
	}
	void BtnAchivementOnClick()
	{
		Controller.BtnAchivementOnClick ();
	}
	public void SetEnablePanelUI(bool _enable)
	{
		panelUI.SetActive (_enable);
	}

		#region Add Item Room 

	public void AddItemRoomFresherView(int _index, ItemRoomInfo _room, bool _isPass, BaseCallBack.onCallBackIntBool _onClick)
	{
		ItemRoomView roomView;

		if (_index >= lstRoomFresher.Count) {
			GameObject obj = Instantiate (objItemRoom) as GameObject;

			obj.transform.SetParent (trsfItemRoomFresherParent);

			obj.transform.SetSiblingIndex (_index);
			obj.transform.localScale = Vector3.one;
			obj.transform.position = Vector3.zero;

			roomView = obj.GetComponent<ItemRoomView> ();
		} else {
			roomView = lstRoomFresher [_index];
		}
		roomView.Init ();
		roomView.Show (_room, _index % 2 == 1, _isPass, _onClick);

		lstRoomFresher.Add (roomView);
	}
    public void AddItemRoomJuniorView(int _index, ItemRoomInfo _room, bool _isPass, BaseCallBack.onCallBackIntBool _onClick)
    {
        ItemRoomView roomView;

        if (_index >= lstRoomJunior.Count)
        {
            GameObject obj = Instantiate(objItemRoom) as GameObject;

            obj.transform.SetParent(trsfItemRoomJuniorParent);

            obj.transform.SetSiblingIndex(_index);
            obj.transform.localScale = Vector3.one;
            obj.transform.position = Vector3.zero;

            roomView = obj.GetComponent<ItemRoomView>();
        }
        else
        {
            roomView = lstRoomJunior[_index];
        }
        roomView.Init();
        roomView.Show(_room, _index % 2 == 1, _isPass, _onClick);
        roomView.SetSoloPlayerMax(_room);//phong solo thì cho user max luôn = 2

        lstRoomJunior.Add(roomView);
    }
    

    #endregion

    public void ClearEmptyRooms(int _level, int _indexBegin)
		{
				if (_level == 1) {
						for (int i = trsfItemRoomFresherParent.childCount - 1; i >= _indexBegin; i--) {
								GameObject.Destroy (trsfItemRoomFresherParent.GetChild (i).gameObject);

								if (lstRoomFresher.Count > i)
										lstRoomFresher.RemoveAt (i);
						}
				} else if (_level == 2) {
						for (int i = trsfItemRoomJuniorParent.childCount - 1; i >= _indexBegin; i--) {
								GameObject.Destroy (trsfItemRoomJuniorParent.GetChild (i).gameObject);

								if (lstRoomJunior.Count > i)
										lstRoomJunior.RemoveAt (i);
						}
				}
				
		}

	public void ShowChip(long _chip)
	{
		txtChipUser.text = Utilities.GetStringMoneyByLong (_chip);

        ChatController.RegistTextChip(txtChipUser);
    }
	public void ShowName(string _nameGame)
	{
		txtName.text = _nameGame;
	}

	void CreateRoomOnClick()
	{
		Controller.CreateRoomOnClick ();
	}
		void QuickGameOnClick()
	{
			Controller.QuickGameOnClick ();
	}

    #region Player View

    public void ShowInfoUser(string _name, string _chip, Sprite _spr)
	{
				//txtNameUser.text = _name;
        txtNameUser_.text = _name;

                txtChipUser.text = _chip;
				imgAvatar.sprite = _spr;


        int id_Bor = MyInfo.MY_BOR_AVATAR;
        if (id_Bor != -1)
        {
            Sprite h = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
            if (h != null)
            {
                imgBorderAvt.sprite = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
                imgBorderAvt.gameObject.SetActive(true);
                imgBorderAvt_Df.gameObject.SetActive(false);
            }
            else
            {
                imgBorderAvt.gameObject.SetActive(false);
                imgBorderAvt_Df.gameObject.SetActive(true);
            }

        }
        else
        {
            imgBorderAvt.gameObject.SetActive(false);
            imgBorderAvt_Df.gameObject.SetActive(true);
        }
        UpdateGemUser();

        //				StartCoroutine (UpdateAvatarThread (_urlAvt));
    }
    public void UpdateChipUser()
    {
        txtChipUser.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
    }
    public void UpdateGemUser()
    {
        txtGemUser.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
    }


    public void UpdateChipUser(long _chip, long _gold)
	{
        txtChipUser.text = Utilities.GetStringMoneyByLong(_chip);
    }
	public void UpdateAvatar()
    {
		//imgAvatar.sprite = DataHelper.GetAvatar (MyInfo.AvatarName);
        if (MyInfo.AvatarName.Contains("http") == true)
        {
            StartCoroutine(DataHelper.UpdateAvatarThread(MyInfo.AvatarName, imgAvatar.sprite));
        }
        else
        {
            //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
            StartCoroutine(DataHelper.UpdateAvatarThread(API.PREFIX_AVT + MyInfo.AvatarName, imgAvatar));
        }
    }
    
	public IEnumerator UpdateAvatarThread (string _url)
	{
		Texture2D mainImage;
		WWW www = new WWW(_url);
		yield return www;

		mainImage = www.texture;
		Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

		imgAvatar.sprite = spr;
	}

		#endregion

		#region Join Room by RoomID

		void BtnEnterOnClick()
		{
				Controller.BtnEnterOnClick (inputRoomIdJoinRoom.text);
		}

		#endregion

		void BtnRefreshOnClick()
		{
			Controller.BtnRefreshOnClick ();
		}

		#region Panel Fresher Junior Senior

		void ToggleFresher(bool _enable)
		{
				Controller.ToggleFresher (_enable);
		}
		void ToggleJunior(bool _enable)
		{
				Controller.ToggleJunior(_enable);
		}
		void ToggleSenior(bool _enable)
		{
				Controller.ToggleSenior (_enable);
		}

		public void SetEnablePanelFresher(bool _enable)
		{
				panelRoomFresher.SetActive (_enable);

				RectTransform rect = trsfItemRoomFresherParent.parent.GetComponent<RectTransform> ();
				rect.anchorMin = new Vector2 (0, 0);
				rect.anchorMax = new Vector2 (1, 1);

		txtFresher.enabled = _enable;

		if (_enable)
			imgBgRoomList.color = (Color)new Color32 (82, 165, 82, 255);

		}
		public void SetEnablePanelJunior(bool _enable)
		{
				panelRoomJunior.SetActive (_enable);

				RectTransform rect = trsfItemRoomJuniorParent.parent.GetComponent<RectTransform> ();
				rect.anchorMin = new Vector2 (0, 0);
				rect.anchorMax = new Vector2 (1, 1);

		txtJunior.enabled = _enable;

		if (_enable)
			imgBgRoomList.color = (Color)new Color32 (222, 239, 16, 255);
		}

		#endregion

	void InitLanguage()
	{
		btnCreateRoom.transform.GetChild (0).GetComponent<Text> ().text = ConstText.Create_Room;
		btnQuickGame.transform.GetChild (0).GetComponent<Text> ().text = ConstText.Quick_Game;

		inputRoomIdJoinRoom.transform.GetChild (0).GetComponent<Text> ().text = ConstText.EnterRoomId;
	}

    public void ShowOrHideUnreadInbox()
    {
        if (MyInfo.UNREAD_INBOX == 0)
        {
            TxtUnreadInbox.gameObject.SetActive(false);
            ImgUnreadInbox.gameObject.SetActive(false);
        }
        else
        {
            TxtUnreadInbox.text = MyInfo.UNREAD_INBOX + "";
            TxtUnreadInbox.gameObject.SetActive(true);
            ImgUnreadInbox.gameObject.SetActive(true);
        }
    }
}

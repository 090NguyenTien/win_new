﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using DG.Tweening;
using System.Collections.Generic;
using Facebook.Unity.Settings;

public class LoginView : MonoBehaviour {
		[SerializeField]
		Text txtTest, txtFaceId;
		private LoginController Controller;

		[SerializeField]
		GameObject panelSignIn, panelSignUp;
//		[SerializeField]
//		Toggle toggleSignIn, toggleSignUp;
	[SerializeField]
	Button btnParent, btnParent2;
	[SerializeField]
	Text txtParent;
		[SerializeField]
		public InputField inputUsernameSignIn, inputPasswordSignIn;
		[SerializeField]
		Button btnSignIn;

		[SerializeField]
		InputField inputUsernameSignUp, inputPasswordSignUp, inputConfirmSignUp;
		[SerializeField]
		Button btnSignUp;


	[SerializeField]
	Button btnFogetPass;

		[SerializeField]
		Text txtErrorSignIn, txtErrorSignUp;

	[SerializeField]
	Toggle toggleTEST;
    [SerializeField]
    Button btnLoginFace;
    [SerializeField]
    bool PC_Version;





    [SerializeField]
    GameObject MIDDLE;



    [SerializeField]
    GameObject TenDangNhap;

    //string[] CacTenGoiY = { "Nguyễn Tiến_01", "Nguyễn Tiến_02", "Nguyễn Tiến_03", "Nguyễn Tiến_04", "Nguyễn Tiến_05", "Nguyễn Tiến_06" };
    //  string MyName = "Nguyễn Tiến";
    public void ShowTenDangNhap(string TenNhap, List<string> CacTenGoiY, bool HopLe)
    {
        TenDangnhapController TenControll = TenDangNhap.GetComponent<TenDangnhapController>();
        TenControll.Init(TenNhap, CacTenGoiY);
        MIDDLE.SetActive(false);
        TenDangNhap.SetActive(true);


        TenControll.ShowTenGoiY(HopLe);

    }

    public void CloseTenDangNhap()
    {
        TenDangnhapController TenControll = TenDangNhap.GetComponent<TenDangnhapController>();
        TenControll.ClearData();
        MIDDLE.SetActive(true);
        TenDangNhap.SetActive(false);
    }












    public void ShowTest(string _text)
	{
        if(txtTest!=null)
			txtTest.text = _text;
	}
	public void Init(LoginController _controller)
	{
		InitLanguage ();
				Controller = _controller;

				btnSignIn.onClick.AddListener (BtnSignInOnClick);
				btnSignUp.onClick.AddListener (BtnSignUpOnClick);

//				toggleSignIn.onValueChanged.AddListener (ToggleSignInOnChange);
		btnParent.onClick.AddListener(BtnParentOnClick);
        MyInfo.PC_VERSION = PC_Version;
        //if (!PC_Version)
        //{
            btnParent2.onClick.AddListener(BtnParentOnClick);
            btnLoginFace.gameObject.SetActive(true);
            //btnParent2.gameObject.SetActive(false);
        //}
        //else
        //{
            //btnLoginFace.gameObject.SetActive(false);
            //btnParent2.gameObject.SetActive(true);
        //}
		

		btnFogetPass.onClick.AddListener (Controller.BtnFogetPasswordOnClick);

		//toggleTEST.onValueChanged.AddListener (toggleOnChangeTEST);

        txtFaceId.text = FacebookSettings.AppId;

    }

	void toggleOnChangeTEST(bool _enable)
	{
		txtTest.gameObject.SetActive (_enable);
	}

	void BtnParentOnClick()
	{
		Controller.BtnParentOnClick ();
	}
	public void ShowTextParent(string _text)
	{
//		txtParent.text = _text;
	}
	void BtnSignInOnClick()
	{
			Controller.BtnSignInOnClick (inputUsernameSignIn.text, inputPasswordSignIn.text);
	}
	void BtnSignUpOnClick()
	{
			Controller.BtnSignUpOnClick (inputUsernameSignUp.text, inputPasswordSignUp.text, inputConfirmSignUp.text);
	}
	void ToggleSignInOnChange(bool _enable)
	{
			Controller.ToggleSignInOnChange (_enable);
	}
	public void SetToogleSignIn(bool _enable)
	{
//				toggleSignIn.isOn = _enable;
	}

	public void SetEnablePanel(bool _isSignIn)
	{
			panelSignIn.SetActive (_isSignIn);
			panelSignUp.SetActive (!_isSignIn);


	}

	public void ShowInfoSignIn(string _uname = "", string _pass = "")
	{
			inputUsernameSignIn.text = _uname;
			inputPasswordSignIn.text = _pass;
	}
	public void ShowInfoSignUp(string _uname = "", string _pass = "", string _passConfirm = "")
	{
			inputUsernameSignUp.text = _uname;
			inputPasswordSignUp.text = _pass;
			inputConfirmSignUp.text = _passConfirm;
	}

	public void ShowErrorSignIn(string _error = "")
	{
	    txtErrorSignIn.text = _error;
	}
	public void ShowErrorSignUp(string _error = "")
	{
		txtErrorSignUp.text = _error;
	}

	IEnumerator Thread(float _time, onCallBack _callBack)
	{
			yield return new WaitForSeconds (_time);

			_callBack ();
	}

	void InitLanguage()
	{
		inputUsernameSignIn.transform.GetChild (0).GetComponent<Text> ().text = ConstText.Username;
		inputUsernameSignUp.transform.GetChild (0).GetComponent<Text> ().text = ConstText.Username;
		inputPasswordSignIn.transform.GetChild (0).GetComponent<Text> ().text = ConstText.Password;
		inputPasswordSignUp.transform.GetChild (0).GetComponent<Text> ().text = ConstText.Password;
		inputConfirmSignUp.transform.GetChild (0).GetComponent<Text> ().text = ConstText.Password;

//		inputUsernameSignIn.transform.GetChild (0).GetComponent<Text> ().text = Const.Username;

		btnSignIn.transform.GetChild (0).GetComponent<Text> ().text = ConstText.SignIn;
		btnSignUp.transform.GetChild (0).GetComponent<Text> ().text = ConstText.SignUp;

		btnFogetPass.GetComponent<Text> ().text = ConstText.FogetPassword;
	}
}

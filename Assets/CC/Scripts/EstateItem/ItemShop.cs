﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShop : MonoBehaviour {
    [SerializeField]
    Image ImgItem, ImgUse;
    [SerializeField]
    GameObject ImgClock, _DiamonPrice, _ChipPrice, Img_DangDung, Action ;
    [SerializeField]
    Text TxtNameItem, TxtDiamonPrice, TxtChipPrice, TxtLevel, TxtPoint, TxtDoanhThu, Txt_Mau;

    [SerializeField]
    Button Btn_SuDung, Btn_Nangcap;
    [SerializeField]
    Text Txt_Btn_nangCap;
    [SerializeField]
    MadControll Mad;
    [SerializeField]
    TestItemInHome PanelBuy;
    [SerializeField]
    UpdateItem PanelUpDate;
    [SerializeField]
    Image Sli_Mau;

    public long LongDiamonPrice, LongChipPrice, Save_Chip, Save_Gem, DoanhThu, DoanhThuSauNangCap;


    public string Ten, ThongTin, Id, Img, MyLevel;

    public int DiemTaiSan, stt, soluong;



    public bool IsClock = true;
    public string MyIdTong = "";
    public string MyId_Rieng = "";
    Button MyButton;
    // Use this for initialization
    long mauhientai = 0;
    long mautong = 0;



    void ChangeSlider_Mau(Image sli_Mau, long MaxPoint, long MinPoint, long VipCollect)
    {
        long Tong = MaxPoint - MinPoint;
        long Diem = VipCollect - MinPoint;

        float KQ = (float)Diem / (float)Tong;
        Debug.LogWarning("Vo ChangeSlider_Mau vaf ra ket qua: " + KQ);
        sli_Mau.fillAmount = KQ;
    }





    public void Init(string IdTong, bool Clock, string level, bool isSale = false, long PercentSale = 0,string Id = "")
    {
 
        My_Item it = DataHelper.DuLieuShop[IdTong];
        MyIdTong = IdTong;
        IsClock = Clock;
        Img = it.img;
        MyId_Rieng = Id;

        //     ImgItem.sprite = DataHelper.dictSprite_Shop[Id];

        API.Instance.Load_SprItem(Img, ImgItem);

        TxtNameItem.text = it.ten;
        MyButton = this.gameObject.GetComponent<Button>();
        LongDiamonPrice = it.gem;
        LongChipPrice = it.chip;

    
        

        TxtPoint.text = it.diemtaisan.ToString();
        DiemTaiSan = it.diemtaisan;
        ThongTin = it.thongtin;
        DoanhThu = it.Revenue_chip;
        Img_DangDung.SetActive(false);
        Action.SetActive(false);
       // TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);
        TxtLevel.text = level;
        Txt_Mau.gameObject.SetActive(false);

        MyLevel = level;

        long levelsaunangcap = long.Parse(level) + 1;

        DoanhThuSauNangCap = GetDoanhThuByGiaBan(LongChipPrice, levelsaunangcap);

        TxtLevel.gameObject.SetActive(true);




        long longLevel = long.Parse(level);
        if (longLevel > 1)
        {
            long dt = GetDoanhThuByGiaBan(LongChipPrice, longLevel);
            TxtDoanhThu.text = Utilities.GetStringMoneyByLong(dt);
        }
        else
        {
            TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);
        }


        Mad = GameObject.Find("Mad").GetComponent<MadControll>();

        if (IsClock == true)
        {

            GameObject obj = GameObject.Find("PanelUI_Home");
            EstateControll estate = obj.GetComponent<EstateControll>();

            GameObject panell = estate.PanelBuyItem;

            PanelBuy = panell.GetComponent<TestItemInHome>();

            // PanelBuy = GameObject.Find("PanelBuyItem").GetComponent<TestItemInHome>();
            Img_DangDung.SetActive(false);
            Action.SetActive(false);



            TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(LongDiamonPrice);

            if (LongChipPrice == 0)
            {

                _ChipPrice.SetActive(false);
            }
            else
            {
                TxtChipPrice.text = Utilities.GetStringMoneyByLong(LongChipPrice);
                _ChipPrice.SetActive(true);
            }

            
            _DiamonPrice.SetActive(true);

            ImgClock.SetActive(true);

            MyButton.onClick.RemoveAllListeners();
            MyButton.onClick.AddListener(BtnClockClick);
        }
        else
        {
            Mad = GameObject.Find("Mad").GetComponent<MadControll>();
            GameObject obj = GameObject.Find("PanelUI_Home");
            EstateControll estate = obj.GetComponent<EstateControll>();
            GameObject UpDate = estate.PaneUpdate;
            PanelUpDate = UpDate.GetComponent<UpdateItem>();
            TxtLevel.text = level;
            TxtLevel.gameObject.SetActive(true);
            _DiamonPrice.SetActive(false);
            _ChipPrice.SetActive(false);
            ImgClock.SetActive(false);

            // ImgUse.gameObject.SetActive(true);

            //hien thi mau
            mauhientai = DataHelper.DuLieuSoHuu_V2[Id].MauHienTai;
            mautong = DataHelper.DuLieuSoHuu_V2[Id].MauTong;

            ChangeSlider_Mau(Sli_Mau, mautong, 0, mauhientai);

            //Txt_Mau.text = mauhientai.ToString() + "/" + mautong.ToString();
            //Txt_Mau.gameObject.SetActive(true);

            My_Item_SoHuu temp = DataHelper.DuLieuSoHuu_V2[MyId_Rieng];
            bool inMad = temp.InMad;

            if (inMad == true)
            {
                Img_DangDung.SetActive(true);

                Btn_SuDung.gameObject.SetActive(false);


            }
            else
            {
                Img_DangDung.SetActive(false);
                Btn_SuDung.gameObject.SetActive(true);
            }


            Action.SetActive(true);


            Btn_SuDung.onClick.RemoveAllListeners();

            Btn_SuDung.onClick.AddListener(BtnClick);



            if (mauhientai < mautong)
            {
                Txt_Btn_nangCap.text = "Sửa";
                Btn_Nangcap.onClick.RemoveAllListeners();
                Btn_Nangcap.onClick.AddListener(BtnSuaChua);
            }
            else
            {
                Btn_Nangcap.onClick.RemoveAllListeners();
                Btn_Nangcap.onClick.AddListener(BtnNangCap);
            }

           
            //if (level == "0")
            //{
            //    MyButton.onClick.AddListener(DontChange);
            //}
            //else
            //{
            //    MyButton.onClick.AddListener(BtnClick);
            //}

        }

    }


    public void InitCar(string IdTong, bool Clock, string level, bool isSale = false, long PercentSale = 0, string Id = "")
    {

        My_Item it = DataHelper.DuLieuXe[IdTong];

        MyIdTong = IdTong;
        IsClock = Clock;
        MyId_Rieng = Id;
        Img = it.img;
        API.Instance.Load_SprItem(Img, ImgItem);
        //  ImgItem.sprite = DataHelper.dictSprite_Car[Id];
        TxtNameItem.text = it.ten;
        MyButton = this.gameObject.GetComponent<Button>();
        LongDiamonPrice = it.gem;
        LongChipPrice = it.chip;
        ThongTin = it.thongtin;

        DoanhThu = it.Revenue_chip;
       
        //TxtDoanhThu.text = it.Revenue_chip.ToString();
        Mad = GameObject.Find("Mad").GetComponent<MadControll>();
 
        TxtPoint.text = it.diemtaisan.ToString();
        DiemTaiSan = it.diemtaisan;

        Txt_Mau.gameObject.SetActive(false);

        Img_DangDung.SetActive(false);
        Action.SetActive(false);

        TxtLevel.text = level;
        TxtLevel.gameObject.SetActive(true);

        MyLevel = level;

        long levelsaunangcap = long.Parse(level) + 1;

        long giaban = LongDiamonPrice * 5000000;

        DoanhThuSauNangCap = GetDoanhThuByGiaBan(giaban, levelsaunangcap);


       // TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);

        long longLevel = long.Parse(level);
       
        if (longLevel > 1)
        {
            long dt = GetDoanhThuByGiaBan(giaban, longLevel);
            TxtDoanhThu.text = Utilities.GetStringMoneyByLong(dt);
        }
        else
        {
            TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);
        }



        if (IsClock == true)
        {

            GameObject obj = GameObject.Find("PanelUI_Home");
            EstateControll estate = obj.GetComponent<EstateControll>();
            GameObject panell = estate.PanelBuyItem;
            PanelBuy = panell.GetComponent<TestItemInHome>();

            Img_DangDung.SetActive(false);
            Action.SetActive(false);


            // PanelBuy = GameObject.Find("PanelBuyItem").GetComponent<TestItemInHome>();
            TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(LongDiamonPrice);
            if (LongChipPrice == 0)
            {
                _ChipPrice.SetActive(false);
            }
            else
            {
                TxtChipPrice.text = Utilities.GetStringMoneyByLong(LongChipPrice);
                _ChipPrice.SetActive(true);
            }
            _DiamonPrice.SetActive(true);

            ImgClock.SetActive(true);
            MyButton.onClick.RemoveAllListeners();
            MyButton.onClick.AddListener(BtnCarClockClick);
        }
        else
        {

            Mad = GameObject.Find("Mad").GetComponent<MadControll>();
            GameObject obj = GameObject.Find("PanelUI_Home");
            EstateControll estate = obj.GetComponent<EstateControll>();
            GameObject UpDate = estate.PaneUpdate;
            PanelUpDate = UpDate.GetComponent<UpdateItem>();
            TxtLevel.text = level;
            TxtLevel.gameObject.SetActive(true);
            _DiamonPrice.SetActive(false);
            _ChipPrice.SetActive(false);
            ImgClock.SetActive(false);
            //ImgUse.gameObject.SetActive(true);

            //hien thi mau
            mauhientai = DataHelper.DuLieuSoHuu_V2[Id].MauHienTai;
            mautong = DataHelper.DuLieuSoHuu_V2[Id].MauTong;

            ChangeSlider_Mau(Sli_Mau, mautong, 0, mauhientai);

            //Txt_Mau.text = mauhientai.ToString() + "/" + mautong.ToString();
            //Txt_Mau.gameObject.SetActive(true);

            My_Item_SoHuu temp = DataHelper.DuLieuSoHuu_V2[MyId_Rieng];
            bool inMad = temp.InMad;

            if (inMad == true)
            {
                Img_DangDung.SetActive(true);

                Btn_SuDung.gameObject.SetActive(false);


            }
            else
            {
                Img_DangDung.SetActive(false);
                Btn_SuDung.gameObject.SetActive(true);
            }


            Action.SetActive(true);


            Btn_SuDung.onClick.RemoveAllListeners();


            Btn_SuDung.onClick.AddListener(BtnCarClick);


            if (mauhientai < mautong)
            {
                Txt_Btn_nangCap.text = "Sửa";
                Btn_Nangcap.onClick.RemoveAllListeners();
                Btn_Nangcap.onClick.AddListener(BtnSuaChuaCar);
            }
            else
            {
                Btn_Nangcap.onClick.RemoveAllListeners();
                Btn_Nangcap.onClick.AddListener(BtnNangCapCar);
            }
        }
    }


    public void DontChange()
    {
        Mad.DontChange();
    }


    public void BtnClick()
    {       
        Mad.ChangeShop(MyIdTong, MyId_Rieng);
    }
    public void BtnClockClick()
    {
        // Mad.ChangeHome(MyId);
        PanelBuy.InitShop(MyIdTong, TxtNameItem.text, ThongTin, LongDiamonPrice, LongChipPrice, DiemTaiSan, DoanhThu);
        PanelBuy.Show();
    }



    public void BtnSuaChua()
    {
       
        PanelUpDate.Init_SuaSanPham(MyIdTong, MyId_Rieng, TxtNameItem.text, "shop");
    }

    public void BtnSuaChuaCar()
    {
     
        PanelUpDate.Init_SuaSanPham(MyIdTong, MyId_Rieng, TxtNameItem.text, "car", true);
    }




    public void BtnNangCap()
    {
      //  Mad.ChangeHome(Id_Aset, MyId_SoHuu);

        int Uplevel = int.Parse(MyLevel) + 1;
        string Uplevel_string = Uplevel.ToString();

        Debug.LogError("MyId_Rieng-------------- " + MyId_Rieng+ "------------MyId_tong------------- " + MyIdTong);


        PanelUpDate.Init(MyIdTong, MyId_Rieng, TxtNameItem.text, Uplevel_string, DoanhThuSauNangCap, "shop");
    }

    public void BtnNangCapCar()
    {
        //  Mad.ChangeHome(Id_Aset, MyId_SoHuu);

        int Uplevel = int.Parse(MyLevel) + 1;
        string Uplevel_string = Uplevel.ToString();

     //   Debug.LogWarning("MyId_Rieng-------------- " + MyId_Rieng);


        PanelUpDate.Init(MyIdTong, MyId_Rieng, TxtNameItem.text, Uplevel_string, DoanhThuSauNangCap, "car", true);
    }



    long GetDoanhThuByGiaBan(long GiaBan, long level)
    {
        long doanhthu = (GiaBan / 1000) * level;
        return doanhthu;
    }


    public void BtnCarClick()
    {
        Mad.ChangeCar(MyIdTong, MyId_Rieng);
    }
    public void BtnCarClockClick()
    {
        // Mad.ChangeHome(MyId);
        PanelBuy.InitCar(MyIdTong, TxtNameItem.text, ThongTin, LongDiamonPrice, LongChipPrice, DiemTaiSan, DoanhThu);
        PanelBuy.Show();
    }

}

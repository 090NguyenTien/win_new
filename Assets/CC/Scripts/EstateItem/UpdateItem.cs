﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class UpdateItem : MonoBehaviour
{
    [SerializeField]
    Text TxtNameItem, TxtLevelUp, TxtDoanhThu, TxtDiamonPrice, TxtChipPrice, TxtMau;
    [SerializeField]
    Image ImgItem;
    string MyId = "";
    string MyId_Rieng = "";
    [SerializeField]
    MadControll Mad;
    [SerializeField]
    EstateControll estate;
    private int ChangeType = -1;

    public long GiaChip, GiaGem;
    [SerializeField]
    GameObject Alert;
    [SerializeField]
    Text TxtAlert;
    [SerializeField]
    GameObject BtnGEM, PanelScroll, TopLevel, BtnCHIP;
    long TienUp = 5000000;
    long GiaNangCap = 0;
    string Type = "";
    long MyDoanhThu = 0;

    long MauSauNangCap = 0;
    [SerializeField]
    PopupAlertManager popupAlert;





    public void Init(string ID, string Id_Rieng, string _name, string level, long DoanhThu, string type, bool ByGem = false)
    {
        PanelScroll.SetActive(false);
        MyId = ID;
        MyId_Rieng = Id_Rieng;
        Type = type;
        MyDoanhThu = DoanhThu;
        Debug.LogError("Id_Rieng init " + Id_Rieng);
        TopLevel.SetActive(false);
        // ImgItem.sprite = DataHelper.dictSprite_House[ID];
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);
        TxtNameItem.text = _name;
        TxtLevelUp.text = level;

        TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);
        //TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
        //  BtnGEM.SetActive(false);
        this.gameObject.SetActive(true);
        BtnGEM.SetActive(true);
        BtnCHIP.SetActive(false);



        My_Item it = new My_Item();
        if (type == "house")
        {
            it = DataHelper.DuLieuNha[MyId];
        }
        else if (type == "shop")
        {
            it = DataHelper.DuLieuShop[MyId];
        }
        else if (type == "car")
        {
            it = DataHelper.DuLieuXe[MyId];
        }





        long level_long = long.Parse(level);
        int level_int = int.Parse(level);
        Debug.LogError("level_int "+ level_int);
        float PhanTramNangCap = DataHelper.PhanTramDeNangCapSanPham[level_int];

        long f_giamua = 0;
        long f_GiaNangCap = 0;

        if (ByGem == false)
        {

            f_giamua  = it.chip;

            // GiaNangCap =(giamua * level_long)/5000000;

           // f_giamua = giamua;
            f_GiaNangCap = (long)(f_giamua * PhanTramNangCap) / 5000000;
            GiaNangCap = (long)f_GiaNangCap;

        }
        else
        {
            f_giamua = it.gem;
            //GiaNangCap = (giamua * TienUp * level_long) / 5000000;

           // f_giamua = (float)giamua;
            f_GiaNangCap = (long)(f_giamua * PhanTramNangCap);
            GiaNangCap = (long)f_GiaNangCap;

        }

        Debug.LogError("level_int " + level_int + " ------------ giamua " + f_giamua + " ------------ PhanTramNangCap = "+ PhanTramNangCap + " ------------ GiaNangCap = " + GiaNangCap);

        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(GiaNangCap);
        long mauhientai = DataHelper.DuLieuSoHuu_V2[MyId_Rieng].MauHienTai;



        long long_level = long.Parse(level);
        long mausaukhinangcap = DataHelper.TongGiaTri_CuaSanPham_TaiLevel(MyId, long_level);

        MauSauNangCap = mausaukhinangcap;

        Debug.LogWarning("MauHienTai = " + mauhientai + " ----------- MauSauNangCap = " + mausaukhinangcap);

        string str_mauhientai = Utilities.GetStringMoneyByLong(mauhientai);
        string str_mausaunangcap = Utilities.GetStringMoneyByLong(MauSauNangCap);

        TxtMau.text = "Lượng Máu tăng từ " + str_mauhientai + " ===>> " + str_mausaunangcap + " máu";

        BtnGEM.GetComponent<Button>().onClick.RemoveAllListeners();
        BtnGEM.GetComponent<Button>().onClick.AddListener(MuaBangGem);
    }








    public void Init_SuaSanPham(string ID, string Id_Rieng, string _name, string type, bool ByGem = false)
    {
        PanelScroll.SetActive(false);
        MyId = ID;
        MyId_Rieng = Id_Rieng;
        Type = type;
        BtnGEM.SetActive(false);
        BtnCHIP.SetActive(true);
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);
        TxtNameItem.text = _name;

        TopLevel.SetActive(true);
     
        this.gameObject.SetActive(true);

        My_Item it = new My_Item();
        if (type == "house")
        {
            it = DataHelper.DuLieuNha[MyId];
        }
        else if (type == "shop")
        {
            it = DataHelper.DuLieuShop[MyId];
        }
        else if (type == "car")
        {
            it = DataHelper.DuLieuXe[MyId];
        }

     //   TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(GiaNangCap);

        

        long mauhientai = DataHelper.DuLieuSoHuu_V2[MyId_Rieng].MauHienTai;
        long mautonghientai = DataHelper.DuLieuSoHuu_V2[MyId_Rieng].MauTong;

        GiaNangCap = (mautonghientai - mauhientai) / 2;

    //    Debug.LogError("gia sua " + GiaNangCap);
        TxtChipPrice.text = Utilities.GetStringMoneyByLong(GiaNangCap);
        TxtMau.gameObject.SetActive(false);
       
        BtnCHIP.GetComponent<Button>().onClick.RemoveAllListeners();
        BtnCHIP.GetComponent<Button>().onClick.AddListener(SuaNhaBangGem);


    }









    long GiaChipUpDate()
    {
        long gia = 0;
        return gia;
    }

    public void BtnBackOnClick()
    {
        this.gameObject.SetActive(false);
       // ChangeType = -1;
    }

    public void ClouseAlert()
    {
        Alert.SetActive(false);
    }

    public void ThongBaoLoi(string msg)
    {
        TxtAlert.text = msg;
        Alert.SetActive(true);
    }

    public void MuaBangGem()
    {
        if (MyInfo.GEM >= GiaNangCap)
        {
            //TxtAlert.text = "Nâng cấp thành công";
            //Alert.SetActive(true);

            API.Instance.RequesNangCapVatPham(MyId_Rieng, RspNangCapVatPham);


         //   API.Instance.RequesSuaChuaVatPham(MyId_Rieng, RspSuaChuaVatPham);
        }
        else
        {
            TxtAlert.text = "Bạn không đủ GEM nâng cấp vật phẩm này";
            Alert.SetActive(true);
        }


    }

    public void SuaNhaBangGem()
    {
        if (MyInfo.CHIP >= GiaNangCap)
        {
            //TxtAlert.text = "Nâng cấp thành công";
            //Alert.SetActive(true);

            API.Instance.RequesSuaChuaVatPham(MyId_Rieng, RspSuaChuaVatPham);
        }
        else
        {
            TxtAlert.text = "Bạn không đủ GEM sửa vật phẩm này";
            Alert.SetActive(true);
        }


    }



    void RspSuaChuaVatPham(string _json)
    {
        Debug.LogError("DU LIEU sua SAN PHAM: " + _json);
        JSONNode node = JSONNode.Parse(_json);
        string sta = node["status"].Value;
        long chip = long.Parse(node["chip"].Value);
        long gem = long.Parse(node["gem"].Value);
        string id = node["asset_id"].Value;
        string level = node["level"].Value;
        long mau = long.Parse(node["health"].Value);
        long mautong = long.Parse(node["max_health"].Value);
        //vd mau


        Debug.LogWarning("fffffffffffffff --------------- " + id + "-------MyId_Rieng---------------- " + MyId_Rieng);
        long level_long = long.Parse(level);
        if (sta == "1")
        {
          //  upgradeDuLieuVatPhamMoiNangCap(id, level_long);

            upgradeDuLieu_Mau_VatPhamMoiNangCap(id, mau, mautong);
            MyInfo.CHIP = chip;
            MyInfo.GEM = gem;
            estate.CapNhatLEFT();
            My_Item_SoHuu it = DataHelper.DuLieuSoHuu_V2[id];

            bool inmad = it.InMad;
            if (inmad == true)
            {
                int vitri = 0;
                //long mauhientai = MauSauNangCap;
                //long mautong = MauSauNangCap;
                foreach (var item in DataHelper.DuLieuMap)
                {
                    string idVatPham = item.Value.id;
                    if (idVatPham == id)
                    {
                        vitri = item.Key;
                        item.Value.MauHienTai = mau;
                        item.Value.MauTong = mautong;
                        MyDoanhThu = item.Value.DoanhThu;
                        //mauhientai = item.Value.MauHienTai;
                        //mautong = item.Value.MauTong;
                    }
                }

                popupAlert.Show("Sửa vật phẩm Thành Công!", popupAlert.Hide);
                PanelScroll.SetActive(false);
                Mad.EditInfoItemInMad(vitri, level_long, MyDoanhThu, mau, mautong, true);
            }
            else
            {
                popupAlert.Show("Sửa vật phẩm Thành Công!\n" + "Mời bạn kiểm kho.", popupAlert.Hide);
                if (Type == "house")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemHomeCroll();
                }
                else if (Type == "shop")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemShopInScoll();
                }
                else if (Type == "car")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemCarInScoll();
                }
            }


            this.gameObject.SetActive(false);
        }
        else
        {
            string MSG = node["msg"].Value;
            // PaneBuy.ThongBaoLoi(MSG);
            TxtAlert.text = MSG;
            Alert.SetActive(true);
            Debug.LogWarning("LỔI SỬA CHỮA SAN PHAM: " + MSG);
        }

        //// Debug.LogWarning("st= " + sta + "  chip= " + chip + "   gem= " + gem + "   id= " + idTONG);
    }


    


    void RspNangCapVatPham(string _json)
    {
        Debug.LogError("DU LIEU NÂNG CẤP SAN PHAM: " + _json);
        JSONNode node = JSONNode.Parse(_json);
        string sta = node["status"].Value;
        long chip = long.Parse(node["chip"].Value);
        long gem = long.Parse(node["gem"].Value);
        string id = node["asset_id"].Value;
        string level = node["level"].Value;

        //vd mau
 

        Debug.LogWarning("fffffffffffffff --------------- " + id + "-------MyId_Rieng---------------- " + MyId_Rieng);
        long level_long = long.Parse(level);
        if (sta == "1")
        {
            upgradeDuLieuVatPhamMoiNangCap(id, level_long);
            MyInfo.CHIP = chip;

            long g = MyInfo.GEM - gem;

            Debug.LogError("Số gem mất thật sự   " + g);
            MyInfo.GEM = gem;


            estate.CapNhatLEFT();
            My_Item_SoHuu it = DataHelper.DuLieuSoHuu_V2[id];

            bool inmad = it.InMad;
            if (inmad == true)
            {
                int vitri = 0;
                long mauhientai = MauSauNangCap;
                long mautong = MauSauNangCap;
                foreach (var item in DataHelper.DuLieuMap)
                {
                    string idVatPham = item.Value.id;
                    if (idVatPham == id)
                    {
                        vitri = item.Key;
                        item.Value.Level = level_long;
                        item.Value.DoanhThu = MyDoanhThu;
                        //mauhientai = item.Value.MauHienTai;
                        //mautong = item.Value.MauTong;
                    }
                }
                popupAlert.Show("Nâng Cấp Thành Công!", popupAlert.Hide);

                PanelScroll.SetActive(false);
                Mad.EditInfoItemInMad(vitri, level_long, MyDoanhThu, mauhientai, mautong, true);
            }
            else
            {

                popupAlert.Show("Nâng Cấp Thành Công!\n" +"Mời bạn kiểm kho.", popupAlert.Hide);

                if (Type == "house")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemHomeCroll();
                }
                else if (Type == "shop")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemShopInScoll();
                }
                else if (Type == "car")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemCarInScoll();
                }
            }

           
            this.gameObject.SetActive(false);
        }
        else
        {
            string MSG = node["msg"].Value;
            // PaneBuy.ThongBaoLoi(MSG);
            TxtAlert.text = MSG;
            Alert.SetActive(true);
            Debug.LogWarning("LỔI NÂNG CẤP SAN PHAM: " + MSG);
        }

       // Debug.LogWarning("st= " + sta + "  chip= " + chip + "   gem= " + gem + "   id= " + idTONG);
    }


    void upgradeDuLieuVatPhamMoiNangCap(string Id, long level)
    {
        foreach (var item in DataHelper.DuLieuSoHuu_V2)
        {
            if (Id == item.Key)
            {
                
                item.Value.Level = level;
                item.Value.MauTong = MauSauNangCap;
                item.Value.MauHienTai = MauSauNangCap;

            }
            
        }

        foreach (My_Item_SoHuu _item in DataHelper.Array_DuLieuSoHuu)
        {

            My_Item_SoHuu it = _item;
            if (it.idRieng == Id)
            {
                _item.Level = level;
                _item.MauHienTai = MauSauNangCap;
                _item.MauTong = MauSauNangCap;
            }
        }
    }


    void upgradeDuLieu_Mau_VatPhamMoiNangCap(string Id, long mau, long mautong)
    {
        foreach (var item in DataHelper.DuLieuSoHuu_V2)
        {
            if (Id == item.Key)
            {

                item.Value.MauHienTai = mau;
                item.Value.MauTong = mautong;                
            }

        }

        foreach (My_Item_SoHuu _item in DataHelper.Array_DuLieuSoHuu)
        {

            My_Item_SoHuu it = _item;
            if (it.idRieng == Id)
            {
                _item.MauHienTai = mau;
                _item.MauTong = mautong;
            }
        }
    }

}

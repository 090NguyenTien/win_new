﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using BaseCallBack;
using System;

public class MadControll : MonoBehaviour {
    [SerializeField]
    Image ImgHome;
    [SerializeField]
    EstateControll estateControll;



    [SerializeField]
    public List<Sprite> SpritesHome;
    [SerializeField]
    public List<Sprite> SpriteItemHome;
    [SerializeField]
    public List<Sprite> SpriteShops;
    [SerializeField]
    public List<Sprite> SpriteCar;
    [SerializeField]
    public List<Sprite> SpriteItemCar;
    [SerializeField]
    GameObject imgAdd_Shop_1, imgAdd_Shop_2, imgAdd_Shop_3, imgAdd_Shop_4, imgAdd_Shop_5;

    [SerializeField]
    GameObject imgAdd_Car_1, imgAdd_Car_2;

    public string ID_House;

    [SerializeField]
    UserInfo userInfo;

    [SerializeField]
    Text levelHome, levelShop1, levelShop2, levelShop3, levelShop4, levelShop5, levelCar1, levelCar2;
    
    string StringHomeUnClock = "";
    string StringShopUnClock = "";
    string StringCarUnClock = "";

    string StringView = "3,1,2,3,4,5,6,7";


    [SerializeField]
    GameObject ViTri_SinhBoom;
    [SerializeField]
    GameObject ItemBoom;



    public string iD_Shop_1, iD_Shop_2, iD_Shop_3, iD_Shop_4, iD_Shop_5;

    public string iD_Car_1, iD_Car_2;

    public Image Shop_1, Shop_2, Shop_3, Shop_4, Shop_5;

    public Image Car_1, Car_2;

    public int ID_CarToChange;

    public int ID_ShopToChange;

    public Sprite SprKhung;
    public Sprite SprNhaMacDinh;


    [SerializeField]
    GameObject House_Info, Shop1_info, Shop2_info, Shop3_info, Shop4_info, Shop5_info, Car1_Info, Car2_Info;
    [SerializeField]
    Text levelHouse, levelShop_1, levelShop_2, levelShop_3, levelShop_4, levelShop_5, levelCar_1, levelCar_2;
    [SerializeField]
    Text DT_house, DT_Shop_1, DT_Shop_2, DT_Shop_3, DT_Shop_4, DT_Shop_5, DT_Car_1, DT_Car_2;
    [SerializeField]
    Image Slider_Home, Slider_S_1, Slider_S_2, Slider_S_3, Slider_S_4, Slider_S_5, Slider_C_1, Slider_C_2;

    


    [SerializeField]
    GameObject eff_chinh, NoNha,ThanhMau, Khieng, TamKhieng, BtnCloseMap, BtnClosePanel, PanelKetQua, BanPhaNha;
    [SerializeField]
    Text TxtChipSauKhiBan, TxtChipReceive;
    [SerializeField]
    FortuneWheelManager _fortuneWheelManager;

    int KetQuaBan = -1;

    [SerializeField]
    public Text txtShield;

    void Start()
    {
        //  Init();
        //XemNha(StringView);
        //txtShield.text = "0";
    }
    public void UpdateShield(int value)
    {
        txtShield.text = value.ToString();
    }
    internal void TruMauNhaBitanCong(string idRieng, long curHealth, long curMaxHealth, bool Khieng = false)
    {
        Update_health_InfoItem_InMap_ById(idRieng, curHealth, curMaxHealth, true, Khieng);
    }

    onCallBackString CallBackItemClick;
    string curItemTaiSanClick="";

    public void AddCallBackClickItem(onCallBackString _callbackItemClick)
    {
        CallBackItemClick = _callbackItemClick;
        estateControll.AddCallBackClickItem(CallBackEstateClick);
        estateControll.IsPirate = true;
    }



    private void CallBackEstateClick(int type)
    {
        string idItem="";
        switch (type)
        {
            
            case 1: idItem = iD_Shop_1; break;
            case 2: idItem = iD_Shop_2; break;
            case 3: idItem = iD_Shop_3; break;
            case 4: idItem = iD_Shop_4; break;
            case 5: idItem = iD_Shop_5; break;
            case 6: idItem = ID_House; break;
            case 7: idItem = iD_Car_1; break;
            case 8: idItem = iD_Car_2; break;
        }

        CallBackItemClick(idItem);
    }

    public void XemTaiSan(string idUser)
    {
        //HienThiMadMacDinh_Enemy();
        ClouseItemInfo();
        API.Instance.RequestVatPham_SoHuu_Enemy(idUser, RspLayDuLieuVatPham_SoHuu);
    }

    void Update_health_InfoItem_InMap_ById(string idRieng, long mauhientai, long mautong, bool enemy = false, bool haveKhieng = false)
    {
        if (enemy == false)
        {
            foreach (var item in DataHelper.DuLieuMap)
            {
                Item_InMad it = item.Value;
                int pos = item.Key;
                if (it.id == idRieng)
                {
                    EditInfoItemInMad(pos, it.Level, it.DoanhThu, mauhientai, mautong, true);


                }
            }
        }
        else
        {
            foreach (var item in DataHelper.DuLieuMap_Enemy)
            {
                Item_InMad it = item.Value;
                int pos = item.Key;
                GameObject obj_efect = null;
                if (it.id == idRieng)
                {
                    if (pos == 0)
                    {
                        eff_chinh.transform.position = ImgHome.gameObject.transform.position;
                        if (haveKhieng == true)
                        {
                            Khieng.transform.position = eff_chinh.transform.position;
                            TaoBoom(TamKhieng, true);
                        }
                        else
                        {
                            TaoBoom(ImgHome.gameObject);
                        }
                        

                        
                    //    obj_efect = eff_home;
                    }
                    else if (pos == 1)
                    {
                        eff_chinh.transform.position = Shop_1.gameObject.transform.position;
                        if (haveKhieng == true)
                        {
                            Khieng.transform.position = eff_chinh.transform.position;
                            TaoBoom(TamKhieng, true);
                        }
                        else
                        {
                            TaoBoom(Shop_1.gameObject);
                        }

                        
                       

                        //obj_efect = eff_S_1;
                    }
                    else if (pos == 2)
                    {
                        
                        // obj_efect = eff_S_2;
                        eff_chinh.transform.position = Shop_2.gameObject.transform.position;
                        if (haveKhieng == true)
                        {
                            Khieng.transform.position = eff_chinh.transform.position;
                            TaoBoom(TamKhieng, true);
                        }
                        else
                        {
                            TaoBoom(Shop_2.gameObject);
                        }
                       
                    }
                    else if (pos == 3)
                    {
                        
                        //obj_efect = eff_S_3;
                        eff_chinh.transform.position = Shop_3.gameObject.transform.position;
                        if (haveKhieng == true)
                        {
                            Khieng.transform.position = eff_chinh.transform.position;
                            TaoBoom(TamKhieng, true);
                        }
                        else
                        {
                            TaoBoom(Shop_3.gameObject);
                        }
                        
                    }
                    else if (pos == 4)
                    {
                        
                        //obj_efect = eff_S_4;
                        eff_chinh.transform.position = Shop_4.gameObject.transform.position;
                        if (haveKhieng == true)
                        {
                            Khieng.transform.position = eff_chinh.transform.position;
                            TaoBoom(TamKhieng, true);
                        }
                        else
                        {
                            TaoBoom(Shop_4.gameObject);
                        }

                        
                    }
                    else if (pos == 5)
                    {
                       
                        //obj_efect = eff_S_5;
                        eff_chinh.transform.position = Shop_5.gameObject.transform.position;
                        if (haveKhieng == true)
                        {
                            Khieng.transform.position = eff_chinh.transform.position;
                            TaoBoom(TamKhieng, true);
                        }
                        else
                        {
                            TaoBoom(Shop_5.gameObject);
                        }
                        
                    }
                    else if (pos == 6)
                    {
                        
                        // obj_efect = eff_C_1;
                        eff_chinh.transform.position = Car_1.gameObject.transform.position;
                        if (haveKhieng == true)
                        {
                            Khieng.transform.position = eff_chinh.transform.position;
                            TaoBoom(TamKhieng, true);
                        }
                        else
                        {
                            TaoBoom(Car_1.gameObject);
                        }
                        
                    }
                    else if (pos == 7)
                    {
                       
                       // obj_efect = eff_C_2;
                        eff_chinh.transform.position = Car_1.gameObject.transform.position;
                        if (haveKhieng == true)
                        {
                            Khieng.transform.position = eff_chinh.transform.position;
                            TaoBoom(TamKhieng, true);
                        }
                        else
                        {
                            TaoBoom(Car_2.gameObject);
                        }
                       
                    }


                    Debug.LogWarning("obj_efect-------------------" + obj_efect);




                    if (haveKhieng == true)
                    {
                        //Khieng.transform.position = eff_chinh.transform.position;
                        KetQuaBan = 0;
                        StartCoroutine(DungKhieng());
                    }
                    else
                    {
                        //mauhientai = 0;
                        if (mauhientai == 0)
                        {
                            KetQuaBan = 2;
                            StartCoroutine(offfEffectChipWin(pos, it.Level, it.DoanhThu, mauhientai, mautong, true));
                        }
                        else
                        {
                            KetQuaBan = 1;
                            StartCoroutine(offfEffectChipWin(pos, it.Level, it.DoanhThu, mauhientai, mautong));
                        }
                    }



                    //KetQuaBan = 0;
                    //StartCoroutine(DungKhieng());





                    //j_efect.SetActive(false);

                }
            }
        }
    }

    private IEnumerator offfEffectChipWin(int pos, long leve, long doanhtthu, long mau, long mautong, bool NoNha = false)
    {
        if (NoNha == false)
        {
            yield return new WaitForSeconds(3f);
            eff_chinh.SetActive(true);
            yield return new WaitForSeconds(3f);
            eff_chinh.SetActive(false);
            EditInfoItemInMad(pos, leve, doanhtthu, mau, mautong, true);
        }
        else
        {
            yield return new WaitForSeconds(3f);
            ThanhMau_TaiViTri(pos);
            //NoNha_TaiViTri(pos);
            
            yield return new WaitForSeconds(0.5f);
            NoNha_TaiViTri(pos);
            //ThanhMau_TaiViTri(pos);
            yield return new WaitForSeconds(2f);           
            eff_chinh.SetActive(true);
            yield return new WaitForSeconds(3f);
            eff_chinh.SetActive(false);
            EditInfoItemInMad(pos, leve, doanhtthu, mau, mautong, false);
        }
        yield return new WaitForSeconds(1f);
        OpenPanelKetQua();


    }

    private IEnumerator DungKhieng()
    {
        Khieng.SetActive(true);
        yield return new WaitForSeconds(0.8f);
        Khieng.SetActive(false);
        OpenPanelKetQua();
    }


    void OpenPanelKetQua()
    {
        BanPhaNha.SetActive(false);
        if (KetQuaBan == 0)// khieng
        {
            TxtChipSauKhiBan.text = "ĐÁNG GHÉT!!! KHU NHÀ NÀY ĐÃ CÓ KHIÊNG BẢO VỆ...";
        }
        else if (KetQuaBan == 1)// tru mau
        {
            TxtChipSauKhiBan.text = "BẠN THU ĐƯỢC " + TxtChipReceive.text +" CHIP SAU LƯỢT BẮN NÀY.";
        }
        else if (KetQuaBan == 2) // sap nha
        {
            BanPhaNha.SetActive(true);
            TxtChipSauKhiBan.text = "ĐÃ BẮN SẬP ĐƯỢC CĂN CỨ ĐỊCH BẠN NHẬN ĐƯỢC 30% GIÁ TRỊ CĂN CỨ ẤY: " + TxtChipReceive.text + " CHIP.";
        }

        PanelKetQua.SetActive(true);

    }

    public void ClosePanelKetQua()
    {
        PanelKetQua.SetActive(false);
        TxtChipSauKhiBan.text = "";
        //BtnCloseMap.SetActive(true);
        _fortuneWheelManager.CloseTaiSan();
        BtnClosePanel.SetActive(true);
    }



    void TaoBoom(GameObject obj_biNem, bool ChamVatThe = false)
    {
        BtnCloseMap.SetActive(false);
        BtnClosePanel.SetActive(false);
        GameObject MyBoom = Instantiate(ItemBoom, ViTri_SinhBoom.transform) as GameObject;
        MyBoom.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        Transform targetMove = obj_biNem.transform;

        ItemBoomControll MyItem = MyBoom.GetComponent<ItemBoomControll>();

        MyItem.Init(targetMove);

        MyItem.NemBoom(NemThuong, 5, 0.5f, false, ChamVatThe);
    }

    void NemThuong()
    {

    }

    void HienThiMadMacDinh_Enemy()
    {
        Item_InMad itInMad_Home = new Item_InMad();
        itInMad_Home.id = "5dc03420e52b8e2d238b6045";
        itInMad_Home.idTong = "5dc03420e52b8e2d238b6045";
        itInMad_Home.Level = 0;
        itInMad_Home.DoanhThu = 0;

        DataHelper.DuLieuMap_Enemy.Add(0, itInMad_Home);

        for (int i = 1; i < 8; i++)
        {

            Item_InMad itInMad = new Item_InMad();
            itInMad.id = "";
            itInMad.idTong = "";
            itInMad.Level = 0;
            itInMad.DoanhThu = 0;


            DataHelper.DuLieuMap_Enemy.Add(i, itInMad);
        }
    }


    void RspLayDuLieuVatPham_SoHuu(string _json)
    {
        Debug.LogWarning("du lieu so huu enemy--- " + _json);
        DataHelper.DuLieuMap_Enemy.Clear();
        if (_json == "false")
        {          
            HienThiMadMacDinh_Enemy();
        }
        else
        {
            JSONNode node = JSONNode.Parse(_json);
            if (node["shield"].Value != "null")
            {
                UpdateShield(int.Parse(node["shield"].Value));
            }
            else
            {
                UpdateShield(0);
            }
            if (node["placed_assets"].Value == "null")
            {               
                HienThiMadMacDinh_Enemy();
            }
            else
            {                
                int cou = node["placed_assets"].Count;
                for (int i = 0; i < cou; i++)
                {
                    int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                    string a_id = node["placed_assets"][i]["id"].Value;
                    string a_id_tong = node["placed_assets"][i]["id_asset"]["$id"].Value;
                    long a_level = long.Parse(node["placed_assets"][i]["level"].Value);
                    long mau = long.Parse(node["placed_assets"][i]["health"].Value);
                    long mautong = long.Parse(node["placed_assets"][i]["max_health"].Value);

                    
                    //aaaa tim thay roi ne

                    Item_InMad itInMad = new Item_InMad();
                    itInMad.id = a_id;
                    itInMad.idTong = a_id_tong;
                    itInMad.Level = a_level;

                    // vi du mau:
                  //  Debug.LogWarning("coooooooooooooooooooooooojjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                    itInMad.MauHienTai = mau;
                    itInMad.MauTong = mautong;


                    string type = "house";

                    if (pos > 0 && pos < 6)
                    {
                        type = "shop";
                    }
                    else if (pos > 5 && pos < 8)
                    {
                        type = "car";
                    }

                    long doanhthu = GetDoanhThuByIDTong(a_id_tong, a_level, type);
                    itInMad.DoanhThu = doanhthu;


                    DataHelper.DuLieuMap_Enemy[pos] = itInMad;
                }

                for (int i = 0; i < 8; i++)
                {
                    if (DataHelper.DuLieuMap_Enemy.ContainsKey(i) == false)
                    {
                        Item_InMad itInMad = new Item_InMad();
                        itInMad.id = "";
                        itInMad.idTong = "";

                        itInMad.MauHienTai = 0;
                        itInMad.MauTong = 0;
                        
                        itInMad.Level = 0;
                        itInMad.DoanhThu = 0;

                        DataHelper.DuLieuMap_Enemy.Add(i, itInMad);
                    }
                }

            }
        }
        XemNha();        
    }



    public void XemNha()
    {
        ClouseItemInfo();
        ImgHome.sprite = SprNhaMacDinh;
        if (DataHelper.DuLieuMap_Enemy[0].id == "")
        {
            ID_House = "5dc03420e52b8e2d238b6045";
        }
        else
        {
            ID_House = DataHelper.DuLieuMap_Enemy[0].id;

        }

        if (DataHelper.DuLieuMap_Enemy[1].id == "")
        {
            iD_Shop_1 = "";
        }
        else
        {
            iD_Shop_1 = DataHelper.DuLieuMap_Enemy[1].id;
        }

        if (DataHelper.DuLieuMap_Enemy[2].id == "")
        {
            iD_Shop_2 = "";
        }
        else
        {
            iD_Shop_2 = DataHelper.DuLieuMap_Enemy[2].id;
        }

        if (DataHelper.DuLieuMap_Enemy[3].id == "")
        {
            iD_Shop_3 = "";
        }
        else
        {
            iD_Shop_3 = DataHelper.DuLieuMap_Enemy[3].id;
        }

        if (DataHelper.DuLieuMap_Enemy[4].id == "")
        {
            iD_Shop_4 = "";
        }
        else
        {
            iD_Shop_4 = DataHelper.DuLieuMap_Enemy[4].id;
        }

        if (DataHelper.DuLieuMap_Enemy[5].id == "")
        {
            iD_Shop_5 = "";
        }
        else
        {
            iD_Shop_5 = DataHelper.DuLieuMap_Enemy[5].id;
        }


        if (DataHelper.DuLieuMap_Enemy[6].id == "")
        {
            iD_Car_1 = "";
        }
        else
        {
            iD_Car_1 = DataHelper.DuLieuMap_Enemy[6].id;
        }

        if (DataHelper.DuLieuMap_Enemy[7].id == "")
        {
            iD_Car_2 = "";
        }
        else
        {
            iD_Car_2 = DataHelper.DuLieuMap_Enemy[7].id;
        }


        //string TenImgNha = DataHelper.DuLieuTenImgTheoID[ID_House];
        //API.Instance.Load_SprItem(TenImgNha, ImgHome);


        if (ID_House == "5dc03420e52b8e2d238b6045")
        {
            string TenImgNha = DataHelper.DuLieuTenImgTheoID[ID_House];
            API.Instance.Load_SprItem(TenImgNha, ImgHome);
        }
        else
        {
            string IdTong_Nha = DataHelper.DuLieuMap_Enemy[0].idTong;


            Debug.LogWarning("IdTong_Nha ---- " + IdTong_Nha);
            string TenImgNha = DataHelper.DuLieuTenImgTheoID[IdTong_Nha];
            API.Instance.Load_SprItem(TenImgNha, ImgHome);

            long level = DataHelper.DuLieuMap_Enemy[0].Level;
            long doanhthu = DataHelper.DuLieuMap_Enemy[0].DoanhThu;

            long mauhientai = DataHelper.DuLieuMap_Enemy[0].MauHienTai;
            long mautong = DataHelper.DuLieuMap_Enemy[0].MauTong;

            EditInfoItemInMad(0, level, doanhthu, mauhientai, mautong, true);

        }

        HienThiHinh_Enemy(Shop_1, iD_Shop_1, 1);
        HienThiHinh_Enemy(Shop_2, iD_Shop_2, 2);
        HienThiHinh_Enemy(Shop_3, iD_Shop_3, 3);
        HienThiHinh_Enemy(Shop_4, iD_Shop_4, 4);
        HienThiHinh_Enemy(Shop_5, iD_Shop_5, 5);

        HienThiHinh_Enemy(Car_1, iD_Car_1, 6, true);
        HienThiHinh_Enemy(Car_2, iD_Car_2, 7, true);
    }


    void ChangeSlider_Mau(Image sli_Mau, long MaxPoint, long MinPoint, long VipCollect)
    {
        long Tong = MaxPoint - MinPoint;
        long Diem = VipCollect - MinPoint;
 
        float KQ = (float)Diem / (float)Tong;
        Debug.LogWarning("Vo ChangeSlider_Mau vaf ra ket qua: " + KQ);
       sli_Mau.fillAmount = KQ;
    }



    public void InitSpr()
    {
        Dictionary<string, Sprite> t = DataHelper.dictSprite_House;
        SpritesHome.Clear();
        SpriteItemHome.Clear();
        foreach (var item in t)
        {
            SpritesHome.Add(item.Value);
            SpriteItemHome.Add(item.Value);
        }

        Dictionary<string, Sprite> sh = DataHelper.dictSprite_Shop;
        SpriteShops.Clear();
        SpriteShops.Add(SprKhung);
        foreach (var item in sh)
        {
            SpriteShops.Add(item.Value);
        }



        Dictionary<string, Sprite> ca = DataHelper.dictSprite_Car;
        SpriteCar.Clear();
        SpriteCar.Add(SprKhung);
        SpriteItemCar.Clear();
        SpriteItemCar.Add(SprKhung);
        foreach (var item in ca)
        {
            SpriteCar.Add(item.Value);
            SpriteItemCar.Add(item.Value);
        }


        DataHelper.IsInitShopVatPham = true;
      //  Debug.LogWarning("Da tao Hinh xong");
    }

    void ClouseItemInfo()
    {
       
        House_Info.SetActive(false);

        Shop1_info.SetActive(false);
        Shop2_info.SetActive(false);
        Shop3_info.SetActive(false);
        Shop4_info.SetActive(false);
        Shop5_info.SetActive(false);

        Car1_Info.SetActive(false);
        Car2_Info.SetActive(false);
    }


    public void Init()
    {
        // Lấy Data
        ClouseItemInfo();
        txtShield.text = MyInfo.KHIEN_PIRATE.ToString();

        if (DataHelper.DuLieuMap[0].id == "")
        {
            ID_House = "5dc03420e52b8e2d238b6045";
            Debug.LogWarning("VO TRUONG HOP 1 ---- " + DataHelper.DuLieuMap[0].id);
        }
        else
        {
            ID_House = DataHelper.DuLieuMap[0].id;
            Debug.LogWarning("VO TRUONG HOP 3 ---- " + DataHelper.DuLieuMap[0].id);

        }

     
        if (DataHelper.DuLieuMap[1].id == "")
        {
            iD_Shop_1 = "";
        }
        else
        {
            iD_Shop_1 = DataHelper.DuLieuMap[1].id;
        }

        if (DataHelper.DuLieuMap[2].id == "")
        {
            iD_Shop_2 = "";
        }
        else
        {
            iD_Shop_2 = DataHelper.DuLieuMap[2].id;
        }

        if (DataHelper.DuLieuMap[3].id == "")
        {
            iD_Shop_3 = "";
        }
        else
        {
            iD_Shop_3 = DataHelper.DuLieuMap[3].id;
        }

        if (DataHelper.DuLieuMap[4].id == "")
        {
            iD_Shop_4 = "";
        }
        else
        {
            iD_Shop_4 = DataHelper.DuLieuMap[4].id;
        }

        if (DataHelper.DuLieuMap[5].id == "")
        {
            iD_Shop_5 = "";
        }
        else
        {
            iD_Shop_5 = DataHelper.DuLieuMap[5].id;
        }


        if (DataHelper.DuLieuMap[6].id == "")
        {
            iD_Car_1 = "";
        }
        else
        {
            iD_Car_1 = DataHelper.DuLieuMap[6].id;
        }

        if (DataHelper.DuLieuMap[7].id == "")
        {
            iD_Car_2 = "";
        }
        else
        {
            iD_Car_2 = DataHelper.DuLieuMap[7].id;
        }




        //    StringHomeUnClock = _StringHomeUnClock;
        //    StringShopUnClock = _StringShopUnClock;
        //    StringCarUnClock = _StringCarUnClock;

        // Xữ lý Data

        // Hiển thị 

        Debug.LogWarning("idhouse ---- " + ID_House);
        

        if (ID_House == "5dc03420e52b8e2d238b6045")
        {
            string TenImgNha = DataHelper.DuLieuTenImgTheoID[ID_House];
            API.Instance.Load_SprItem(TenImgNha, ImgHome);
        }
        else
        {
            string IdTong_Nha = DataHelper.DuLieuSoHuu_V2[ID_House].idTong;
            Debug.LogWarning("IdTong_Nha ---- " + IdTong_Nha);
            string TenImgNha = DataHelper.DuLieuTenImgTheoID[IdTong_Nha];
            API.Instance.Load_SprItem(TenImgNha, ImgHome);

            long level = DataHelper.DuLieuMap[0].Level;
            long doanhthu = DataHelper.DuLieuMap[0].DoanhThu;

            long mauhientai = DataHelper.DuLieuMap[0].MauHienTai;
            long mautong = DataHelper.DuLieuMap[0].MauTong;

            EditInfoItemInMad(0, level, doanhthu, mauhientai, mautong, true);

        }

      
/*
        Debug.LogWarning("LAY HINH NHA NE  " + hinhNha);

        if (hinhNha == null)
        {
            ImgHome.sprite = SprNhaMacDinh;
        }
        else
        {
            ImgHome.sprite = hinhNha;
        }
*/
/*

        if (DataHelper.dictSprite_House[ID_House] == null)
        {
            ImgHome.sprite = SprNhaMacDinh;
        }
        else
        {
            ImgHome.sprite = DataHelper.dictSprite_House[ID_House];
        }
        
*/
        

        HienThiHinh(Shop_1, iD_Shop_1, 1);
        HienThiHinh(Shop_2, iD_Shop_2, 2);
        HienThiHinh(Shop_3, iD_Shop_3, 3);
        HienThiHinh(Shop_4, iD_Shop_4, 4);
        HienThiHinh(Shop_5, iD_Shop_5, 5);

        HienThiHinh(Car_1, iD_Car_1, 6, true);
        HienThiHinh(Car_2, iD_Car_2, 7, true);


       
     
        CheckShop();
        CheckCar();

        //XemTaiSan(MyInfo.ID);
    }


    public void HienThanhMau(Image img)
    {
        ThanhMau.transform.position = img.gameObject.transform.position;

        ItemNoNha it = ThanhMau.GetComponent<ItemNoNha>();
        it.InitThanhMau();
    }


    public void ChoNoNha(Image img)
    {
        NoNha.transform.position = img.gameObject.transform.position;

        ItemNoNha it = NoNha.GetComponent<ItemNoNha>();
        it.Init(img);
    }

    public void NoNha_TaiViTri(int Vitri)
    {
        if (Vitri == 0)
        {        
            ChoNoNha(ImgHome);
        }
        else if (Vitri == 1)
        {
            ChoNoNha(Shop_1);
        }
        else if (Vitri == 2)
        {
            ChoNoNha(Shop_2);
        }
        else if (Vitri == 3)
        {
            ChoNoNha(Shop_3);
        }
        else if (Vitri == 4)
        {
            ChoNoNha(Shop_4);
        }
        else if (Vitri == 5)
        {
            ChoNoNha(Shop_5);
        }
        else if (Vitri == 6)
        {
            ChoNoNha(Car_1);
        }
        else if (Vitri == 7)
        {
            ChoNoNha(Car_2);
        }
        EditInfoItemInMad(Vitri, 0, 0, 0, 0, false);
    }


    public void ThanhMau_TaiViTri(int Vitri)
    {
        if (Vitri == 0)
        {
            HienThanhMau(ImgHome);
        }
        else if (Vitri == 1)
        {
            HienThanhMau(Shop_1);
        }
        else if (Vitri == 2)
        {
            HienThanhMau(Shop_2);
        }
        else if (Vitri == 3)
        {
            HienThanhMau(Shop_3);
        }
        else if (Vitri == 4)
        {
            HienThanhMau(Shop_4);
        }
        else if (Vitri == 5)
        {
            HienThanhMau(Shop_5);
        }
        else if (Vitri == 6)
        {
            HienThanhMau(Car_1);
        }
        else if (Vitri == 7)
        {
            HienThanhMau(Car_2);
        }
    }



    public void EditInfoItemInMad(int Vitri, long level, long doanhthu, long mauhientai, long mautong, bool trangthai)
    {
        string lv = "Level " + level.ToString();
        
        string dt = Utilities.GetStringMoneyByLong(doanhthu);

        if (Vitri == 0)
        {
            levelHouse.text = lv;
            DT_house.text = dt;
            House_Info.SetActive(trangthai);
            ChangeSlider_Mau(Slider_Home, mautong, 0, mauhientai);

        }
        else if (Vitri == 1)
        {
            levelShop_1.text = lv;
            DT_Shop_1.text = dt;
            Shop1_info.SetActive(trangthai);
            ChangeSlider_Mau(Slider_S_1, mautong, 0, mauhientai);

        }
        else if (Vitri == 2)
        {
            levelShop_2.text = lv;
            DT_Shop_2.text = dt;
            Shop2_info.SetActive(trangthai);
            ChangeSlider_Mau(Slider_S_2, mautong, 0, mauhientai);

          
        }
        else if (Vitri == 3)
        {
            levelShop_3.text = lv;
            DT_Shop_3.text = dt;
            Shop3_info.SetActive(trangthai);
            ChangeSlider_Mau(Slider_S_3, mautong, 0, mauhientai);

        }
        else if (Vitri == 4)
        {
            levelShop_4.text = lv;
            DT_Shop_4.text = dt;
            Shop4_info.SetActive(trangthai);
            ChangeSlider_Mau(Slider_S_4, mautong, 0, mauhientai);

        }
        else if (Vitri == 5)
        {
            levelShop_5.text = lv;
            DT_Shop_5.text = dt;
            Shop5_info.SetActive(trangthai);
            ChangeSlider_Mau(Slider_S_5, mautong, 0, mauhientai);

        }
        else if (Vitri == 6)
        {
            levelCar_1.text = lv;
            DT_Car_1.text = dt;
            Car1_Info.SetActive(trangthai);
            ChangeSlider_Mau(Slider_C_1, mautong, 0, mauhientai);

        }
        else if (Vitri == 7)
        {
            levelCar_2.text = lv;
            DT_Car_2.text = dt;
            Car2_Info.SetActive(trangthai);
            ChangeSlider_Mau(Slider_C_2, mautong, 0, mauhientai);

        }


    }


    void HienThiHinh(Image img, string id, int vitri, bool iscar = false)
    {
        if (id != "")
        {
            string IdTong = DataHelper.DuLieuSoHuu_V2[id].idTong;

            string tenImg = DataHelper.DuLieuTenImgTheoID[IdTong];

            API.Instance.Load_SprItem(tenImg, img);

            long level = DataHelper.DuLieuMap[vitri].Level;
            long doanhthu = DataHelper.DuLieuMap[vitri].DoanhThu;

            long mauhientai = DataHelper.DuLieuMap[vitri].MauHienTai;
            long mautong = DataHelper.DuLieuMap[vitri].MauTong;

            EditInfoItemInMad(vitri, level, doanhthu, mauhientai, mautong, true);


        }
        else
        {
            img.sprite = SprKhung;
        }
    }


    void HienThiHinh_Enemy(Image img, string id, int vitri, bool iscar = false)
    {
        if (id != "")
        {
            string IdTong = DataHelper.DuLieuMap_Enemy[vitri].idTong;

            string tenImg = DataHelper.DuLieuTenImgTheoID[IdTong];

            API.Instance.Load_SprItem(tenImg, img);

            long level = DataHelper.DuLieuMap_Enemy[vitri].Level;
            long doanhthu = DataHelper.DuLieuMap_Enemy[vitri].DoanhThu;

            long mauhientai = DataHelper.DuLieuMap_Enemy[vitri].MauHienTai;
            long mautong = DataHelper.DuLieuMap_Enemy[vitri].MauTong;

            EditInfoItemInMad(vitri, level, doanhthu, mauhientai, mautong, true);



        }
        else
        {
            img.sprite = SprKhung;
        }
    }


    public void XuLySoLuongItemConLaiTrongKho()
    {/*
        for (int i = 1; i < 6; i++)
        {
            string id_shop_inMad = DataItemInMad[i];
            if (id_shop_inMad != "0")
            {
                string temp = DataItemShopUnClock[int.Parse(id_shop_inMad)];
                int t = int.Parse(temp);
                t--;
                DataItemShopUnClock[int.Parse(id_shop_inMad)] = t.ToString();
            }
            
        }
        for (int i = 6; i < 8; i++)
        {
            string id_car_inMad = DataItemInMad[i];
            if (id_car_inMad != "0")
            {
                string temp = DataItemCarUnClock[int.Parse(id_car_inMad)];
                int t = int.Parse(temp);
                t--;
                DataItemCarUnClock[int.Parse(id_car_inMad)] = t.ToString();
            }          
        }
        */
    }


    public void CheckShop()
    {
        if (iD_Shop_1 == "")
        {
            imgAdd_Shop_1.SetActive(true);
        }
        else
        {
            imgAdd_Shop_1.SetActive(false);
        }

        if (iD_Shop_2 == "")
        {
            imgAdd_Shop_2.SetActive(true);
        }
        else
        {
            imgAdd_Shop_2.SetActive(false);
        }

        if (iD_Shop_3 == "")
        {
            imgAdd_Shop_3.SetActive(true);
        }
        else
        {
            imgAdd_Shop_3.SetActive(false);
        }

        if (iD_Shop_4 == "")
        {
            imgAdd_Shop_4.SetActive(true);
        }
        else
        {
            imgAdd_Shop_4.SetActive(false);
        }

        if (iD_Shop_5 == "")
        {
            imgAdd_Shop_5.SetActive(true);
        }
        else
        {
            imgAdd_Shop_5.SetActive(false);
        }
    }


    public void CheckCar()
    {
        if (iD_Car_1 == "")
        {
            imgAdd_Car_1.SetActive(true);
        }
        else
        {
            imgAdd_Car_1.SetActive(false);
        }

        if (iD_Car_2 == "")
        {
            imgAdd_Car_2.SetActive(true);
        }
        else
        {
            imgAdd_Car_2.SetActive(false);
        }

      
    }





    public void ChangeHome (string IdTong, string id)
    {
        estateControll.CloseItemScroll();
       // ImgHome.sprite = DataHelper.dictSprite_House[Id];

        string Img = DataHelper.DuLieuTenImgTheoID[IdTong];
        API.Instance.Load_SprItem(Img, ImgHome);

        //     int count = DataHelper.KhoItem[IdTong];
       


        string curentId = ID_House;

        long level = 0;

        if (DataHelper.DuLieuSoHuu_V2.ContainsKey(id) && DataHelper.DuLieuSoHuu_V2[id].InMad == false)
        {
            //count--;
            //DataHelper.KhoItem[IdTong] = count;

            DataHelper.DuLieuSoHuu_V2[id].InMad = true;

            level = DataHelper.DuLieuSoHuu_V2[id].Level;

            if (DataHelper.DuLieuSoHuu_V2.ContainsKey(curentId))
            {
                DataHelper.DuLieuSoHuu_V2[curentId].InMad = false;
            }
        }



        ID_House = id;
        Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + id);
        API.Instance.RequestDatVatPhamTrenMad(id, 0, IdTong, level.ToString(), RspDatVatPhamTrenMad);

        
        estateControll.OpenBtnController();
    }




    void RspDatVatPhamTrenMad(string _json)
    {
        Debug.LogError("DU LIEU Dat Vat Pham: " + _json);
        JSONNode node = JSONNode.Parse(_json);


        string sta = node["status"].Value;
        Debug.LogWarning("DU LIEU Dat Vat Pham  STA: " + sta);
        if (sta == "1")
        {
            JSONNode data = node["placed_assets"];
            Debug.LogWarning("DU LIEU Dat Vat Pham  placed_assets: " + data);

            MyInfo.DOANHTHU = 0;
            int cou = data.Count;

            for (int i = 0; i < cou; i++)
            {
                int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                string a_id = node["placed_assets"][i]["id"].Value;
                string a_id_tong = node["placed_assets"][i]["id_asset"]["$id"].Value;
                long a_level = long.Parse(node["placed_assets"][i]["level"].Value);

                long mau = long.Parse(node["placed_assets"][i]["health"].Value);
                long mautong = long.Parse(node["placed_assets"][i]["max_health"].Value);

                Item_InMad itInMad = new Item_InMad();
                itInMad.id = a_id;
                itInMad.idTong = a_id_tong;
                itInMad.Level = a_level;

                // vd mau
                itInMad.MauHienTai = mau;
                itInMad.MauTong = mautong;

                string type = "house";

                if (pos > 0 && pos < 6)
                {
                    type = "shop";
                }
                else if (pos > 5 && pos < 8)
                {
                    type = "car";
                }

                long doanhthu = GetDoanhThuByIDTong(a_id_tong, a_level, type);
                itInMad.DoanhThu = doanhthu;
                MyInfo.DOANHTHU += doanhthu;
                // Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                DataHelper.DuLieuMap[pos] = itInMad;

                //long level = DataHelper.DuLieuMap[0].Level;
                //long doanhthu = DataHelper.DuLieuMap[0].DoanhThu;



                EditInfoItemInMad(pos, a_level, doanhthu, itInMad.MauHienTai, itInMad.MauTong, true);

            }

        }
        else
        {
            string smg = node["msg"].Value;
            Debug.LogWarning("DU LIEU Dat Vat Pham loiiiiiiiii: " + smg);
        }

        estateControll.CapNhatLEFT();
    }


    long GetDoanhThuByIDTong(string idTong, long level, string type)
    {
        long giaban = 0;
        if (type == "house")
        {
            giaban = DataHelper.DuLieuNha[idTong].chip;
        }
        else if (type == "shop")
        {
            giaban = DataHelper.DuLieuShop[idTong].chip;
        }
        else if (type == "car")
        {
            long giagem = DataHelper.DuLieuXe[idTong].gem;
            giaban = giagem * 5000000;
        }
        long thunhap = GetDoanhThuByGiaBan(giaban, level);
        return thunhap;
    }


    long GetDoanhThuByGiaBan(long GiaBan, long level)
    {
        long doanhthu = (GiaBan / 1000) * level;
        return doanhthu;
    }


    public void ChangeShop(string IdTong, string Id)
    {
        estateControll.CloseItemScroll();
        string curentId = "";
        long level = DataHelper.DuLieuSoHuu_V2[Id].Level;
        string level_string = level.ToString();
        if (ID_ShopToChange == 1)
        {
            curentId = iD_Shop_1;
            iD_Shop_1 = Id;
            CheckShop();
            //Shop_1.sprite = DataHelper.dictSprite_Shop[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[IdTong];
            API.Instance.Load_SprItem(Img, Shop_1);

            //  Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 1" );
            API.Instance.RequestDatVatPhamTrenMad(Id, 1, IdTong, level_string,  RspDatVatPhamTrenMad);
        }
        else if (ID_ShopToChange == 2)
        {
            curentId = iD_Shop_2;
            iD_Shop_2 = Id;
            CheckShop();
            //Shop_2.sprite = DataHelper.dictSprite_Shop[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[IdTong];
            API.Instance.Load_SprItem(Img, Shop_2);
            //   Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 2");
            API.Instance.RequestDatVatPhamTrenMad(Id, 2, IdTong, level_string, RspDatVatPhamTrenMad);
        }
        else if (ID_ShopToChange == 3)
        {
            curentId = iD_Shop_3;
            iD_Shop_3 = Id;
            CheckShop();
            // Shop_3.sprite = DataHelper.dictSprite_Shop[Id];
            string Img = DataHelper.DuLieuTenImgTheoID[IdTong];
            API.Instance.Load_SprItem(Img, Shop_3);


            //   Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 3");
            API.Instance.RequestDatVatPhamTrenMad(Id, 3, IdTong, level_string, RspDatVatPhamTrenMad);
        }
        else if (ID_ShopToChange == 4)
        {
            curentId = iD_Shop_4;
            iD_Shop_4 = Id;
            CheckShop();
            //  Shop_4.sprite = DataHelper.dictSprite_Shop[Id];
            string Img = DataHelper.DuLieuTenImgTheoID[IdTong];
            API.Instance.Load_SprItem(Img, Shop_4);

            //    Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 4");
            API.Instance.RequestDatVatPhamTrenMad(Id, 4, IdTong, level_string, RspDatVatPhamTrenMad);
        }
        else if (ID_ShopToChange == 5)
        {
            curentId = iD_Shop_5;
            iD_Shop_5 = Id;
            CheckShop();

            string Img = DataHelper.DuLieuTenImgTheoID[IdTong];
            API.Instance.Load_SprItem(Img, Shop_5);
            //  Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 5");
            API.Instance.RequestDatVatPhamTrenMad(Id, 5, IdTong, level_string, RspDatVatPhamTrenMad);
        }

      
        if (curentId != Id)
        {
          //  --DataHelper.KhoItem[IdTong];
            DataHelper.DuLieuSoHuu_V2[Id].InMad = true;

            if (curentId != "")
            {
                //++DataHelper.KhoItem[curentId];
                //Debug.LogError("sl kho curentId ===== " + DataHelper.KhoItem[curentId]);

                //Debug.LogError("sl kho curentId sau tru ===== " + DataHelper.KhoItem[curentId]);


                DataHelper.DuLieuSoHuu_V2[curentId].InMad = false;
            }
           
        }

      //  Debug.LogError("sl kho sau tru ===== " + DataHelper.KhoItem[IdTong]);
        estateControll.OpenBtnController();
       
    }

    public void DontChange()
    {
        estateControll.CloseItemScroll();
        estateControll.OpenBtnController();
    }



    public void ChangeCar(string IdTong, string Id)
    {
        estateControll.CloseItemScroll();

        string curentId = "";
        long level = DataHelper.DuLieuSoHuu_V2[Id].Level;
        string level_string = level.ToString();

        if (ID_CarToChange == 1)
        {
            curentId = iD_Car_1;
            iD_Car_1 = Id;
            CheckCar();
            //Car_1.sprite = DataHelper.dictSprite_Car[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[IdTong];
            API.Instance.Load_SprItem(Img, Car_1);

            //   Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 6");
            API.Instance.RequestDatVatPhamTrenMad(Id, 6, IdTong, level_string, RspDatVatPhamTrenMad);
        }
        else if (ID_CarToChange == 2)
        {
            curentId = iD_Car_2;
            iD_Car_2 = Id;
            CheckCar();
          //  Car_2.sprite = DataHelper.dictSprite_Car[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[IdTong];
            API.Instance.Load_SprItem(Img, Car_2);
            //  Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 7");
            API.Instance.RequestDatVatPhamTrenMad(Id, 7, IdTong, level_string, RspDatVatPhamTrenMad);
        }


    
        if (curentId != Id)
        {
            //  --DataHelper.KhoItem[IdTong];
            DataHelper.DuLieuSoHuu_V2[Id].InMad = true;

            if (curentId != "")
            {
                //++DataHelper.KhoItem[curentId];
                //Debug.LogError("sl kho curentId ===== " + DataHelper.KhoItem[curentId]);

                //Debug.LogError("sl kho curentId sau tru ===== " + DataHelper.KhoItem[curentId]);


                DataHelper.DuLieuSoHuu_V2[curentId].InMad = false;
            }

        }

      

        estateControll.OpenBtnController();
    }

    public void BuyHome(string Id)
    {      
      //  DataItemHomeUnClock[Id] = "1";
       // ID_House = Id;


       
    }

    public void BuyShop(string Id)
    {
      //  int countCurent = int.Parse(DataItemShopUnClock_Start[Id]);
       // countCurent++;
       // DataItemShopUnClock_Start[Id] = countCurent.ToString();
    }

    public void BuyCar(string Id)
    {
       // int countCurent = int.Parse(DataItemCarUnClock_Start[Id]);
       // countCurent++;
       // DataItemCarUnClock_Start[Id] = countCurent.ToString();
    }

    public string GetStringDataInMad()
    {
        string Data = ID_House.ToString() + "," + iD_Shop_1.ToString() + "," + iD_Shop_2.ToString() + "," + iD_Shop_3.ToString() + "," + iD_Shop_4.ToString() + "," + iD_Shop_5.ToString() + "," + iD_Car_1.ToString() + "," + iD_Car_2.ToString();
        return Data;
    }

    public string GetStringDataHome()
    {
        
        string data = "";
        /*
        for (int i = 0; i < DataItemHomeUnClock.Length; i++)
        {
            if (i < DataItemHomeUnClock.Length - 1)
            {
                data += DataItemHomeUnClock[i] + ",";
            }
            else
            {
                data += DataItemHomeUnClock[i];
            }            
        }
        */
        return data;
    }

    public string GetStringDataShop()
    {
        string data = "";
        /*
        for (int i = 0; i < DataItemShopUnClock_Start.Length; i++)
        {
            if (i < DataItemShopUnClock_Start.Length - 1)
            {
                data += DataItemShopUnClock_Start[i] + ",";
            }
            else
            {
                data += DataItemShopUnClock_Start[i];
            }
        }
        */
        return data;
    }

    public string GetStringDataCar()
    {
        string data = "";
        /*
        for (int i = 0; i < DataItemCarUnClock_Start.Length; i++)
        {
            if (i < DataItemCarUnClock_Start.Length - 1)
            {
                data += DataItemCarUnClock_Start[i] + ",";
            }
            else
            {
                data += DataItemCarUnClock_Start[i];
            }
        }
        */
        return data;
    }

    public void BackAndSave()
    {/*
        string dataInMad, dataHome, dataShop, dataCar;

        dataInMad = GetStringDataInMad();
        dataHome = GetStringDataHome();
        dataShop = GetStringDataShop();
        dataCar = GetStringDataCar();

        userInfo.DataInMad_Change = GetStringDataInMad();
        userInfo.StringHomeUnClock_Change = GetStringDataHome();
        userInfo.StringShopUnClock_Change = GetStringDataShop();
        userInfo.StringCarUnClock_Change = GetStringDataCar();

        Debug.LogWarning("dataInMad = " + dataInMad);
        Debug.LogWarning("dataHome = " + dataHome);
        Debug.LogWarning("dataShop = " + dataShop);
        Debug.LogWarning("dataCar = " + dataCar);
        */
    }


}

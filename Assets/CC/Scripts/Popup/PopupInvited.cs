﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using System.Collections.Generic;

public class PopupInvited : MonoBehaviour {

        
    List<Hashtable> ListInvitePlayGame = new List<Hashtable>();

    [SerializeField]
    private GameObject panel, ScrollInvitePlayGame, ContainerInvite;
    [SerializeField]
    ItemInvitePlay itemInvitePlay;
	[SerializeField]
	Text txtRoomID, txtPlayer, txtBet, txtGame;// txtContent;
    [SerializeField]
    TMPro.TextMeshProUGUI txtNameUser_;

    [SerializeField]
	Button btnCancel, btnOK;

	onCallBack cancelOnClick;
	onCallBackIntString okOnClick;

	int roomID;
	string pass = "";

	long betRequire;

    void Start()
    {
        ScrollInvitePlayGame.SetActive(false);
    }

	public void Init(onCallBack _cancel, onCallBackIntString _ok)
	{
			btnCancel.onClick.AddListener (CancelOnClick);
			btnOK.onClick.AddListener (OkOnClick);

			cancelOnClick = _cancel;
			okOnClick = _ok;
	}

	public void Show(string _username, int _roomID, int _gameID, int _roomLevel, long _bet, long _betRequire, string _pass = "")
		{
		if (!SettingHelper.Instance.INVITED)
			return;
        Hashtable itemInvite = new Hashtable();
        itemInvite.Add("_username", _username);
        itemInvite.Add("_roomID", _roomID);
        itemInvite.Add("_gameID", _gameID);
        itemInvite.Add("_roomLevel", _roomLevel);
        itemInvite.Add("_bet", _bet);
        itemInvite.Add("_betRequire", _betRequire);
        itemInvite.Add("_pass", _pass);
        ListInvitePlayGame.Add(itemInvite);
        return;
				roomID = _roomID;

				panel.SetActive (true);


		//txtPlayer.text = _username;
        txtNameUser_.text = _username;

        txtGame.text = GameHelper.GetNameGame ((GAMEID)_gameID);
		txtRoomID.text = _roomID.ToString ();
		txtBet.text = _bet.ToString ();

		betRequire = _betRequire;
		pass = _pass;
//				txtContent.text = content;
	}
	public void Hide()
	{
		panel.SetActive (false);
	}

	void CancelOnClick()
	{
		cancelOnClick ();
	}
	void OkOnClick()
	{
	    if (MyInfo.CHIP < betRequire)
		    okOnClick (-1);
	    else
		    okOnClick (roomID, pass);
	}
    public void ShowListInvitePlayGame()
    {
        if (ScrollInvitePlayGame.activeSelf == false)
        {
            for (int z = 0; z < ContainerInvite.transform.childCount; z++)
            {
                ItemInvitePlay itemTrsf = (ContainerInvite.transform.GetChild(z)).GetComponent<ItemInvitePlay>();
                Destroy(itemTrsf.gameObject);
            }

            ScrollInvitePlayGame.SetActive(true);
            //Fill Data Into Scroll List
            foreach (Hashtable item in ListInvitePlayGame)
            {
                ItemInvitePlay invitePlay = Instantiate(itemInvitePlay, ContainerInvite.transform) as ItemInvitePlay;
                invitePlay.Init(item);
            }
        }
        else
        {
            ScrollInvitePlayGame.SetActive(false);
        }
    }
}
/* ========= CONTROLLER =========

		[SerializeField]
		PopupInvited popupInvited;


				popupInvited.Init (BtnCancelInvitedOnClick, BtnOkInvitedOnClick);

	void BtnCancelInvitedOnClick ()
	{
				popupInvited.Hide ();
	}


		void RspInvited(GamePacket _param)
		{
				string s = _param.GetString ("inv");
				string[] info = s.Split ('#');

				string name = info [0];
				int roomID = int.Parse (info [1]);
				int gameID = int.Parse (info [2]);
				int roomLv = int.Parse (info [3]);

				popupInvited.Show (name, roomID, gameID, roomLv);
		}

case CommandKey.INVITE:
						RspInvited (param);
						break;
						

 */

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using BaseCallBack;
using SimpleJSON;
using UnityEngine.SceneManagement;
using System;
using Com.TheFallenGames.OSA.Util.IO;
using UnityEngine.Events;

public class ShopBorderAvatar : MonoBehaviour {

    [SerializeField]
    GameObject ItemBuyBorAvatar, ContentAvatar, ItemNha, ItemShop, ItemXe, ItemAvoid, ItemColor,  ContentNha, ContentShop, ContentXe, ContentAvoid, CrollKhung, CrollNha, CrollShop, CrollXe,
        ContentAvoidScroll, ContentMauChu, ContentMauChuScroll;
    [SerializeField]
    GameObject BtnKhung, BtnNha, BtnShop, BtnXe, BtnAvoidItem, BtnMauChu;
    [SerializeField]
    public GameObject PanelBuy, PannelBuyAvoidItem;

    [SerializeField]
    GameObject PanelThongBaoMuaVatPhamThanhCong;
    [SerializeField]
    Text TxtMuaThanhCong;

    [SerializeField]
    Text TxtChip, TxtGem, TxtNameUser;

    [SerializeField]
    TMPro.TextMeshProUGUI txtNameUser_;

    [SerializeField]
    HomeControllerV2 Home;
    [SerializeField]
    LobbyView _LobbyView;
    [SerializeField]
    Image Avatar, Border;

    private static ShopBorderAvatar instance;

    [SerializeField]
    PopupAlertManager popupAlert;

    [SerializeField]
    GameObject BuyCardAvoid, nameItemAvoid, Price30, Price60, Price90, Price120, PriceInapp30, PriceInapp60, PriceInapp90, PriceInapp120,
        Btn30, Btn60, Btn90, Btn120, BtnInapp30, BtnInapp60, BtnInapp90, BtnInapp120;

    public static ShopBorderAvatar Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject obj = new GameObject("ShopBorderAvatar");
                instance = obj.AddComponent<ShopBorderAvatar>();
            }
            return instance;
        }
    }


    

    void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);

        instance = this;

        DontDestroyOnLoad(gameObject);

        popupAlert.Init();
        popupAlert.gameObject.SetActive(false);

        InitTop();
        //BuyCardAvoid.SetActive(GameHelper.EnablePayment);
    }


    public void InitTop()
    {
        //Avatar.sprite = DataHelper.GetAvatar(MyInfo.AvatarName);
        StartCoroutine(DataHelper.UpdateAvatarThread(API.PREFIX_AVT + MyInfo.AvatarName, Avatar));
        if (DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName) != null)
        {
            Border.sprite = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
        }        
       // TxtNameUser.text = MyInfo.NAME;
        txtNameUser_.text = MyInfo.NAME;
        UpDatePrice();
    }





    public void BtnKhungAvataClick()
    {
        BtnKhung.transform.GetChild(0).gameObject.SetActive(true);
        BtnNha.transform.GetChild(0).gameObject.SetActive(false);
        BtnShop.transform.GetChild(0).gameObject.SetActive(false);
        BtnXe.transform.GetChild(0).gameObject.SetActive(false);
        BtnAvoidItem.transform.GetChild(0).gameObject.SetActive(false);
        BtnMauChu.transform.GetChild(0).gameObject.SetActive(false);

        CrollKhung.SetActive(true);
        CrollShop.SetActive(false);
        CrollXe.SetActive(false);
        CrollNha.SetActive(false);
        ContentAvoidScroll.SetActive(false);
        ContentMauChuScroll.SetActive(false);
    }


    public void ThongBaoMuaThanhCong()
    {
        TxtMuaThanhCong.text = "Giao dịch thành công.";
        PanelThongBaoMuaVatPhamThanhCong.SetActive(true);
    }

    public void Close_ThongBaoMuaThanhCong()
    {
        PanelThongBaoMuaVatPhamThanhCong.SetActive(false);
    }


    public void InitScroll_Nha()
    {
        int cn = ContentNha.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentNha.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
        foreach (var item in DataHelper.DuLieuNha)
        {

            if (item.Key != "5dc03420e52b8e2d238b6045")
            {
                GameObject Obj = Instantiate(ItemNha) as GameObject;
                // Obj.transform.SetParent(Content.transform);
                Obj.GetComponent<RectTransform>().SetParent(ContentNha.GetComponent<RectTransform>().transform);
                Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

                ItemAssetInShop MyItem = Obj.GetComponent<ItemAssetInShop>();
                string keyID = item.Key;
                MyItem.Init(keyID, "nha");
            }

        }

        CrollKhung.SetActive(false);
        CrollShop.SetActive(false);
        CrollXe.SetActive(false);
        ContentAvoidScroll.SetActive(false);
        ContentMauChuScroll.SetActive(false);
        CrollNha.SetActive(true);
        BtnKhung.transform.GetChild(0).gameObject.SetActive(false);
        BtnNha.transform.GetChild(0).gameObject.SetActive(true);
        BtnShop.transform.GetChild(0).gameObject.SetActive(false);
        BtnXe.transform.GetChild(0).gameObject.SetActive(false);
        BtnAvoidItem.transform.GetChild(0).gameObject.SetActive(false);
        BtnMauChu.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void InitScroll_Shop()
    {
        int cn = ContentShop.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentShop.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }

        foreach (var item in DataHelper.DuLieuShop)
        {


            GameObject Obj = Instantiate(ItemShop) as GameObject;
            // Obj.transform.SetParent(Content.transform);
            Obj.GetComponent<RectTransform>().SetParent(ContentShop.GetComponent<RectTransform>().transform);
            Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            ItemAssetInShop MyItem = Obj.GetComponent<ItemAssetInShop>();




            string keyID = item.Key;


            MyItem.Init(keyID, "shop");
        }

        CrollKhung.SetActive(false);
        CrollShop.SetActive(true);
        CrollXe.SetActive(false);
        ContentAvoidScroll.SetActive(false);
        ContentMauChuScroll.SetActive(false);
        CrollNha.SetActive(false);
        BtnKhung.transform.GetChild(0).gameObject.SetActive(false);
        BtnNha.transform.GetChild(0).gameObject.SetActive(false);
        BtnShop.transform.GetChild(0).gameObject.SetActive(true);
        BtnXe.transform.GetChild(0).gameObject.SetActive(false);
        BtnAvoidItem.transform.GetChild(0).gameObject.SetActive(false);
        BtnMauChu.transform.GetChild(0).gameObject.SetActive(false);
    }




    public void InitScroll_Xe()
    {
        int cn = ContentXe.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentXe.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }

        foreach (var item in DataHelper.DuLieuXe)
        {


            GameObject Obj = Instantiate(ItemXe) as GameObject;
            // Obj.transform.SetParent(Content.transform);
            Obj.GetComponent<RectTransform>().SetParent(ContentXe.GetComponent<RectTransform>().transform);
            Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            ItemAssetInShop MyItem = Obj.GetComponent<ItemAssetInShop>();




            string keyID = item.Key;


            MyItem.Init(keyID, "xe");
        }

        CrollKhung.SetActive(false);
        CrollShop.SetActive(false);
        CrollXe.SetActive(true);
        ContentAvoidScroll.SetActive(false);
        ContentMauChuScroll.SetActive(false);
        CrollNha.SetActive(false);
        BtnKhung.transform.GetChild(0).gameObject.SetActive(false);
        BtnNha.transform.GetChild(0).gameObject.SetActive(false);
        BtnShop.transform.GetChild(0).gameObject.SetActive(false);
        BtnXe.transform.GetChild(0).gameObject.SetActive(true);
        BtnAvoidItem.transform.GetChild(0).gameObject.SetActive(false);
        BtnMauChu.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void InitScroll_AvoidItem()
    {
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.CallServiceGetProductInAppId();
        int cn = ContentAvoid.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentAvoid.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
        foreach (var item in DataHelper.AvoidItems)
        {
            GameObject Obj = Instantiate(ItemAvoid) as GameObject;
            Obj.GetComponent<RectTransform>().SetParent(ContentAvoid.GetComponent<RectTransform>().transform);
            Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            Obj.GetComponent<ItemAvoid>().init((AvoidItem)item, avoidItemClick);
        }

        CrollKhung.SetActive(false);
        CrollShop.SetActive(false);
        CrollXe.SetActive(false);
        CrollNha.SetActive(false);
        ContentAvoid.SetActive(true);
        ContentAvoidScroll.SetActive(true);
        ContentMauChuScroll.SetActive(false);
        BtnKhung.transform.GetChild(0).gameObject.SetActive(false);
        BtnNha.transform.GetChild(0).gameObject.SetActive(false);
        BtnShop.transform.GetChild(0).gameObject.SetActive(false);
        BtnXe.transform.GetChild(0).gameObject.SetActive(false);
        BtnAvoidItem.transform.GetChild(0).gameObject.SetActive(true);
        BtnMauChu.transform.GetChild(0).gameObject.SetActive(false);
    }
    public void InitScroll_MauChu()
    {        
        int cn = ContentMauChu.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentMauChu.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }

        for (int index = 0; index < DataHelper.ShopColorItems.color.Count; index++)
        {
            GameObject Obj = Instantiate(ItemColor) as GameObject;
            Obj.GetComponent<RectTransform>().SetParent(ContentMauChu.GetComponent<RectTransform>().transform);
            Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            Obj.GetComponent<ItemColor>().init(index, colorItemClick);
        }

        CrollKhung.SetActive(false);
        CrollShop.SetActive(false);
        CrollXe.SetActive(false);
        CrollNha.SetActive(false);
        ContentAvoid.SetActive(false);
        ContentAvoidScroll.SetActive(false);
        ContentMauChu.SetActive(true);
        ContentMauChuScroll.SetActive(true);
        BtnKhung.transform.GetChild(0).gameObject.SetActive(false);
        BtnNha.transform.GetChild(0).gameObject.SetActive(false);
        BtnShop.transform.GetChild(0).gameObject.SetActive(false);
        BtnXe.transform.GetChild(0).gameObject.SetActive(false);
        BtnAvoidItem.transform.GetChild(0).gameObject.SetActive(false);
        BtnMauChu.transform.GetChild(0).gameObject.SetActive(true);
    }
    private void colorItemClick(int index)
    {
        AlertController.api.showAlert("Bạn Muốn Mua Màu Này Với Giá " + Utilities.GetStringMoneyByLong(DataHelper.ShopColorItems.cost),()=> {
            if(MyInfo.CHIP< DataHelper.ShopColorItems.cost)
            {
                //AlertController.api.showAlert("Bạn Không Đủ Chip Để Mua Màu Này");
                popupAlert.Show("Bạn Không Đủ Chip Để Mua Màu Này!", popupAlert.Hide);
                return;
            }
            else if (MyInfo.MY_ID_VIP < DataHelper.ShopColorItems.minvip) // Kiểm tra loại Vip
            {
                popupAlert.Show("Bạn Cần Đạt VIP " + DataHelper.ShopColorItems.minvip + " Để Mua Màu Này!", popupAlert.Hide);
                return;
            }
            else
            {
                //goi service mua
                WWWForm form = new WWWForm();               
                form.AddField("color", DataHelper.ShopColorItems.color[index].ToString());
                API.Instance.BaseCallService("/userinfo/changeColor", form, RspColorChangeComplete);
            }
        },true);        
    }
    void RspColorChangeComplete(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        if (node["status"].AsInt == 1)
        {
            //AlertController.api.showAlert(node["msg"]);
            if (node["chip"].Value != "" && node["chip"].Value != null)
            {
                MyInfo.CHIP = long.Parse(node["chip"].Value);
            }
            popupAlert.Show("Đổi Màu Vip Vip Công", popupAlert.Hide);
            //goi service lấy danh sách color màu user
            WWWForm form = new WWWForm();
            form.AddField("", "");
            API.Instance.BaseCallService("/userinfo/usercolor", form, RspListUserColor);            
        }
        else
        {
            AlertController.api.showAlert(node["msg"]);
        }

    }
    void RspListUserColor(string _json)
    {
        JSONNode _nodeAssets = JSONNode.Parse(_json);
        DataHelper.UserColorChat.Clear();
        for (int i = 0; i < _nodeAssets.Count; i++)
        {
            //string name = _nodeAssets[i]["name"].Value;
            string name = _nodeAssets[i]["username"].Value;
            string color = _nodeAssets[i]["color"].Value;
            DataHelper.UserColorChat.Add(name, color);
        }
    }
    int curItemAvoid;
    private void avoidItemClick(int id)
    {
        PannelBuyAvoidItem.SetActive(true);
        Button closebtn = PannelBuyAvoidItem.GetComponent<Button>();
        closebtn.onClick.RemoveAllListeners();
        closebtn.onClick.AddListener(() => { PannelBuyAvoidItem.SetActive(false); });
        foreach (var item in DataHelper.AvoidItems)
        {
            AvoidItem it = (AvoidItem)item;
            if (it.id == id)
            {
                curItemAvoid = id;
                PannelBuyAvoidItem.GetComponentInChildren<RemoteImageBehaviour>().Load(API.Instance.DOMAIN + "/files/" + it.image);
                nameItemAvoid.GetComponent<Text>().text = it.name;
                SetPriceToItem(it.price);
                break;                
            }
        }
    }
    int buyAvoidItemDay = 0;

    private void OnClickBuyItem(JSONNode jSONNode)
    {
        Debug.LogError("OnClickBuyItem "+ jSONNode["days"]); 
        GameObject buyPanel = PannelBuyAvoidItem.transform.Find("BuyTypePopup").gameObject;
        buyPanel.SetActive(true);

        buyPanel.GetComponent<Button>().onClick.RemoveAllListeners();
        buyPanel.GetComponent<Button>().onClick.AddListener(() => { buyPanel.SetActive(false); });
        GameObject buttonBuyCard = buyPanel.transform.Find("BuyGroup").transform.Find("BtnMua1").gameObject;
        Text cardPrice = buttonBuyCard.transform.Find("Text").GetComponentInChildren<Text>();
        cardPrice.text = jSONNode["card"].Value;
        buttonBuyCard.GetComponent<Button>().onClick.RemoveAllListeners();
        buttonBuyCard.GetComponent<Button>().onClick.AddListener(() => { BuyItemAvoidBuyPrice(jSONNode["card"].Value); });

        buttonBuyCard.SetActive(GameHelper.EnablePayment);

        GameObject buttonBuyInapp = buyPanel.transform.Find("BuyGroup").transform.Find("BtnMua2").gameObject;
        Text inappPrice = buttonBuyInapp.transform.Find("Text").GetComponentInChildren<Text>();
        inappPrice.text = jSONNode["inapp"].Value;
        buttonBuyInapp.GetComponent<Button>().onClick.RemoveAllListeners();
        buttonBuyInapp.GetComponent<Button>().onClick.AddListener(() => { BuyItemAvoidBuyPrice(jSONNode["inapp"].Value, jSONNode["days"], false); });
        
    }
    void SetPriceToItem(JSONNode jSONNode)
    {
        GameObject buyBtnGroup = PannelBuyAvoidItem.transform.Find("Bg").transform.Find("BuyGroup").gameObject;
        GameObject btnbuyprefab = buyBtnGroup.transform.Find("BtnMua").gameObject;
        int count = buyBtnGroup.transform.childCount;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                GameObject obj = buyBtnGroup.transform.GetChild(i).gameObject;
                if(obj.name != "BtnMua")
                    Destroy(obj);
            }
        }
        for (int i = 0; i < jSONNode.Count; i++)
        {
            JSONNode node = jSONNode[i];
            int day = node["days"].AsInt;
            GameObject buybtn = Instantiate(btnbuyprefab, buyBtnGroup.transform);
            buybtn.name = "BtnMua"+day;
            buybtn.SetActive(true);
            buybtn.transform.GetComponentInChildren<Text>().text = day + " NGÀY";
            buybtn.GetComponent<Button>().onClick.RemoveAllListeners();
            buybtn.GetComponent<Button>().onClick.AddListener(() => { OnClickBuyItem(node); });
            
        }
    }

 

    public void BuyItemAvoid(int type)
    {
        switch (type)
        {
            case 1: BuyItemAvoidBuyPrice(Price30.GetComponent<Text>().text); break;
            case 2: BuyItemAvoidBuyPrice(Price60.GetComponent<Text>().text); break;
            case 3: BuyItemAvoidBuyPrice(Price90.GetComponent<Text>().text); break;
            case 4: BuyItemAvoidBuyPrice(Price120.GetComponent<Text>().text); break;
            case 11: BuyItemAvoidBuyPrice(PriceInapp30.GetComponent<Text>().text,"30",false); break;
            case 12: BuyItemAvoidBuyPrice(PriceInapp60.GetComponent<Text>().text, "60", false); break;
            case 13: BuyItemAvoidBuyPrice(PriceInapp90.GetComponent<Text>().text, "90", false); break;
            case 14: BuyItemAvoidBuyPrice(PriceInapp120.GetComponent<Text>().text, "120", false); break;
        }
    }

    void BuyItemAvoidBuyPrice(string price, string IdItem="", bool isCard=true)
    {
        if(price== "non" || isCard)
        {
            AlertController.api.showAlert("Liên Hệ Admin", onCallBackBackAdmin);
            return;
        }
        Debug.Log("BuyAvoidItem======");
        string idInapItem = "avoid_"+IdItem;
        Debug.LogError("idInapItem " + idInapItem);
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.OnPurchaseClicked(idInapItem, OnPuchased);
        
    }

    private void onCallBackBackAdmin()
    {
        Application.OpenURL(DataHelper.ZALO_ADD);
    }

    void OnPuchased(string productId, string purchaseToken, bool isSuccess)
    {
        Debug.Log("OnPuchased====BuyAvoidItem======");
        if (isSuccess)
        {
            //string result = _result.purchasedProduct.receipt;
            //if (_result.Google_PurchaseInfo != null)
            //    Debug.LogWarning(_result.Google_PurchaseInfo);
            //else if (_result.IOS_PurchaseInfo != null)
            //    Debug.LogWarning(_result.IOS_PurchaseInfo);
#if UNITY_ANDROID
            //JSONNode node = JSONNode.Parse(_result.Google_PurchaseInfo.originalJson);
            string store = "googleplay";
            string productID = productId;//node["productId"].Value;
            string token = purchaseToken;//node["purchaseToken"].Value;

            WWWForm form = new WWWForm();
            form.AddField("store", store);
            form.AddField("product_id", productID);
            form.AddField("package_id", Application.identifier);
            form.AddField("purchase_token", token);
            form.AddField("item_id", curItemAvoid);
            API.Instance.BaseCallService("/payment/avoidcard", form, RspPaymentInApp);
#endif
        }
        else
        {
            AlertController.api.showAlert("Giao dịch không thành công.");
        }
    }
    void RspPaymentInApp(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);        
        if (node["status"].AsInt == 1)
        {
            AlertController.api.showAlert(node["msg"]);
        }
        else 
        {
            AlertController.api.showAlert(node["msg"]);
        }

    }

    public void UpDatePrice()
    {
        TxtChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        TxtGem.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
    }




    public void InitPanelBuyBorAvatar()
    {
        Dictionary<string, BoderAvatar> DicBor = DataHelper.DicDataBoderAvatar;
        //   Debug.LogError("DicBor.Count;------------ " + DicBor.Count);

        
        int count = ContentAvatar.transform.childCount;

      //  Debug.LogError("count;------------ " + count);
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                GameObject obj = ContentAvatar.transform.GetChild(i).gameObject;
                Destroy(obj);
            }
        }

        int BorCount = MyInfo.MY_BORDERS_AVATAR.Count;
        List<int> ListBors = MyInfo.MY_BORDERS_AVATAR;

        for (int i = 1; i < DicBor.Count; i++)
        {
            bool t = CheckSoHuu(i);
            if (t == true)
            {

                string key = i.ToString();
                //  Debug.LogError("DicBor[i.ToString()].id ------------ " + DataHelper.DicDataBoderAvatar[key]);
                GameObject obj = Instantiate(ItemBuyBorAvatar) as GameObject;
                obj.transform.SetParent(ContentAvatar.transform);
                obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

                ItemBuyBorderAvatar item = obj.GetComponent<ItemBuyBorderAvatar>();
                string id = DicBor[i.ToString()].id;
                string vip = DicBor[i.ToString()].vip_require;
                string chip = DicBor[i.ToString()].chip;
                string gem = DicBor[i.ToString()].gem;

                Sprite spr = DataHelper.dictSpriteBoderAvt_Shop[id];
                //  Debug.LogError("id " + id + "   vip " + vip + "    chip " + chip + "   gem " +gem + "  Hinh " + spr);
                if (i > 7)
                {
                    // Debug.LogError("mua gem");
                    //   Debug.LogError("id " + id + "   vip " + vip + "    chip " + chip + "   gem " + gem + "  Hinh " + spr);
                    item.Init(id, vip, gem, spr, true);
                }
                else
                {
                    //  Debug.LogError("id " + id + "   vip " + vip + "    chip " + chip + "   gem " + gem + "  Hinh " + spr);
                    item.Init(id, vip, chip, spr);
                }


            }




        }
        
    }

    bool CheckSoHuu(int id)
    {
        int BorCount = MyInfo.MY_BORDERS_AVATAR.Count;
        List<int> ListBors = MyInfo.MY_BORDERS_AVATAR;

        for (int i = 0; i < BorCount; i++)
        {
            if (id == ListBors[i])
            {
                return false;
            }
        }

        return true;
    }


    public void MuaKhungAvartar(int id_Border)
    {
        
        API.Instance.RequestBuyAvatarBorder(id_Border, RspBuyBorder);
    }

    void RspBuyBorder(string _json)
    {
        int Id_Vip = int.Parse(Id_Vip_Can);
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log("node Mua Border   " + node);
        Debug.Log("status Mua Border   " + node["status"].Value);
        int status = node["status"].AsInt;
        if (status ==  -1)
        {
            popupAlert.Show("Bạn đã sở hữu khung Avatar này!", popupAlert.Hide);
           // Debug.LogError("avatar đã sở hữu");
        }
        else if (status == -2)
        {
            popupAlert.Show("Khung Avatar không tồn tại!", popupAlert.Hide);
            Debug.Log("avatar không tồn tại");
        }
        else if (status == -3)
        {
            popupAlert.Show("Bạn cần đạt VIP " + Id_Vip + " để được sỡ hữu khung Avatar này!", popupAlert.Hide);
            Debug.Log("ko đủ mức vip mua avatar này (ở đây sẽ có thêm param vip_require chỉ mức vip cần để mua cho client hiện thông báo)");
        }
        else if (status == -4)
        {
            popupAlert.Show("Không đủ CHIP!", popupAlert.Hide);
            Debug.Log("không đủ chip để mua avatar này");
        }
        else if (status == -5)
        {
            popupAlert.Show("Không đủ GEM!", popupAlert.Hide);
            Debug.Log("không đủ gem để mua avatar này");
        }
        else if (status == 1)
        {
            MyInfo.MY_NEW_AVARTAR_BORDER = node["new_avatar_border"].Value;
            MyInfo.MY_BORDERS_AVATAR.Clear();
            for (int i = 0; i < node["avatar_border"].Count; i++)
            {
                MyInfo.MY_BORDERS_AVATAR.Add(node["avatar_border"][i].AsInt);
            }

            MyInfo.CHIP = long.Parse(node["chip"].Value);
            MyInfo.GEM = long.Parse(node["gem"].Value);

            UpDatePrice();

            Scene my_Scene = SceneManager.GetActiveScene();

            if (my_Scene.name == "HomeSceneV2")
            {
                Home.UpdateInfoUser();
            }
            else if (my_Scene.name == "WaitingRoom")
            {
                _LobbyView.UpdateChipUser();
                _LobbyView.UpdateGemUser();
            }

            
            DongPaneMua();

            popupAlert.Show("Giao dịch thành công!", popupAlert.Hide);
            Debug.Log("So Chip Con lai: " + node["chip"].Value);
            Debug.Log("So Gem Con lai: " + node["gem"].Value);
        }
        
    }

    [SerializeField]
    public Image ImgBor, ImgIcon;
    [SerializeField]
    public Text TxtTenVipMua, TxtGia;
    [SerializeField]
    public Sprite NutVang, KimCuong;
    [SerializeField]
    public GameObject PanelMua;

    private string Id_Bor_CanMua;
    private string Id_Vip_Can;
    private long GiaChip;
    private long GiaGem;
    private bool byGem;

    public void MoPanelMua(string id, string TenVip, string gia, bool ByGem = false)
    {
        //  Debug.LogError("Vo Shop -> SprBor " + DataHelper.dictSpriteBoderAvt_Shop[id] + "   TenVip = " + TenVip + "   Gia = " + gia + "  ByGem = " + ByGem);
        //  ImgBor.sprite = sprBor;
        //   Debug.Log("Images-------------- " + ImgBor + "  TxtTenVipMua-----------------  " + TxtTenVipMua + "  TxtGia------------------ " + TxtGia);
        //  Debug.Log("DataHelper.dictSpriteBoderAvt_Shop[id]--------------- " + DataHelper.dictSpriteBoderAvt_Shop[id]);

        Id_Bor_CanMua = id;
        Id_Vip_Can = TenVip;
        byGem = ByGem;


        ImgBor.sprite = DataHelper.dictSpriteBoderAvt_Shop[id];
        TxtTenVipMua.text = "VIP" + TenVip;

        if (ByGem == true)
        {
            TxtGia.text = "<color=#49EEFDFF>" + gia + "</color>";
            ImgIcon.sprite = KimCuong;
            GiaGem = long.Parse(gia);
            GiaChip = -1;
        }
        else
        {

            long t = long.Parse(gia);
            string Gia = Utilities.GetStringMoneyByLong(t); ;

            TxtGia.text = Gia;
            ImgIcon.sprite = NutVang;

            GiaChip = long.Parse(gia);
            GiaGem = -1;
        }
        PanelMua.SetActive(true);
    }


    public void DongPaneMua()
    {
        ImgBor.sprite = null;
        TxtTenVipMua.text = "";
        TxtGia.text = "";
        ImgIcon.sprite = NutVang;
        PanelMua.SetActive(false);

        Id_Bor_CanMua = "";
        Id_Vip_Can = "";
        GiaChip = 0;
        GiaGem = 0;
        byGem = false;


}
    public void BtnMuaBatTuItem(int idDay)
    {
        //1: 30 - 2 : 60 - 3 : 90 - 4 : 120

    }
    public void BtnMuaClick()
    {
        bool check = Check();

        if (check == true)
        {
            int id = int.Parse(Id_Bor_CanMua);
            MuaKhungAvartar(id);
        }
    }

    bool Check()
    {
        int Id_Vip = int.Parse(Id_Vip_Can);
        if (MyInfo.MY_ID_VIP < Id_Vip) // Kiểm tra loại Vip
        {
          //  Debug.LogError("Không đủ điều kiện VIP. Bạn cần đạt VIP " + Id_Vip + " để được sỡ hữu khung Avatar này!");
            popupAlert.Show("Bạn cần đạt VIP " + Id_Vip + " để được sỡ hữu khung Avatar này!", popupAlert.Hide);
            return false;
        }

        if (byGem == false) // kiểm tra đủ tiền để mua không
        {
            // Mua bằng Chip
            if (MyInfo.CHIP < GiaChip) 
            {
                Debug.Log("Không đủ CHIP để mua!");
                popupAlert.Show("Không đủ CHIP!", popupAlert.Hide);
                return false;
            }
        }
        else
        {
            // Mua bằng Gem
            if (MyInfo.GEM < GiaGem)
            {
                Debug.Log("Không đủ GEm để mua!");
                popupAlert.Show("Không đủ GEM!", popupAlert.Hide);
                return false;
            }
        }

        if (DataHelper.DicDataBoderAvatar.ContainsKey(Id_Bor_CanMua) == false) // Kiểm tra khung có tồn tại hay không
        {
            Debug.Log("Khung Avatar không tồn tại!");
            popupAlert.Show("Khung Avatar không tồn tại!", popupAlert.Hide);
            return false;
        }

        List<int> KhungSoHuu = MyInfo.MY_BORDERS_AVATAR; //Kiểm tra khung đã sỡ hữu
        int Id_Bor = int.Parse(Id_Bor_CanMua);
        for (int i = 0; i < KhungSoHuu.Count; i++)
        {
            if (Id_Bor == KhungSoHuu[i])
            {
                Debug.Log("Bạn đã sở hữu khung Avatar này!");
                popupAlert.Show("Bạn đã sở hữu khung Avatar này!", popupAlert.Hide);
                return false;
            }
        }


        Debug.Log("Đang gửi request mua");
        return true;
    }



}

public class CommandKey
{
    //Bang Hoi
    public const string REQUEST_REQUIRE_JOIN = "responseRequestJoin";
    public const string GET_LIST_THANH_VIEN = "getMember";
    public const string GET_LIST_THANH_VIEN_CUU_TRO = "getRequestHelp";
    public const string REJECT_CUU_TRO = "rejectHelp";
    public const string REQUEST_CUU_TRO = "requestHelp";
    public const string CONTRIBUTE_CLAN = "contributeClan";
    public const string UPGRADE_DEPUTY = "upgradeDeputy";
    public const string DOWN_GRADE_DEPUTY = "downGradeDeputy";
    public const string KICK_MEMBER_BANG_HOI = "kickMember";
    public const string HELP_MEMBER_BANGHOI = "helpMember";
    public const string RECEIVE_HELP_BANGHOI = "receiveClanHelp";
    public const string GET_LIST_CHO_DUYET = "getRequestJoin";
    public const string GET_CLAN_INFO = "getClanInfo";
    public const string SET_UP_CLAN = "setUpConfig";
    public const string GET_CLAN_LIST = "getClanList";
    public const string REQUEST_JOIN_CLAN = "requestJoinClan";
    public const string REQUEST_LEAVE_CLAN = "requestLeaveClan";

    public const string RECEIVE_INBOX = "receiveinbox";
    public const string TRANSFER_ITEM = "transferitem";

    public const string POST_NEW = "pn";
    public const string GET_POST_NEW = "gnp";

        public const string ERROR = "-1";
		//Lobby
		public const string CREATE_ROOM = "3";

    public const string GET_PLAY_BOARD_LIST = "4";
    public const string JOIN_GAME_LOBBY_ROOM = "5";
    public const string JOIN_GAME_ROOM = "7";
		public const string GET_PLAYERS_INVITE = "44";
		public const string INVITE = "45";
    public const string NOTICE_JOIN_GAME_ROOM = "6";
    public const string GET_GAME_ROOM_INFO = "10";
    public const string START_GAME = "8";
    public const string SEND_CARD = "12";
    public const string PASS = "13";
    public const string FINISH_CARD = "118";
    public const string DIE_13_CARD = "117";
    public const string END_GAME = "15";
    public const string USER_EXIT = "110";
    public const string TURN_ON_SEND_CARD_BTN = "119";
    public const string WAIT_FOR_FOUR_PAIR_SEQUENCE = "120";
    public const string REACTIVE_USER = "14";

    //BAI CAO
    public const string OPEN_CARD = "215";

    public const string PICK_CARD = "111";
	public const string RAISE = "112";
	public const string CHECK = "113";
	public const string FOLD = "114";
	public const string DEAL_ONE_CARD = "115";
	public const string JOIN_QUICK_GAME = "121";
	public const string REFRESH = "122";
	public const string FINISH_XT = "15";
	public const string REQUEST_ADS = "143";
	public const string REWARD_ADS = "144";

    public const string KickUser = "46";
    public const string KICK_USER_BY_HOST = "kubh";
    public const string KICK_USER_ERROR = "kerr";
    public const string MISSION = "150";
    public const string CLAIM_MISSION = "151";


    



    public const string LotteryBet = "1501";
   // public const string LotteryHistory = "1500";
    public const string LotteryInit = "1508";
    


    public const string ResultLottery = "1505";

    public const string GET_USER_MONEY = "1506";

    public const string ResultBet = "1507";




    //ARENA
    public const string JOIN_ARENA = "123";
	public const string START_ARENA = "124";
	public const string FINISH_ARENA = "125";
    public const string JOIN_ARENA_GAME = "126";
    public const string ARENA_KICK = "127";
    public const string TIMES_UP = "128";
    public const string POPUP_PRIZE = "129";

    // Phom
    public const string WatcherGetGameRoomInfoOK = "WatcherGetGameRoomInfoOK";
    public const string PlayerGetGameRoomInfoOK = "PlayerGetGameRoomInfoOK";
    public const string PlayerNoticeJoinGameRoom = "PlayerNoticeJoinGameRoom";
    public const string StartGameOK = "StartGameOK";
    public const string DiscardNG = "DiscardNG";
    public const string DiscardOK = "DiscardOK";
    public const string StartTurnOK = "StartTurnOK";
    public const string DrawCardOK = "DrawCardOK";
    public const string MeldCardsOK = "MeldCardsOK";
    public const string LayDownOK = "LayDownOK";
    public const string SendCardsOK = "SendCardsOK";
    public const string PlayerEndGameOK = "PlayerEndGameOK";
    public const string PlayerNoticeExit = "NoticeExit";
    public const string CountDownAtStartGame = "CountDownAtStartGame";

    public const string GetGameRoomInfo = "GetGameRoomInfo";
    public const string HackDeal = "HackDeal";
    public const string HackDraw = "HackDraw";
    public const string Discard = "Discard";
    public const string StartNextTurn = "StartNextTurn";
    public const string DrawCard = "DrawCard";
    public const string MeldCards = "MeldCards";
    public const string LayDown = "LayDown";
    public const string SendCards = "SendCards";
    public const string ChangeCombo = "ChangeCombo";

    public const string DRAW_CARD = "401";
    public const string CHOOSE_A_CARD_TO_DISCARD = "403";
    public const string END_TURN = "404";
    public const string DISCARD = "409";
    public const string ANIMATE_DISCARD_ACTION = "410";
    public const string START_TURN = "405";
    public const string ANIMATE_DRAW_CARD_ACTION = "411";
    public const string MELD_CARDS = "412";
    public const string ANIMATE_MELD_CARDS_ACTION = "413";

    public const string PLAYER_MELD_CARDS_NG = "1414";
    public const string PLAYER_BE_MELDED_CARDS_OK = "1415";
    public const string ENEMY_MELD_CARDS_OK = "1416";
    public const string PLAYER_MELD_CARDS_OK = "1417";

    // Tai Xiu
    public const string JOIN_LUCKY_GAME = "1";
    public const string JOIN_GAME = "7";
    public const string UPDATE = "1418";
    public const string DICE_RESULT = "1419";
    public const string BET = "112";
    public const string CHEAT = "1420";

	public const string QUICK_GAME = "130";
	public const string GET_SPEC_WHEEL = "145";
	public const string START_ROLL_SPEC_WHEEL = "146";
    public const string JOIN_LOTTERY_GAME = "148";

    //dua ngua
    public const string JOIN_RACING_HORSE_GAME = "210";
    public const string RESULT_RACING_HORSE = "211";
    public const string RESULT_RACING_HORSE_COMPLETE = "212";
    public const string HISTORY_USER_BET = "213";
    public const string JOIN_BAU_CUA_GAME = "216";
    //tournament
    public const string GET_EVENT_STATUS = "get_event_status";
    public const string JOIN_EVENT_REQUEST = "join_event_request";
    public const string JOIN_GAME_EVENT = "join_game_event";
    public const string START_GAME_EVENT = "start_game_event";
    public const string EXIT_GAME_EVENT = "110";
    public const string LEADERBOARD = "leaderboard";
    public const string USE_GIFTCODE = "use_giftcode";
    //Level Up
    public const string LEVEL_UP = "level_up";
    public const string JOIN_CHAT_ROOM = "jcr";
    public const string GET_LIST_FRIEND = "get_list_friend";
    public const string ADD_FRIEND_REQUEST = "add_friend_request";
    public const string ADD_FRIEND_RESULT = "add_friend_result";
    public const string ADD_FRIEND_CONFIRM = "add_friend_confirm";
    public const string ADD_FRIEND_SUCCESS = "add_friend_success";
    public const string ADD_FRIEND_IGNORE = "add_friend_ignore";
    public const string ADD_FRIEND_NOTICE = "nfo";
    public const string UNFRIEND_FRIEND_REQUEST = "unfriend";
}
﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class Deck : MonoBehaviour {
    public Card cardObject;
    public Card[] cards;
    public Sprite[] ValueSprites;
    public Sprite[] upperSprites;
    public Sprite[] mainSprites;
    public Sprite[] jqkSprites;
	void Awake()
    {
        cards = new Card[52];
        for (int i = 1; i <= 4; i++)
        {
            for(int j = 1; j <= 13; j++)
            {
                int index = (i - 1) * 13 + (j - 1);
                Card temp = Instantiate(cardObject);
                temp.transform.SetParent(transform, false);
                temp.Id = index;

                SetValueByGame(temp, j);

                temp.Type = (CardType)(i - 1);
                
                if ((int)temp.Type < 2)
                    temp.ValueImage.color = Color.black;
                else
                    temp.ValueImage.color = Color.red;
                
                temp.ValueImage.sprite = ValueSprites[j - 1];
                temp.UpperTypeImage.sprite = upperSprites[i - 1];
                
                if (j >= 11)
                    temp.MainTypeImage.sprite = jqkSprites[(j - 11)];
                else
                    temp.MainTypeImage.sprite = mainSprites[i - 1];
                //temp.MainTypeImage.SetNativeSize();
                cards[index] = temp;
            }
        }
    }

    private void SetValueByGame(Card card, int value)
    {
        if (SceneManager.GetActiveScene().name.Equals(GameScene.TLMNScene.ToString()))
        {
            if (value == 1)
                card.Value = 14;
            else if (value == 2)
                card.Value = 15;
            else
                card.Value = value;
        }
		if (SceneManager.GetActiveScene().name.Equals(GameScene.PhomScene.ToString()))
		{
			card.Value = value;
		}
    }

}
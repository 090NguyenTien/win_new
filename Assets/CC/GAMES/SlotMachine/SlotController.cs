﻿using DG.Tweening;
using HelperXT;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotController : MonoBehaviour {
    private long ChoosingBetMoney;
    public List<Button> BetButtons;
    [SerializeField]
    private Sprite[] arrSprSmallSuite,
        arrSprBigSuite,
        arrSprValue,
        arrSprJQK;
    // Use this for initialization
    [SerializeField]
    List<SlotItem> lstCardSlot;
    [SerializeField]
    Toggle AutoSpin;
    //[SerializeField]
    //Image imgResult;
    //[SerializeField]
    ///List<Sprite> lstValueRow;
    [SerializeField]
    Text txtWinMoney, txtNohu, txtResult;
    [SerializeField]
    Image imgDown;
    [SerializeField]
    Button BtnSpin, btnOpenGuide, btnCloseGuide, btnCloseSpin;
    [SerializeField]
    GameObject PopupGuide;
    string[] cardsId;
    string defaultCard = "";
    bool isRun;
    long curScore;
    bool isCount;
    void Start () {
        //imgResult.gameObject.SetActive(false);
        defaultCard = "";
        AutoSpin.isOn = false;
        isRun = false;
        txtResult.gameObject.SetActive(false);
        txtWinMoney.gameObject.SetActive(false);
        PopupGuide.SetActive(false);
        setDefaultCard();
        ChoosingBetMoney = 10000;
        DisableAllBetButtons();
        EnableButton(BetButtons[0]);
    }
    
    // Update is called once per frame
    void Update () {
        if (!isCount) return;
        float ScoreIncrement = 100 * Time.deltaTime;        
        curScore += (long)ScoreIncrement;        
        txtNohu.text = curScore.ToString();
    }
    public void Show()
    {        
        gameObject.SetActive(true);        
        API.Instance.RequestInitSlot(SetInitSlotMachine);
    }
    public void OnBetMoneyButtonClick(Button button)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        DisableAllBetButtons();
        EnableButton(button);
        string betMoneyText = button.transform.GetChild(1).GetComponent<Text>().text;
        ChoosingBetMoney = TXUtil.ConvertShortTextToMoney(betMoneyText);
    }
    public void DisableAllBetButtons()
    {
        foreach (Button button in BetButtons)
        {
            button.transform.GetChild(2).gameObject.SetActive(true);
        }
    }
    public void EnableButton(Button button)
    {
        button.transform.GetChild(2).gameObject.SetActive(false);
    }
    private void SetInitSlotMachine(string _json)
    {
        Debug.Log(" ========================SetInitSlotMachine=============== " + _json);
        JSONNode node = JSONNode.Parse(_json);        
        long chipPerSecond = long.Parse(node["chip_per_second"]);
        long lastTimePing = long.Parse(node["last_pingoo"]);
        long nowTime = long.Parse(node["now"]);
        txtNohu.text = chipPerSecond.ToString();
        long distanceTime = nowTime - lastTimePing;
        long seconds = distanceTime / 1000;
        curScore = seconds * 100;
        isCount = true;
    }

    public void Spin()
    {
        if (isRun) return;
        if (MyInfo.CHIP < ChoosingBetMoney)
        {
            AlertController.api.showAlert("Bạn Không Đủ Tiền Để Quay Tiếp!!!");
            return;
        }
        StartCoroutine(AniBtnSpin());
        imgDown.gameObject.SetActive(false);
        //imgResult.gameObject.SetActive(false);
        txtResult.gameObject.SetActive(false);
        txtWinMoney.gameObject.SetActive(false);
        isRun = true;
        setDefaultCard();
        Debug.LogWarning("Spin===============");
        API.Instance.RequestSpinSlot((int)ChoosingBetMoney, onSpinComplete);
        txtWinMoney.transform.DOMoveY(txtWinMoney.transform.position.y - 1f, 0f);//set default y cua txtWinmoney
    }

    private IEnumerator AniBtnSpin()
    {
        BtnSpin.transform.DOMoveY(BtnSpin.transform.position.y - 1f, 0.3f).SetEase(Ease.InOutExpo);
        yield return new WaitForSeconds(0.3f);
        BtnSpin.transform.DOMoveY(BtnSpin.transform.position.y + 1f, 0.3f).SetEase(Ease.InOutExpo);
    }

    private void onSpinComplete(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" ========================onSpinComplete=============== " + _json);
        if(node["status"].AsInt == 1)
        {
            long chipReceive = long.Parse(node["chip_receive"]);
            if (chipReceive > 0)
            {
                txtWinMoney.text = "+" + Utilities.GetStringMoneyByLongBigSmall(chipReceive);
            }
            else
            {
                txtWinMoney.text = "-" + Utilities.GetStringMoneyByLongBigSmall(-chipReceive);
            }

            MyInfo.CHIP = long.Parse(node["chip"]);
            
            if (node["status"].AsInt == 1)//no hu trung lon
            {
                long lastTimePing = long.Parse(node["last_pingoo"]);
                long nowTime = long.Parse(node["now"]);
            }
            //Set List Card Win From Service
            JSONArray cardArray = node["cards"].AsArray;
            cardsId = new string[5];
            for (int i=0;i<cardArray.Count;i++)
            {
                cardsId[i] = cardArray[i];
            }
            //end
            
            cardDefault = cardsId;//update data default thanh ket qua
            int index = 0;
            foreach (SlotItem slotItem in lstCardSlot)
            {
                setEndDataCardToEndAnimation(slotItem.getListCardAnimation(), index);//set la bai` cuoi cung` chay animation la` la' bai` ket qua
                if (index == lstCardSlot.Count - 1)
                {
                    StartCoroutine(slotItem.runAnimationSlot(index,OnCompleteAnimation));
                }
                else
                {
                    StartCoroutine(slotItem.runAnimationSlot(index));
                }
                
                index++;
            }
            StartCoroutine(EndAnimationSpin());
            StartCoroutine(ShowValueHandCards(node["card_type"], node["card_index"].AsInt, node["ispingoo"].AsInt));            
        }
    }
    
    public IEnumerator ShowValueHandCards(string cardType, int indexType, int ispingoo)
    {
        yield return new WaitForSeconds(3.0f);
        //Debug.LogError("ShowValueHandCards============" + _type);
        HomeViewV2.api.UpdateChip(Utilities.GetStringMoneyByLong(MyInfo.CHIP));
        txtWinMoney.gameObject.SetActive(true);
        txtWinMoney.transform.DOMoveY(txtWinMoney.transform.position.y + 1f, 0.5f);
        
        if (indexType >= 0)
        {

            //imgResult.sprite = lstValueRow[indexType];
            //imgResult.gameObject.SetActive(true);
            txtResult.gameObject.SetActive(true);
            txtResult.text = cardType; 
            if (ispingoo == 1)//no hu
            {
                txtNohu.text = "0";
            }
        }
        if (indexType == -1) txtResult.text = "";
    }
    private void OnCompleteAnimation()
    {
        isRun = false;
        if (AutoSpin.isOn)
        {
            StartCoroutine(AutoSpinMachine());
        }else
        {
            imgDown.gameObject.SetActive(true);
        }
    }

    private IEnumerator AutoSpinMachine()
    {
        yield return new WaitForSeconds(1.0f);
        Spin();
    }

    private IEnumerator EndAnimationSpin()
    {
        yield return new WaitForSeconds(1.5f);
        int index = 0;
       
        foreach (SlotItem slotItem in lstCardSlot)
        {
            //Debug.LogError("EndAnimationSpin========cardsId[index]============"+ cardsId[index]);
            ShowCard(slotItem.getCard(), int.Parse(cardsId[index].ToString()));
            index++;
        }        
    }

    public void setEndDataCardToEndAnimation(List<XTCardItem> lstCardAnimation, int index)
    {
        ShowCard(lstCardAnimation[lstCardAnimation.Count - 1], int.Parse(cardsId[index].ToString()));
        //ShowCard(lstCardAnimation[lstCardAnimation.Count - 1], 1);
        //Debug.LogError("EndcardAnimatioin============" + cardsId[index]); 
    }
    string[] cardDefault;
    void setDefaultCard()
    {
        int index = 0;
        int cardId;        
        if (defaultCard == "")
        {
            //cardDefault = new string[0];
            defaultCard = "47_48_49_50_51";
            cardDefault = defaultCard.Split('_');
        }
        
        foreach(SlotItem slotItem in lstCardSlot)
        {
            //Debug.Log("cardDefault[index]=============" + cardDefault[index]);
            cardId = int.Parse(cardDefault[index].ToString());
            ShowCard(slotItem.getCard(), cardId);
            setRandomCardAnimation(slotItem.getListCardAnimation(), cardId);
            index++;
        }
    }
    public void setRandomCardAnimation(List<XTCardItem> lstCardAnimation, int cardID)
    {
        int index = 0;
        foreach(XTCardItem cardItem in lstCardAnimation)
        {
            if (index == 0)
            {                
                ShowCard(cardItem, cardID);
            }else
            {
                ShowCard(cardItem, UnityEngine.Random.Range(0, 52));
            }            
            index++;
        }
    }
    private void ShowCard(BaseCardItem _cardItem, int _id)
    {
        //Debug.LogError("SLOTMACHINE===========ID"+ _id);
        _cardItem.gameObject.SetActive(true);
        _cardItem.card = BaseCardInfo.Get(_id);

        if (_cardItem.Value == null)
        {
            _cardItem.Value = _cardItem.transform.Find("Card Value").GetComponent<Image>();
            _cardItem.SmallSuite = _cardItem.transform.Find("Upper Type").GetComponent<Image>();
            _cardItem.BigSuite = _cardItem.transform.Find("Main Type").GetComponent<Image>();
        }

        _cardItem.Value.sprite = arrSprValue[(_cardItem.card.Value - 1) % 13];

        _cardItem.Value.color = (int)_cardItem.card.Type < 2 ? Color.black : Color.red;

        //		Debug.Log ("TYPE _ " + (int)_cardItem.card.Type);
        _cardItem.SmallSuite.sprite = arrSprSmallSuite[(int)_cardItem.card.Type];

        if (_cardItem.card.Value < 11 || _cardItem.card.Value > 13)
        {
            _cardItem.BigSuite.sprite = arrSprBigSuite[(int)_cardItem.card.Type];
        }
        else
        {
            _cardItem.BigSuite.sprite = arrSprJQK[(_id / 13) * 3 + (_cardItem.card.Value % 11)];
        }
    }
    public void OpenGuide()
    {
        PopupGuide.SetActive(true);
    }
    public void CloseGuide()
    {
        PopupGuide.SetActive(false);
    }
    public void CloseSpin()
    {
        if (isRun)
        {
            AlertController.api.showAlert("Bạn Vui Lòng Đợi Quay Xong Hãy Thoát Nhé!!!");
            AutoSpin.isOn = false;
            return;
        }
        gameObject.SetActive(false);
        //StopAllCoroutines();
       // Destroy(gameObject);
    }
}

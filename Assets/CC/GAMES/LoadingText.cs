﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingText : MonoBehaviour
{
    [SerializeField]
    private string orginText = "", dynamicText = "";

    private Text txtTextLoading = null;

    private int maxLeng = 0;
    private void Awake()
    {
        txtTextLoading = GetComponent<Text>();
        maxLeng = dynamicText.Length + 1;
    }
    private float lastTimeUpdate = 0;

    private int countText = 0;
    private void Update()
    {
        if (Time.realtimeSinceStartup - lastTimeUpdate >= 1)
        {
            lastTimeUpdate = Time.realtimeSinceStartup;
            ++countText;
            txtTextLoading.text = orginText + dynamicText.Substring(0, countText % maxLeng );
            //Debug.Log(maxLeng + "    " +  countText + "    =   " + (countText % maxLeng));
        }
    }
}

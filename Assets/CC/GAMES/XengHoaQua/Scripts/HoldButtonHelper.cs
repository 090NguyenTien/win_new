﻿using BaseCallBack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldButtonHelper : MonoBehaviour {
    Coroutine holdButton;
    onCallBackInt cb;
    int param;
    public void Run(onCallBackInt callback, int _param=-1)
    {
        //Debug.LogWarning("RunHoldButton====");
        cb = callback;
        param = _param;
        holdButton = StartCoroutine(HoldUpPointAni(0.3f));
    }
    
    private IEnumerator HoldUpPointAni(float time)
    {
        if (cb == null) yield return null;
       yield return new WaitForSeconds(time);
        cb(param);
        float nextTime = time - 0.1f;
        if (nextTime <= 0) nextTime = 0.02f;
        holdButton = StartCoroutine(HoldUpPointAni(nextTime));
    }
    public void Remove()
    {
        if (holdButton != null) StopCoroutine(holdButton);
        holdButton = null;
        cb = null;
    }
}

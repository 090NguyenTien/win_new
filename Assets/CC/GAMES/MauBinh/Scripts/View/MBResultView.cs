﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MBResultView : MonoBehaviour {

	BaseCallBack.onCallBack resultComplete;

	[SerializeField]
	GameObject panel, result;
	[SerializeField]
	List<Text> lstTxtName, 
			lstTxtResultRow1, lstTxtResultRow2, lstTxtResultRow3, 
			lstTxtResultAt, 
			lstTxtResultTotal, 
			lstTxtResultDie;

	public void Show(int _position, string _name, int _resultRow1, int _resultRow2, int _resultRow3, int _resultAt, int _resultTotal, bool _isDie, BaseCallBack.onCallBack _complete, float _time = 0)
	{
		lstTxtName [_position].text = _name;

		lstTxtResultRow1 [_position].text = _resultRow1.ToString ();
		lstTxtResultRow2 [_position].text = _resultRow2.ToString ();
		lstTxtResultRow3 [_position].text = _resultRow3.ToString ();

		lstTxtResultAt [_position].text = _resultAt.ToString ();

		lstTxtResultTotal [_position].text = _resultTotal.ToString ();

		lstTxtResultDie [_position].gameObject.SetActive (_isDie);

		resultComplete = _complete;
	}
	IEnumerator CallBackThread(float _time)
	{
		yield return new WaitForSeconds (_time);

		resultComplete ();
	}
	public void Hide(int _position)
	{
		string s = "---";
		lstTxtName [_position].text = s;

		lstTxtResultRow1 [_position].text = s;
		lstTxtResultRow2 [_position].text = s;
		lstTxtResultRow3 [_position].text = s;

		lstTxtResultAt [_position].text = s;

		lstTxtResultTotal [_position].text = s;

		lstTxtResultDie [_position].gameObject.SetActive (false);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class MBBigCardItem : MBCardItem, 
				IDropHandler, 
				IPointerEnterHandler, 
				IPointerExitHandler, 
				IBeginDragHandler, 
				IDragHandler, 
				IEndDragHandler  {

	private Image imgBg;

	private static bool isDragging;	//Dang keo bai
	private bool firstDrag; //la bai dang keo (dau tien)
			

	private int indexBigCard = -1; //0-12 trai qua phai, duoi len tren

	private static List<int> lstCardsDrag;

	RectTransform rectTrsf;

	public bool DarkLight
	{
		set{
			imgBg.color = value ? new Color (.7f, .7f, .7f) : Color.white;
		}
	}

	public void Init(int _index)
	{
		Value = transform.Find ("Card Value").GetComponent<Image> ();
		SmallSuite = transform.Find ("Upper Type").GetComponent<Image> ();
		BigSuite = transform.Find ("Main Type").GetComponent<Image> ();
		BackCard = false;

		imgBg = GetComponent<UnityEngine.UI.Image> ();
		rectTrsf = GetComponent<RectTransform> ();


		isDragging = false;
		firstDrag = false;

		name = _index.ToString ();


		lstCardsDrag = new List<int> ();

		indexCard = _index;
	}


	public void OnBeginDrag (PointerEventData pointerEventData) {
		firstDrag = true;
		isDragging = true;


        lstCardsDrag.Clear();
		lstCardsDrag.Add (indexCard);


		imgBg.raycastTarget = false;


		transform.parent.SetSiblingIndex(2);
		transform.SetSiblingIndex (transform.parent.childCount - 1);

	}
	public void OnDrag(PointerEventData pointerEventData) {
		
		Vector2 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		transform.position = point;




	}
	public void OnDrop(PointerEventData pointer)
	{
//		Debug.Log ("OnDrop - CHANGE");
	}
	public void OnEndDrag(PointerEventData pointerEventData) {

		isDragging = false;
		firstDrag = false;

		imgBg.raycastTarget = true;


//				Debug.Log ("End Drag - " + lstCardsDrag.Count) ;

		if (lstCardsDrag.Count == 2) {
			
			GameObject.FindGameObjectWithTag ("CardView").SendMessage ("EndDragSuccess", lstCardsDrag);

		}
        else
        {
			
			GameObject.FindGameObjectWithTag ("CardView").SendMessage ("EndDragFail", indexCard);

		}


		lstCardsDrag.Clear ();

	}

	public void OnPointerEnter(PointerEventData pointer)
	{
		
		if (isDragging && !firstDrag) {
			DarkLight = true;
            Debug.LogWarning("index " + indexCard + " card "+ card.Value);
			lstCardsDrag.Add (indexCard);
		}
	}
	public void OnPointerExit(PointerEventData pointer)
	{
		if (lstCardsDrag.Contains (indexCard))
		if (!firstDrag)
			lstCardsDrag.Remove (indexCard);

		DarkLight = false;
	}


//	void sendmsg(object obj)
//	{
//		Debug.Log (obj);
//	}

//	public void FinishDrag()
//	{
//		firstDrag = isDragging = false;
//		GameObject.FindGameObjectWithTag ("CardView").SendMessage ("FinishMove", indexCard);
//
//		lstCardsDrag.Clear ();
//
//	}

}

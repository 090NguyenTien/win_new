﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
public class BauCuaHistoryManager : MonoBehaviour {

    private static BauCuaHistoryManager instance;
    public static BauCuaHistoryManager Instance { get { return instance; } }
    [SerializeField]
    GameObject Panel;
    [SerializeField]
    GameObject HistoryList;
    [SerializeField]
    GameObject HistoryItemObj;
    [SerializeField]
    Transform HistoryItemParent;
    [SerializeField]
    Text Message;
    [SerializeField]
    Image Dice1, Dice2, Dice3;
    [SerializeField]
    Text TotalBet, TotalGet;

    private int[] DiceResult;
    private string GameResult;

    void Awake()
    {
        instance = this;
    }

    void ShowItem(int index, HistoryItemBauCua item)
    {
        HistoryBauCuaItemView itemView;
        GameObject obj = Instantiate(HistoryItemObj) as GameObject;
        obj.transform.SetParent(HistoryItemParent);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<HistoryBauCuaItemView>();
        itemView.Init(item);
        itemView.Show();
    }

    public void Show()
    {
        HistoryList.SetActive(false);
        Panel.SetActive(true);
        Message.text = string.Empty;

        if (DiceResult == null)
        {
            Message.text = "CHƯA XONG VÒNG ĐẦU TIÊN.";
            return;
        }
        
        Dice1.sprite = DiceSprites.Instance.Get(DiceResult[0] - 1);
        Dice2.sprite = DiceSprites.Instance.Get(DiceResult[1] - 1);
        Dice3.sprite = DiceSprites.Instance.Get(DiceResult[2] - 1);

        if (string.IsNullOrEmpty(GameResult))
        {
            Message.text = "BẠN KHÔNG CHƠI VÒNG NÀY.";
            return;
        }

        //if (DiceResult[0] == DiceResult[1] && DiceResult[1] == DiceResult[2])
       // {
         //   Message.text = "XỔ BÃO.";
        //    return;
       // }

        HistoryList.SetActive(true);

        for (int i = 0; i < HistoryItemParent.childCount; i++)
            Destroy(HistoryItemParent.GetChild(i).gameObject);

        long totalBet = 0;
        long totalGet = 0;

        string detailResult = GameResult.Split(DelimiterKey.hash).ToList()[0];
        string[] betResults = detailResult.Split(DelimiterKey.dollarPack);
        Dictionary<int, HistoryItemBauCua> resultDic = new Dictionary<int, HistoryItemBauCua>();
        Debug.LogWarning("BauCuaHistoryManager==betResults========" + betResults.ToString());
        foreach (string betResult in betResults)
        {
            string[] parts = betResult.Split(DelimiterKey.colon);
            Debug.LogWarning("=======BauCuaHistoryManager===parts===========" + parts.Length);
            int clientBetType;
            int cnt = 1;
            if (parts.Length == 3)
            {
                clientBetType = int.Parse(parts[0]);
                cnt = 1;
            }
            else
            {
                clientBetType = int.Parse(parts[0] + parts[1]);
                cnt = 2;
            }
            //int clientBetType = parts[0]//TXUtil.ToClientBetType(int.Parse(parts[0]));
            HistoryItemBauCua item = new HistoryItemBauCua();
            item.Type = TXUtil.ConvertBetTypeToStringBauCua(clientBetType);
            item.Bet = TXUtil.ConvertMoneyToText(long.Parse(parts[cnt]));
            item.Get = TXUtil.ConvertMoneyToText(long.Parse(parts[cnt+1]));            
            Debug.LogWarning("BauCuaHistoryManager==clientBetType========" + clientBetType);
            if (!resultDic.ContainsKey(clientBetType))
            {
                resultDic.Add(clientBetType, item);                
            }
            totalBet += long.Parse(parts[cnt]);
            totalGet += long.Parse(parts[cnt+1]);
        }

        int index = -1;

        foreach (KeyValuePair<int, HistoryItemBauCua> pair in resultDic.OrderBy(i => i.Key))
        {
            index++;
            ShowItem(index, pair.Value);
        }

        TotalBet.text = TXUtil.ConvertMoneyToText(totalBet);
        TotalGet.text = TXUtil.ConvertMoneyToText(totalGet);
    }

    public void Hide()
    {
        Panel.SetActive(false);
    }

    public void UpdateHistory(int[] diceResult, string gameResult)
    {
        DiceResult = diceResult;
        GameResult = gameResult;

        if (Panel.activeSelf)
            Show();
    }
}

public class HistoryItemBauCua
{
    public string Type { get; set; }
    public string Bet { get; set; }
    public string Get { get; set; }
}
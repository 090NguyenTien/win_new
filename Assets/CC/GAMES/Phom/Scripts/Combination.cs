﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Combination {
	
	public List<PhomCard> All {get; set;}
    public List<PhomCard> ChosenMeld { get; set; }
	public List<List<PhomCard>> Runs {get; set;}
	public List<List<PhomCard>> Sets {get; set;}
    public List<PhomCard> Waits { get; set; }
    public List<PhomCard> Trashs {get; set;}
	private int ScoreLeft;

	public Combination()
	{
		All = new List<PhomCard>();
        ChosenMeld = new List<PhomCard>();
        Runs = new List<List<PhomCard>>();
		Sets = new List<List<PhomCard>>();
        Waits = new List<PhomCard>();
        Trashs = new List<PhomCard>();
		ScoreLeft = int.MaxValue;
	}

    public int GetAllScore()
    {
        int score = 0;
        foreach (PhomCard card in GetSortedCards()) {
            score += card.Value;
        }
        return score;
    }

	public int GetScoreLeft()
	{
		return ScoreLeft;
	}

	public List<PhomCard> GetSortedCards()
	{
		List<PhomCard> sortedCards = new List<PhomCard> ();

        foreach (PhomCard card in ChosenMeld)
            sortedCards.Add(card);

        foreach (List<PhomCard> cards in Sets)
            foreach (PhomCard card in cards)
                sortedCards.Add(card);

        foreach (List<PhomCard> cards in Runs)
			foreach (PhomCard card in cards)
				sortedCards.Add (card);

        foreach (PhomCard card in Waits)
            sortedCards.Add(card);

        foreach (PhomCard card in Trashs)
			sortedCards.Add (card);

		return sortedCards;
	}

	public bool IsSorted()
	{
		if (All == null || All.Count == 0)
			return false;
		return All.Count == GetSortedCards ().Count ? true : false;
	}

	public bool IsNoMeldsOrWaits()
	{
		if (All == null || All.Count == 0)
			return false;
		return All.Count == Trashs.Count ? true : false;
	}

	public bool IsAllMelds()
	{
		if (All == null || All.Count == 0)
			return false;
		return IsSorted () && ScoreLeft == 0 ? true : false;
	}

	public bool IsAllMeldsAnd10Cards()
	{
		if (All == null || All.Count == 0)
			return false;
		return IsAllMelds () && All.Count == 10 ? true : false;
	}
}
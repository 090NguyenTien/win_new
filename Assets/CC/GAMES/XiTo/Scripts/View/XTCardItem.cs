﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class XTCardItem : BaseCardItem {

	[SerializeField]
	GameObject backCard, darkLight;

	public bool BackCard
    {
		set
        {
            backCard.SetActive(value);

            backCard.transform.localPosition = Vector2.zero;
            if (backCard.transform.childCount > 0)
            {
                backCard.transform.GetChild(0).gameObject.SetActive(false);
            }

            isOnDrag = false;
            
            Debug.Log("set back card visible" + backCard.activeInHierarchy + "    " + value + "    " + backCard.GetComponent<Image>().color.a);
        }
	}
	public bool DarkLight
    {
		set
        {
			darkLight.SetActive (value);
		}
	}

    private bool canDrag = false;
    private bool isOnDrag = false;
    private Image imgBackCard = null;
    public void setActiveDrag()
    {
        canDrag = true;
        isOnDrag = false;
        backCard.transform.GetChild(0).gameObject.SetActive(false);
        
        imgBackCard = backCard.GetComponent<Image>();
        Color colorDefault = Color.white;
        colorDefault.a = 1;
        imgBackCard.color = colorDefault;
        Vector2 posDrag = Vector2.zero;
        posDrag.x = 10;
        imgBackCard.rectTransform.anchoredPosition = posDrag;

        backCard.SetActive(true);
        StartCoroutine(delayShowHint());
    }

    public void stopcanDrag()
    {
        canDrag = false;
        isOnDrag = false;
        backCard.transform.GetChild(0).gameObject.SetActive(false);
    }
    private IEnumerator delayShowHint()
    {
        yield return new WaitForSeconds(1f);
        if (isOnDrag == false)
        {
            backCard.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    private float distanceCompleteDrag = 50;
    public void onDragBack()
    {
        if (canDrag == true)
        {
            imgBackCard = backCard.GetComponent<Image>();
            Vector2 posDrag = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - distanceDrag;
            backCard.transform.position = posDrag;

            if (imgBackCard.rectTransform.anchoredPosition.magnitude >= distanceCompleteDrag)//drag complete
            {
                imgBackCard.gameObject.AddComponent<TweenAlpha>().initTween(imgBackCard, Color.white, 1, 0, 0.5f, tweenComplete);
                canDrag = false;
                stopcanDrag();
            }
        }
    }
    private void tweenComplete()
    {
        Color colorDefault = Color.white;
        colorDefault.a = 1;
        imgBackCard.transform.localPosition = Vector2.zero;
        imgBackCard.color = colorDefault;
        Debug.Log("tween completeeeeeeeeeeeeeeeeeeeeeeee");
        imgBackCard.gameObject.SetActive(false);

    }
    private Vector2 distanceDrag;
    public void onBeginDrag()
    {
        if (canDrag == true)
        {
            isOnDrag = true;
            distanceDrag = Camera.main.ScreenToWorldPoint(Input.mousePosition) - imgBackCard.transform.position;
            backCard.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    //	public void OnSelect()
    //	{
    //		string currentSceneName = SceneManager.GetActiveScene().name;
    //		if(currentSceneName.Equals(GameScene.TLMNScene.ToString()))
    //			TLMNRoomController.Instance.player.gameObject.SendMessage("PickCard", this, SendMessageOptions.DontRequireReceiver);
    //	}


}

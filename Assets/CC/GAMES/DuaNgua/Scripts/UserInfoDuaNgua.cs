﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInfoDuaNgua : MonoBehaviour {
    [SerializeField]
    Text TxtChipUser, TxtMoneyBet, TxtMoneyWin;
    [SerializeField]
    TMPro.TextMeshProUGUI TxtNameUser;
    [SerializeField]
    Image imgAvatar, ImgBor, ImgBor_Df;
    [SerializeField]
    Button BtnClose;
    long betMoney;
    public Text getTxtChipUser()
    {
        return TxtChipUser;
    }
    public void Init()
    {
        TxtNameUser.text = MyInfo.NAME;
        TxtChipUser.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.CHIP);
        TxtMoneyBet.text = "0";
        TxtMoneyWin.text = "0";
        betMoney = 0;
    }
    public void ShowInfoUser(string _name, long _chip, Sprite _spr)
    {
        TxtNameUser.text = _name;
        TxtChipUser.text = Utilities.GetStringMoneyByLongBigSmall(_chip);
        imgAvatar.sprite = _spr;

        int id_Bor = MyInfo.MY_BOR_AVATAR;
        if (id_Bor != -1)
        {
            Sprite h = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
            if (h != null)
            {
                ImgBor.sprite = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
                ImgBor.gameObject.SetActive(true);
                ImgBor_Df.gameObject.SetActive(false);
            }
            else
            {
                ImgBor.gameObject.SetActive(false);
                ImgBor_Df.gameObject.SetActive(true);
            }

        }
        else
        {
            ImgBor.gameObject.SetActive(false);
            ImgBor_Df.gameObject.SetActive(true);
        }


    }
    public void updateInfoBet(long chip, string moneyBet, string moneyWin)
    {
        TxtChipUser.text = Utilities.GetStringMoneyByLongBigSmall(chip);
        TxtMoneyBet.text = moneyBet;
        TxtMoneyWin.text = moneyWin;
    }
    public void updateBetMoney(long moneyBet)
    {
        betMoney += moneyBet;
        TxtMoneyBet.text = Utilities.GetStringMoneyByLongBigSmall(betMoney);
    }
    public void updateChipUser(long chip)
    {
        TxtChipUser.text = Utilities.GetStringMoneyByLongBigSmall(chip);
    }
    
    public void Close()
    {
        LoadingManager.Instance.ENABLE = true;
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        SFS.Instance.SendRoomRequest(new GamePacket(CommandKey.USER_EXIT));        
    }
}

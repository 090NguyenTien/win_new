﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class BaseUserInfo {

	protected int idSFS;

	public int IdSFS {
		get {
			return idSFS;
		}
		set {
			idSFS = value;
		}
	}

	protected string idUser;

	public string IdUser {
		get {
			return idUser;
		}
		set {
			idUser = value;
		}
	}

	protected string name;

	public string Name {
		get {
			return name;
		}
		set {
			name = value;
		}
	}

	protected long chip;

	public long Chip {
		get {
			return chip;
		}
		set {
			chip = value;
		}
	}

    protected long gem;

    public long Gem
    {
        get
        {
            return gem;
        }
        set
        {
            gem = value;
        }
    }

    protected int vipPoint;

	public int VipPoint {
		get {
			return vipPoint;
		}
		set {
			vipPoint = value;
		}
	}

    private int level;
    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }

    protected string avatar;

	public string Avatar {
		get {                    
            return avatar;
		}
		set {
            avatar = value;
        }
	}

    protected string bor_avatar;

    public string Bor_Avatar
    {
        get
        {
            return bor_avatar;
        }
        set
        {
            bor_avatar = value;
        }
    }


    protected string vip_collec;

    public string Vip_collec
    {
        get
        {
            return vip_collec;
        }
        set
        {
            vip_collec = value;
        }
    }



    //Co trong ban hay ko
    public bool IsJoined {
		get;
		set;
	}
	//Choi hay ko
	public bool IsReady {
		get;
		set;
	}

//	public bool isJustJoin {
//		get;
//		set;
//	}


	public int ClientPos {
		get;
		set;
	}

    public string logoBangUrl
    {
		get;
		set;
	}

   
}

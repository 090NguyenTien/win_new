﻿using Com.TheFallenGames.OSA.Util.IO;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

public class ItemPT : MonoBehaviour
{
    [SerializeField]
    RemoteImageBehaviour image;
    public Text itemname;
    public AvoidItem item;
    private BaseCallBack.onCallBackStringInt callBack;
    string id, name, des;
    int bought, quantity; //quantity = -1 là ko phải bản limit
    long price, sale;
    [SerializeField]
    Button BtnDetail, BtnShowBuy;
    [SerializeField]
    Text txtBought, txtQuantity, txtPrice, txtSale;
    [SerializeField]
    GameObject line;
    public void init(JSONNode item, BaseCallBack.onCallBackStringInt callBack)
    {
        image.Load(API.Instance.DOMAIN + "/files/shop/" + item["image"]);
        name = item["name"];
        des = item["description"];
        id = item["_id"]["$id"];
        bought = int.Parse(item["bought"]);
        if (item["qty"] != null || item["qty"] != "" || item["qty"] != "null") quantity = int.Parse(item["qty"]);
        else quantity = 0;
        price = long.Parse(item["price"]);
        sale = long.Parse(item["sale"]);
        itemname.text = name;
        txtBought.text = "Đã Bán:" + bought.ToString();
        if (quantity == -1)
        {
            txtQuantity.gameObject.SetActive(false);
        }
        else
        {
            txtQuantity.gameObject.SetActive(true);
            txtQuantity.text = "Còn " + quantity + " Mẫu";
        }
        txtPrice.text = "Giá:"+ Utilities.GetStringMoneyByLongBigSmall(price);
        line.SetActive(false);
        if (sale > 0)
        {
            txtSale.gameObject.SetActive(true);
            txtSale.text = "Giảm Còn:"+ Utilities.GetStringMoneyByLongBigSmall(sale);
            line.SetActive(true);
        }
        else
        {
            txtSale.gameObject.SetActive(false);
        }
        //this.item = item;
        this.callBack = callBack;
        BtnDetail.onClick.AddListener(onClickDetail);
        BtnShowBuy.onClick.AddListener(onClickBuy);
    }

    private void onClickDetail()
    {
        callBack(des, 0);
    }
    private void onClickBuy()
    {
        long itemPrice = price;
        if (sale > 0) itemPrice = sale;
        callBack(id+","+itemPrice, 1);
    }
}

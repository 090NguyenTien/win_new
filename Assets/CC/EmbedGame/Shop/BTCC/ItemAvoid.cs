﻿using Com.TheFallenGames.OSA.Util.IO;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemAvoid : MonoBehaviour
{
    public RemoteImageBehaviour image;
    public Text itemname;
    public AvoidItem item;
    private BaseCallBack.onCallBackInt callBack;
    public void init(AvoidItem item, BaseCallBack.onCallBackInt callBack)
    {
        image.Load(API.Instance.DOMAIN + "/files/" + item.image);
        itemname.text = item.name;
        this.item = item;
        this.callBack = callBack;
        image.GetComponent<Button>().onClick.AddListener(onClick);
    }

    private void onClick()
    {
        callBack(item.id);
    }
}
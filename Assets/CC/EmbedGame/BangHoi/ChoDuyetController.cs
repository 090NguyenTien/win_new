﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoDuyetController : MonoBehaviour
{
    [SerializeField]
    GameObject container;
    [SerializeField]
    ItemDuyetController itemDuyetBanghoi;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Show()
    {
        for (int z = 0; z < container.transform.childCount; z++)
        {
            Destroy(container.transform.GetChild(z).gameObject);
        }
        GamePacket gp = new GamePacket(CommandKey.GET_LIST_CHO_DUYET);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case CommandKey.GET_LIST_CHO_DUYET:
                GetListDuyetThanhVienRes(param);
                break;
            case CommandKey.REQUEST_REQUIRE_JOIN:
                RequestRequiredJoinRes(param);
                break;
        }
    }

    private void RequestRequiredJoinRes(GamePacket param)
    {
        int response = param.GetInt("response");//1 đồng ý -1 không đồng ý
        int status = param.GetInt("status");//1 là thành công 
        //không show alert khi duyệt thành công chỉ alert khi thất bại
        if (status == -1)//thất bại
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }

    private void GetListDuyetThanhVienRes(GamePacket param)
    {
        ISFSArray members = param.GetSFSArray("members");        
        foreach (ISFSObject member in members)
        {
            ItemDuyetController item = Instantiate(itemDuyetBanghoi, container.transform) as ItemDuyetController;
            item.InitInfo(member);
            //item.AddCallBack(OnClickMenuAction);
        }
    }
}

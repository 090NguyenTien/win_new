﻿using Sfs2X.Entities.Data;
using System;
using UnityEngine;
using UnityEngine.UI;

public class InfoBangChien : MonoBehaviour
{
    [SerializeField]
    GameObject ContentListBang, ContentListUser, ViewListBang, ViewListUser;
    [SerializeField]
    ItemBangHoiChien itemBangChien;
    [SerializeField]
    ItemUserChienBang itemUserBangChien;
    [SerializeField]
    GameObject PopupConfirm, PopupListGame;
    [SerializeField]
    Text TxtTitleStatus;
    [SerializeField]
    Button BtnHuy, BtnDongY, BtnTuChoi, BtnThamChien;
    [SerializeField]
    Button XITO, MAUBINH, PHOM, TLMN, TLDL, BAICAO, LIENG;
    [SerializeField]
    GameObject GroupButtonConfirm, GroupThamChien;
    [SerializeField]
    TimeCountDownController TxtCountDown;
    string owner, deputy, deputy2;
    string IdMatchBattle;

    long chipKhieuChien;
    long ChipToiThieuKhieuChien = 1000000000;
    [SerializeField]
    Text TxtChipKhieuChien;
    [SerializeField]
    Button BtnCloseLstGame, BtnThele, BtnCloseThele, BtnLichSu, BtnCloseLichSu;
    [SerializeField]
    GameObject PanelThele, PanelLichSu, ContentLichSu;
    [SerializeField]
    ItemLichSuChienBang itemLichsuchienbang;
    // Start is called before the first frame update
    void Start()
    {
        InitRun();
        BtnCloseLstGame.onClick.AddListener(OnClosePopupLstGame);
        BtnHuy.onClick.AddListener(OnHuyLoiMoiChien);
        BtnDongY.onClick.AddListener(OnDongYLoiMoiChien);
        BtnTuChoi.onClick.AddListener(OnTuChoiLoiMoiChien);

        XITO.onClick.AddListener(OnClickXiToGame);
        MAUBINH.onClick.AddListener(OnClickMBGame);
        PHOM.onClick.AddListener(OnClickPhomGame);
        TLMN.onClick.AddListener(OnClickTLMNGame);
        TLDL.onClick.AddListener(OnClickTLDLGame);
        BAICAO.onClick.AddListener(OnClickBAICAOGame);
        LIENG.onClick.AddListener(OnClickLIENGGame);
        
        BtnThamChien.onClick.AddListener(OnClickThamChien);

        BtnThele.onClick.AddListener(OnClickThele);
        BtnCloseThele.onClick.AddListener(OnClickCloseThele);
        BtnLichSu.onClick.AddListener(OnClickLichSu);
        BtnCloseLichSu.onClick.AddListener(OnClickCloseLichSu);
    }

    private void OnClickCloseLichSu()
    {
        PanelLichSu.gameObject.SetActive(false);
    }

    private void OnClickLichSu()
    {
        GamePacket gp = new GamePacket("getClanWarHistory");
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }

    private void OnClickCloseThele()
    {
        PanelThele.SetActive(false);
    }

    private void OnClickThele()
    {
        PanelThele.SetActive(true);
    }

    private void OnClickLIENGGame()
    {
        if (chipKhieuChien < ChipToiThieuKhieuChien)
        {
            AlertController.api.showAlert("Chip Khiêu Chiến Tối Thiểu Là " + Utilities.GetStringMoneyByLongBigSmall(ChipToiThieuKhieuChien));
            return;
        }
        curGameChien = GAMEID.Lieng;
        isKhieuChien = true;
        GetUserList();
    }

    private void OnClickBAICAOGame()
    {
        if (chipKhieuChien < ChipToiThieuKhieuChien)
        {
            AlertController.api.showAlert("Chip Khiêu Chiến Tối Thiểu Là " + Utilities.GetStringMoneyByLongBigSmall(ChipToiThieuKhieuChien));
            return;
        }
        curGameChien = GAMEID.BaiCao;
        isKhieuChien = true;
        GetUserList();
    }

    private void OnClickTLDLGame()
    {
        if (chipKhieuChien < ChipToiThieuKhieuChien)
        {
            AlertController.api.showAlert("Chip Khiêu Chiến Tối Thiểu Là " + Utilities.GetStringMoneyByLongBigSmall(ChipToiThieuKhieuChien));
            return;
        }
        curGameChien = GAMEID.TLDL;
        isKhieuChien = true;
        GetUserList();
    }

    private void OnClickTLMNGame()
    {
        if (chipKhieuChien < ChipToiThieuKhieuChien)
        {
            AlertController.api.showAlert("Chip Khiêu Chiến Tối Thiểu Là " + Utilities.GetStringMoneyByLongBigSmall(ChipToiThieuKhieuChien));
            return;
        }
        curGameChien = GAMEID.TLMN;
        isKhieuChien = true;
        GetUserList();
    }

    private void OnClickPhomGame()
    {
        if (chipKhieuChien < ChipToiThieuKhieuChien)
        {
            AlertController.api.showAlert("Chip Khiêu Chiến Tối Thiểu Là " + Utilities.GetStringMoneyByLongBigSmall(ChipToiThieuKhieuChien));
            return;
        }
        curGameChien = GAMEID.Phom;
        isKhieuChien = true;
        GetUserList();
    }

    private void OnClosePopupLstGame()
    {
        PopupListGame.SetActive(false);
    }

    void InitRun()
    {
        BtnThele.gameObject.SetActive(true);
        PanelThele.SetActive(false);
        PanelLichSu.SetActive(false);
        ViewListBang.SetActive(false);
        ViewListUser.SetActive(false);
        PopupConfirm.SetActive(false);
        PopupListGame.SetActive(false);
        GroupButtonConfirm.SetActive(false);
        GroupThamChien.SetActive(false);
        BtnHuy.gameObject.SetActive(false);
        TxtTitleStatus.gameObject.SetActive(false);
        TxtTitleStatus.text = "";
    }
    void OnClickThamChien()
    {
        GameHelper.currentGid = (int)curGameChien;
        GamePacket gp = new GamePacket("fightClanDue");
        gp.Put("id", IdMatchBattle);
        SFS.Instance.SendRoomBangHoiRequest(gp);
        
        //gọi lấy thời gian còn lại của trận chiến
        GamePacket gp2 = new GamePacket("getClanWarTime");
        gp2.Put("id", IdMatchBattle);
        SFS.Instance.SendRoomBangHoiRequest(gp2);
        
    }
    private void OnClickMBGame()
    {
        if (chipKhieuChien < ChipToiThieuKhieuChien)
        {
            AlertController.api.showAlert("Chip Khiêu Chiến Tối Thiểu Là " + Utilities.GetStringMoneyByLongBigSmall(ChipToiThieuKhieuChien));
            return;
        }
        curGameChien = GAMEID.MauBinh;
        isKhieuChien = true;
        GetUserList();
        return;        
    }
    
    bool isKhieuChien=false;// true là khieu chien false la bị khieu chien
    GAMEID curGameChien = GAMEID.XiTo;
    private void OnClickXiToGame()
    {
        if (chipKhieuChien < ChipToiThieuKhieuChien)
        {
            AlertController.api.showAlert("Chip Khiêu Chiến Tối Thiểu Là " + Utilities.GetStringMoneyByLongBigSmall(ChipToiThieuKhieuChien));
            return;
        }
        curGameChien = GAMEID.XiTo;
        isKhieuChien = true;
        GetUserList();
        return;        
    }

    //Action của user nhận được thư khiêu chiến
    private void OnTuChoiLoiMoiChien()
    {
        GamePacket gp = new GamePacket("responseClanDue");
        gp.Put("response", -1);
        gp.Put("id", IdMatchBattle);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }

    private void OnDongYLoiMoiChien()
    {
        isKhieuChien = false;
        GetUserList();
    }
    //End
    private void OnHuyLoiMoiChien()
    {
        GamePacket gp = new GamePacket("cancelClanDue");
        gp.Put("id", IdMatchBattle);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void InitData()
    {
        ShowBangChien();
    }
    public void ShowBangChien()
    {        
        GamePacket gp = new GamePacket("getClanWarInfo");
        gp.Put("clanid", MyInfo.CLAN_ID);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case "getClanWarInfo":
                getClanWarInfoRes(param);
                break;
            case "cancelClanDue":
                CancelClanDueRsp(param);
                break;
            case CommandKey.GET_LIST_THANH_VIEN:
                LoadListUser(param);
                break;
            case CommandKey.GET_CLAN_LIST:
                ShowDanhSachBangHoi(param);
                break;
            case "sendClanDuel":
                SendClandDuelRsp(param);
                break;
            case "receiveClanDue":
                ReceiveClanDueRsp(param);
                break;
            case "responseClanDue":
                ResponseClanDue(param);
                break;
            case "noticeClanWarMatch":
                NoticeClanWarMatch(param);
                break;
            case "endClanWarMatch":
                EndClanWarMatch(param);
                break;
            case "fightClanDue":
                FightClanDue(param);
                break;
            case "clanWarMatchPause":
                ClanWarMatchPause(param);
                break;
            case "cancelClanDueCauseNotFight":
                CancelClanDueCauseNotFight(param);
                break;
            case "getClanWarHistory":
                GetClanWarHistory(param);
                break;
            case "noticeUserRejoin":
                NoticeUserRejoin(param);
                break;

        }
    }

    private void NoticeUserRejoin(GamePacket param)
    {
        if (param.GetString("rejoinuser") == MyInfo.NAME) return;
        AlertController.api.showAlert("Người Chơi "+param.GetString("rejoinuser") + " Đã Vào Để Tiếp Tục Trận Đấu", () => {
            InitRun();
            ShowBangChien();
        });
    }

    private void GetClanWarHistory(GamePacket param)
    {
        PanelLichSu.gameObject.SetActive(true);
        for (int z = 0; z < ContentLichSu.transform.childCount; z++)
        {
            Destroy(ContentLichSu.transform.GetChild(z).gameObject);
        }
        ISFSArray history = param.GetSFSArray("history");        
        foreach (ISFSObject child in history)
        {
            ItemLichSuChienBang item = Instantiate(itemLichsuchienbang, ContentLichSu.transform) as ItemLichSuChienBang;
            item.InitData(child);
        }
    }

    private void CancelClanDueCauseNotFight(GamePacket param)
    {
        AlertController.api.showAlert(param.GetString("message"), () => {
            InitRun();
            ShowBangChien();
        });
    }

    private void ClanWarMatchPause(GamePacket param)
    {
        AlertController.api.showAlert("Trận Đấu Đang Tạm Dừng Do " + param.GetString("exitusername") + " Đã Rời Cuộc Chiến Nếu Không Vào Lại Đúng Giờ Người Còn Lại Sẽ Thắng Cuộc", () => {
            InitRun();
            ShowBangChien();
        });
       
        throw new NotImplementedException();
    }

    private void FightClanDue(GamePacket param)
    {
        AlertController.api.showAlert(param.GetString("message"));
    }

    private void EndClanWarMatch(GamePacket param)
    {
        AlertController.api.showAlert(param.GetString("noti"), () => {
            InitRun();
            ShowBangChien();
        });
    }

    private void NoticeClanWarMatch(GamePacket param)
    {
        AlertController.api.showAlert("2 Bang Đã Bắt Đầu Trận Chiến! Hãy Vào Kênh Chat Để Bang Chủ Cập Nhật Tình Hình", () => {
            InitRun();
            ShowBangChien();
        });        
    }

    private void ResponseClanDue(GamePacket param)
    {
        int response = param.GetInt("response");
        if (response == 1)// đồng ý khiêu chiến
        {
            AlertController.api.showAlert(param.GetString("message"),()=> {
                InitRun();
                ShowBangChien();
            });
        }
        else if(response == -1)// từ chối khiêu chiến
        {
            AlertController.api.showAlert(param.GetString("message"), () => {
                InitRun();
                ShowBangChien();
            });
            //InitRun();
            //ShowBangChien();
        }
        else
        {
            AlertController.api.showAlert("Không thành công!");
        }
    }

    private void ReceiveClanDueRsp(GamePacket param)
    {
        InitRun();
        ShowBangChien();
    }

    private void SendClandDuelRsp(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert(param.GetString("message"),()=> {
                InitRun();
                ShowBangChien();
            });
        }
        else
        {
            AlertController.api.showAlert(param.GetString("message"));
        }
    }

    private void ShowDanhSachBangHoi(GamePacket param)
    {
        //    //load danh sach ba
        ISFSArray clans = param.GetSFSArray("clans");
        foreach (SFSObject clan in clans)
        {
            if (clan.GetUtfString("id") == MyInfo.CLAN_ID) continue;
            ItemBangHoiChien item = Instantiate(itemBangChien, ContentListBang.transform) as ItemBangHoiChien;
            item.InitInfo(clan, OnClickBang);
        }
        InitRun();
        ViewListBang.SetActive(true);
    }

    private void TuchoiThachDauRes(GamePacket param)
    {
        ShowBangChien();
    }

    private void getClanWarInfoRes(GamePacket param)
    {
        for (int z = 0; z < ContentListBang.transform.childCount; z++)
        {
            Destroy(ContentListBang.transform.GetChild(z).gameObject);
        }
        //Set Clan Info
        ISFSObject info = param.GetSFSObject("info");
        owner = info.GetUtfString("owner");
        if (info.ContainsKey("deputy"))
        {
            deputy = info.GetUtfString("deputy");            
        }

        if (info.ContainsKey("deputy2"))
        {
            deputy2 = info.GetUtfString("deputy2");            
        }
        //ennd
        //show Game from config
        VisibleGame();
        if (param.ContainKey("gameids"))
        {
            int[] gameids = param.GetIntArray("gameids");
            foreach (int idgame in gameids)
            {
                GAMEID game = (GAMEID)idgame;
                switch (game)
                {
                    case GAMEID.XiTo: XITO.gameObject.SetActive(true); break;
                    case GAMEID.MauBinh: MAUBINH.gameObject.SetActive(true); break;
                    case GAMEID.Phom: PHOM.gameObject.SetActive(true); break;
                    case GAMEID.TLMN: TLMN.gameObject.SetActive(true); break;
                    case GAMEID.TLDL: TLDL.gameObject.SetActive(true); break;
                    case GAMEID.BaiCao: BAICAO.gameObject.SetActive(true); break;
                    case GAMEID.Lieng: LIENG.gameObject.SetActive(true); break;
                }
            }
        }
        //end game
        if (param.ContainKey("clanduel")==false)//trạng thái lúc đầu chưa khiêu chiến
        {
            if (MyInfo.ID != deputy && MyInfo.ID != owner && MyInfo.ID != deputy2)
            {
                TxtTitleStatus.gameObject.SetActive(true);
                TxtTitleStatus.text = "Hiện Tại Bang Của Bạn Chưa Khiêu Chiến Bang Khác!";
            }
            else
            {
                //load danh sách bang hội
                GamePacket gp = new GamePacket(CommandKey.GET_CLAN_LIST);
                gp.Put("top_type", 1);
                SFS.Instance.SendZoneRequest(gp);
            }                      
        }
        else
        {
            ISFSObject clanduel = param.GetSFSObject("clanduel");
            int status = clanduel.GetInt("status");
            string idBangKhieuChien = clanduel.GetUtfString("clan_duel_id");
            string idBangDuocKhieuChien = clanduel.GetUtfString("clan_receive_duel_id");
            IdMatchBattle = clanduel.GetUtfString("id");
            if (MyInfo.CLAN_ID == idBangKhieuChien)//mình là bang khiêu chiến
            {
                if (status == 0)//đã gửi lời mời cho đối thủ
                {
                    PendingStatusKhieuChien(info, clanduel, true);
                }                
                else if (status == 1)//2 bên đã đồng ý đang chờ tham chiến.
                {
                    WaitConfirmUserBattle(info, clanduel, Int32.Parse(param.GetLong("timetofight").ToString()));
                }
                else if (status == 2)//trận đấu đang diễn ra
                {
                    BattleFighting(clanduel, true, Int32.Parse(param.GetLong("timetofight").ToString()));
                }else if(status==5)//trận đấu tạm dừng
                {
                    PauseBattle(info, clanduel, Int32.Parse(param.GetLong("timetofight").ToString()));
                }             
            }
            else if(MyInfo.CLAN_ID == idBangDuocKhieuChien)//bang được khiêu chiến
            {
                if (status == 0)//đã được đối thử gửi lời mời
                {                    
                    PendingStatusKhieuChien(info, clanduel, false);                   
                }
                else if (status == 1)//2 bên đã đồng ý đang chờ tham chiến.
                {
                    WaitConfirmUserBattle(info, clanduel, Int32.Parse(param.GetLong("timetofight").ToString()));
                }
                
                else if (status == 2)//trận đấu đang diễn ra
                {
                    BattleFighting(clanduel, false, Int32.Parse(param.GetLong("timetofight").ToString()));
                }
                else if (status==5)//trận đấu tạm dừng
                {
                    PauseBattle(info, clanduel, Int32.Parse(param.GetLong("timetofight").ToString()));
                }
            }
        }
        
    }

    private void PauseBattle(ISFSObject info, ISFSObject clanduel, int timeOfFight)
    {
        // trả về thời gian còn lại của trận đấu        
        TxtTitleStatus.gameObject.SetActive(true);
        TxtTitleStatus.text = "Trận Đấu Đang Tạm Dừng Do 1 Trong 2 Người Chơi Rời Khỏi Phòng Vui Lòng Vào Lại Trước Khi Hết Thời Gian Nếu Không Người Còn Lại Sẽ Thắng Cuộc";
        int secondDelay = timeOfFight;
        TxtCountDown.gameObject.SetActive(true);
        TxtCountDown.initTimeCountDown(0, secondDelay, 0, null);
        if (MyInfo.ID != deputy2 && MyInfo.ID != deputy && MyInfo.ID != owner)//khong phải bang chủ và bang phó chỉ hiển thị text
        {
            if (MyInfo.ID == clanduel.GetUtfString("duel_member") || MyInfo.ID == clanduel.GetUtfString("fight_member"))//user được chọn khiêu chiến
            {
                curGameChien = (GAMEID)clanduel.GetInt("gameid");
                GroupThamChien.SetActive(true);
                PopupConfirm.SetActive(false);
                BtnHuy.gameObject.SetActive(false);
            }
            else//user thường ko dc chọn
            {
                GroupThamChien.SetActive(false);
                PopupConfirm.SetActive(false);
                BtnHuy.gameObject.SetActive(false);
            }
        }
        else//trường hợp là bang chủ và bang phó
        {
            if (MyInfo.ID == owner || MyInfo.ID == clanduel.GetUtfString("duel_member") || MyInfo.ID == clanduel.GetUtfString("fight_member"))           
            {
                curGameChien = (GAMEID)clanduel.GetInt("gameid");
                GroupThamChien.SetActive(true);
                PopupConfirm.SetActive(false);
                BtnHuy.gameObject.SetActive(false);
            }
            else
            {
                GroupThamChien.SetActive(false);
                PopupConfirm.SetActive(false);
                BtnHuy.gameObject.SetActive(false);
            }
        }
    }

    private void VisibleGame()
    {
        XITO.gameObject.SetActive(false);
        MAUBINH.gameObject.SetActive(false);
        PHOM.gameObject.SetActive(false);
        TLMN.gameObject.SetActive(false);
        TLDL.gameObject.SetActive(false);
        BAICAO.gameObject.SetActive(false);
        LIENG.gameObject.SetActive(false);
    }
    

    private void BattleFighting(ISFSObject clanduel, bool isKchien, int timeOfFight)
    {
        if (MyInfo.ID == clanduel.GetUtfString("duel_member") || MyInfo.ID == clanduel.GetUtfString("fight_member"))//user được chọn khiêu chiến
        {
            curGameChien = (GAMEID)clanduel.GetInt("gameid");
            GroupThamChien.SetActive(true);
            PopupConfirm.SetActive(false);
            BtnHuy.gameObject.SetActive(false);
        }
        else
        {
            string msg = "";
            string clanName = "";
            string userUngChien = "";
            if (isKchien)//bang khieu chiến
            {
                msg = "Bang Của Bạn Đang Khiêu Chiến Bang ";
                clanName = clanduel.GetUtfString("clan_duel_name");
                userUngChien = clanduel.GetUtfString("duel_member");
            }
            else//bang bi khieu chiến
            {
                msg = "Bang Của Bạn Đang Khiêu Chiến Bang ";
                clanName = clanduel.GetUtfString("clan_receive_duel_name");
                userUngChien = clanduel.GetUtfString("fight_member");
            }
            TxtTitleStatus.gameObject.SetActive(true);
            TxtTitleStatus.text = msg + clanName +
                "\n Mức Cược : " + Utilities.GetStringMoneyByLongBigSmall(clanduel.GetLong("betting_money")) +
                "\n Vào Lúc : " + clanduel.GetUtfString("created_date") +
                "\n Ứng Chiến : " + userUngChien;

            PopupConfirm.SetActive(false);
            BtnHuy.gameObject.SetActive(false);
            GroupButtonConfirm.gameObject.SetActive(false);
            GroupThamChien.SetActive(false);
        }
        int secondDelay = timeOfFight;
        TxtCountDown.gameObject.SetActive(true);
        TxtCountDown.initTimeCountDown(0, secondDelay, 0, null);
    }

    private void PendingStatusKhieuChien(ISFSObject info, ISFSObject clanduel,bool isKchien)
    {
        string msg = "";
        string clanName = "";
        string userUngChien = "";
        if (isKchien)//bang khieu chiến
        {
            msg = "Bang Của Bạn Đã Gửi Lời Khiêu Chiến Tới Bang ";
            clanName = clanduel.GetUtfString("clan_receive_duel_name");
            userUngChien = clanduel.GetUtfString("duel_member_name");
        }
        else//bang bi khieu chiến
        {
            msg = "Bang Của Bạn Nhận Được Lời Khiêu Chiến Từ Bang ";
            clanName = clanduel.GetUtfString("clan_duel_name");
            userUngChien = clanduel.GetUtfString("fight_member_name");
        }
        TxtTitleStatus.gameObject.SetActive(true);
        TxtTitleStatus.text = msg + clanName +
            "\n Mức Cược : " + Utilities.GetStringMoneyByLongBigSmall(clanduel.GetLong("betting_money")) +
            "\n Vào Lúc : " + clanduel.GetUtfString("created_date") +
            "\n Ứng Chiến : " + userUngChien;
        if (MyInfo.ID != deputy2 && MyInfo.ID != deputy && MyInfo.ID != owner)//khong phải bang chủ và bang phó chỉ hiển thị text
        {
            PopupConfirm.SetActive(false);
            BtnHuy.gameObject.SetActive(false);
            GroupButtonConfirm.gameObject.SetActive(false);
            GroupThamChien.SetActive(false);
        }
        else
        {
            if (isKchien)
            {
                PopupConfirm.SetActive(true);
                BtnHuy.gameObject.SetActive(true);
                GroupButtonConfirm.gameObject.SetActive(false);
                
            }
            else
            {
                PopupConfirm.SetActive(true);
                GroupButtonConfirm.gameObject.SetActive(true);
                BtnHuy.gameObject.SetActive(false);
            }
            ViewListBang.SetActive(false);
            GroupThamChien.SetActive(false);
        }
    }

    private void WaitConfirmUserBattle(ISFSObject info, ISFSObject clanduel, int timeOfFight)
    {
        // trả về thời gian còn lại của trận đấu
        // có nút tham chiến đối với người được chọn (click vào thì sẽ bay qua game khiêu chiến)
        TxtTitleStatus.gameObject.SetActive(true);
        TxtTitleStatus.text = "2 Bang Đã Chấp Nhận Lời Khiêu Chiến. Vui Lòng Gọi Bang Viên Được Chọn Thi Đấu Vào Tham Chiến";
        int secondDelay = timeOfFight;
        TxtCountDown.gameObject.SetActive(true);
        TxtCountDown.initTimeCountDown(0, secondDelay, 0, null);
        if (MyInfo.ID != deputy2 && MyInfo.ID != deputy && MyInfo.ID != owner)//khong phải bang chủ và bang phó chỉ hiển thị text
        {
            if (MyInfo.ID == clanduel.GetUtfString("duel_member") || MyInfo.ID == clanduel.GetUtfString("fight_member"))//user được chọn khiêu chiến
            {
                curGameChien = (GAMEID)clanduel.GetInt("gameid");                
                GroupThamChien.SetActive(true);
                PopupConfirm.SetActive(false);
                BtnHuy.gameObject.SetActive(false);
            }
            else//user thường ko dc chọn
            {
                GroupThamChien.SetActive(false);
                PopupConfirm.SetActive(false);
                BtnHuy.gameObject.SetActive(false);
            }
        }
        else//trường hợp là bang chủ và bang phó
        {
            if (MyInfo.ID == owner || MyInfo.ID == clanduel.GetUtfString("duel_member") || MyInfo.ID == clanduel.GetUtfString("fight_member"))            
            {
                curGameChien = (GAMEID)clanduel.GetInt("gameid");
                GroupThamChien.SetActive(true);
                PopupConfirm.SetActive(false);
                BtnHuy.gameObject.SetActive(false);
            }
            else
            {
                GroupThamChien.SetActive(false);
                PopupConfirm.SetActive(false);
                BtnHuy.gameObject.SetActive(false);
            }
            
        }
    }

    void CancelClanDueRsp(GamePacket param)
    {
        int status = param.GetInt("status");
        Debug.Log("CancelClanDueRsp===========" + status);
        if (status == 1)
        {
            InitRun();
            ShowBangChien();
        }
        else
        {
            AlertController.api.showAlert(param.GetString("message"));
        }
        
    }
    //void ChonUserDauRes(GamePacket param)//gửi lời mời đấu thì hiển thị cho toàn bộ thành viên bang 1 cái popup tức thời.
    //{
    //    TxtTitleStatus.gameObject.SetActive(true);
    //    TxtTitleStatus.text = param.GetString("msg");
    //    PopupConfirm.SetActive(true);
    //    BtnHuy.gameObject.SetActive(true);
    //}
    string curIdBang="";
    private void OnClickBang(string idBang)
    {
        curIdBang = idBang;
        PopupListGame.SetActive(true);
        chipKhieuChien = 0;
        TxtChipKhieuChien.text = "0";
    }
    void GetUserList()
    {
        GamePacket gp = new GamePacket(CommandKey.GET_LIST_THANH_VIEN);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    
    void LoadListUser(GamePacket param)
    {
        ViewListBang.SetActive(false);
        PopupListGame.SetActive(false);
        ViewListUser.SetActive(true);
        for (int z = 0; z < ContentListUser.transform.childCount; z++)
        {
            Destroy(ContentListUser.transform.GetChild(z).gameObject);
        }
        ISFSArray members = param.GetSFSArray("members");
        deputy = "";
        if (param.ContainKey("deputy"))
        {
            deputy = param.GetString("deputy");
        }
        if (param.ContainKey("deputy2"))
        {
            deputy2 = param.GetString("deputy2");
        }
        owner = param.GetString("owner");
        foreach (ISFSObject member in members)
        {
            ItemUserChienBang item = Instantiate(itemUserBangChien, ContentListUser.transform) as ItemUserChienBang;
            item.InitData(member, ChoseUserDau);            
        }
    }

    private void ChoseUserDau(string idUser)
    {
        if (isKhieuChien)//bang khieu chien
        {
            if(chipKhieuChien < ChipToiThieuKhieuChien)
            {
                AlertController.api.showAlert("Chip Khiêu Chiến Tối Thiểu Là "+Utilities.GetStringMoneyByLongBigSmall(ChipToiThieuKhieuChien));
                return;
            }
            Debug.Log("ChoseUserDau==== gameid:::" + (int)curGameChien);
            GamePacket gp = new GamePacket("sendClanDuel");
            gp.Put("gameid", (int)curGameChien);
            gp.Put("clan_id", curIdBang);
            gp.Put("dual_user_id", idUser);
            gp.Put("betmoney", chipKhieuChien);
            SFS.Instance.SendRoomBangHoiRequest(gp);
        }
        else//bang bi khieu chien
        {
            GamePacket gp = new GamePacket("responseClanDue");
            gp.Put("response", 1);
            gp.Put("id", IdMatchBattle);
            gp.Put("fight_member_id", idUser);
            SFS.Instance.SendRoomBangHoiRequest(gp);
        }
        
    }
    
    public void AddChipKhieuChien(string val)
    {
        chipKhieuChien += long.Parse(val);
        TxtChipKhieuChien.text = Utilities.GetStringMoneyByLongBigSmall(chipKhieuChien);
    }
}

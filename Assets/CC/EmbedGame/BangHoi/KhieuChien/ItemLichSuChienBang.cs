﻿using BaseCallBack;
using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemLichSuChienBang : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI TxtNameUC, TxtBangKC, TxtStatus;
    [SerializeField]
    Text TxtChip, TxtTime;
    
    string idUser;
    onCallBackString callback;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    

    public void InitData(ISFSObject data)
    {
        //fight_member_name : user bi khieu chien
        //duel_member_name : user khieu chien
        //
        string UNameUngChien = "";
        string nameBangThang = "";
        if (MyInfo.CLAN_ID == data.GetUtfString("clan_duel_id"))//bang mình khiêu chiến
        {
            UNameUngChien = data.GetUtfString("duel_member_name");
            TxtBangKC.text = data.GetUtfString("clan_receive_duel_name");            
        }
        else if (MyInfo.CLAN_ID == data.GetUtfString("clan_receive_duel_id"))//bang mình bị khiêu chiến
        {
            UNameUngChien = data.GetUtfString("fight_member_name");
            TxtBangKC.text = data.GetUtfString("clan_duel_name");
        }
        TxtNameUC.text = UNameUngChien;
        int status = data.GetInt("status");
        if (status == 3)//kết thúc trận có thắng thua
        {
            TxtStatus.text = "";
            if (data.GetUtfString("clan_win") == data.GetUtfString("clan_duel_id"))
            {
                nameBangThang = data.GetUtfString("clan_duel_name");
            }else if (data.GetUtfString("clan_win") == data.GetUtfString("clan_receive_duel_id"))
            {
                nameBangThang = data.GetUtfString("clan_receive_duel_name"); 
            }
            TxtStatus.text = nameBangThang;
        }else if (status == -2)
        {
            TxtStatus.text = "Không Đấu";
        }
        TxtChip.text = Utilities.GetStringMoneyByLongBigSmall(data.GetLong("betting_money"));
        TxtTime.text = data.GetUtfString("created_date");
        //TxtName.text = data.GetUtfString("username");        
        //TxtChip.text = Utilities.GetStringMoneyByLongBigSmall(data.GetLong("chip"));        
    }
    
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TienIchController : MonoBehaviour
{
    [SerializeField]
    Button BtnBangHoi, BtnKetSat, BtnClose;
    [SerializeField]
    BaseController BaseControll;
    // Start is called before the first frame update
    void Start()
    {
        BtnBangHoi.onClick.AddListener(OnClickShowBangHoi);
        BtnKetSat.onClick.AddListener(OnClickShowKetSat);
        BtnClose.onClick.AddListener(OnCloseTienIch);
        this.name = "PanelTienIch";
    }

    public void OnCloseTienIch()
    {
        Destroy(gameObject);
    }

    private void OnClickShowKetSat()
    {
        GameObject parent = gameObject;       
        GameObject ModuleKetSat = Instantiate(Resources.Load("Modules/KETSAT", typeof(GameObject)), new Vector3(0, 0, 0), Quaternion.identity, parent.transform) as GameObject;
        ModuleKetSat.name = "KETSAT";
        ModuleKetSat.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        ModuleKetSat.GetComponent<KetSatConTroll>().OpenFromChatTienIch();
    }

    private void OnClickShowBangHoi()
    {
        GameObject parent = gameObject;
        //BaseControll.OpenModuleInfoBangHoi(parent);
        GameObject ModuleBangHoi = Instantiate(Resources.Load("Modules/PanelBangHoi", typeof(GameObject)), new Vector3(0, 0, 0), Quaternion.identity, parent.transform) as GameObject;

        ModuleBangHoi.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        ModuleBangHoi.transform.Find("Menu").gameObject.SetActive(false);
        ModuleBangHoi.GetComponent<BanghoiController>().isLoadFromChat=true;
    }

}

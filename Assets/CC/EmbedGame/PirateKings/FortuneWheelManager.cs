﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using BaseCallBack;
using DG.Tweening;
using Sfs2X.Entities.Data;

public class FortuneWheelManager : MonoBehaviour
{
    private bool _isStarted;
    private float[] _sectorsAngles;
    private float _finalAngle;
    private float _startAngle = 0;
    private float _currentLerpRotationTime;
    public Button TurnButton;
    public GameObject Circle; 			// Rotatable Object with rewards
    public GameObject ResultObj;
    public Image[] ResultImg;
    public Text ResultWheelText; 		
    public Text CurrentGemText; 		
   
    const String COMMAND_WHEEL = "pirateWheel";
    const String COMMAND_ATTACK = "pirateAttack";
    const String COMMAND_CHANGE_ATTACK = "changeVictim";
    string curResult = "";
    string curIdBeAttack="";
    [SerializeField]
    MadControll Mad;
    [SerializeField]
    Button BtnClose;
    [SerializeField]
    GameObject PanelAttack;
    [SerializeField]
    GameObject ReceiveChipEffect;
    public Text GemTurn;
    public Text TxtUserName;
    [SerializeField]
    GameObject ListKethu, ContainerKethu, BtnShowKeThu;
    [SerializeField]
    ItemKethu InstanceItemKethu;
    [SerializeField]
    Text TxtValueChangeAttack, TxtChipReceive;
    [SerializeField]
    GameObject Weapon, Panel_Quay, Panel_Ban, PanelTheLe;
    public static FortuneWheelManager api { get; private set; }
    string IdTaiSan_Click = "";



    
    private void Awake ()
    {
        api = this;
        _isStarted = false;
        //PreviousCoinsAmount = CurrentCoinsAmount;
        CurrentGemText.text = MyInfo.GEM.ToString();
        GemTurn.text = MyInfo.GEM_PIRATE.ToString();
        TxtValueChangeAttack.text = MyInfo.GEM_PIRATE.ToString();
        BtnClose.gameObject.SetActive(false);
        Mad.gameObject.SetActive(false);
        Panel_Ban.SetActive(false);
        Panel_Quay.SetActive(true);

        Weapon.SetActive(false);
        TxtChipReceive.gameObject.SetActive(false);
        TxtUserName.gameObject.SetActive(false);
        PanelAttack.SetActive(false);
        //ResultWheelText.gameObject.SetActive(false);
        ResultObj.gameObject.SetActive(false);
        VisibleResultImgage(-1);
        ReceiveChipEffect.SetActive(false);
    }
    public void DoiNguoiTanCong()
    {
        GamePacket packet = new GamePacket(COMMAND_CHANGE_ATTACK);
        SFS.Instance.SendZoneRequest(packet);
    }
    private void VisibleResultImgage(int v)//0:chip 1:Attack 2:Khien 3:Freewheel
    {
        for(int i = 0; i < ResultImg.Length; i++)
        {
            ResultImg[i].gameObject.SetActive(false);
        }
        if (v != -1)
        {
            ResultImg[v].gameObject.SetActive(true);
        }
    }

    public void OpenTheLe()
    {
        PanelTheLe.SetActive(true);
    }
    public void CloseTheLe()
    {
        PanelTheLe.SetActive(false);
    }

    public void CloseTaiSan()
    {
        IdTaiSan_Click = "";
        BtnClose.gameObject.SetActive(false);
        Mad.gameObject.SetActive(false);

        Panel_Ban.SetActive(false);
        Panel_Quay.SetActive(true);

        TxtUserName.gameObject.SetActive(false);
        ClosePanelAttack();
        Weapon.SetActive(false);
        _isStarted = false;
    }
    public void OpenTaisan()
    {
        IdTaiSan_Click = "";
        BtnClose.gameObject.SetActive(true);
        Mad.gameObject.SetActive(true);

        Panel_Ban.SetActive(true);
        Panel_Quay.SetActive(false);

        TxtUserName.gameObject.SetActive(true);
        Mad.XemTaiSan(curIdBeAttack);
        Mad.AddCallBackClickItem(ItemTaiSanOnClick);
    }
    void ItemTaiSanOnClick(string idTaiSan)
    {
        //Debug.Log("isShooting=========" + isShooting);
        if (isShooting) return;
        Debug.LogError("idTaiSan================" + idTaiSan);
        IdTaiSan_Click = idTaiSan;
        GamePacket packet = new GamePacket(COMMAND_ATTACK);
        packet.Put("attack_assetid", idTaiSan);
        SFS.Instance.SendZoneRequest(packet);
    }
    public void ClosePirateKings()
    {
        ModuleLoadPrefabScript ModuleParent = gameObject.transform.parent.GetComponent<ModuleLoadPrefabScript>();
        if (ModuleParent != null)
        {
            ModuleParent.UpdateInfoHome();
        }
        SoundManager.StopSound(SoundManager.WHEEL_STAR);
        SoundManager.StopSound(SoundManager.WHEEL_END);
        Destroy(gameObject);
    }
    public void TanCong()
    {
        ClosePanelAttack();
        ShowWeaponAttack();
    }

    private void ShowWeaponAttack()
    {
        Weapon.SetActive(true);
        Weapon.GetComponent<Animator>().Play("WeaponNormal");
    }

    public void ClosePanelAttack()
    {
        PanelAttack.SetActive(false);
    }
    public void SetDataClickKeThu(string _idKethu, string _tenKethu)
    {        
        curIdBeAttack = _idKethu;
        TxtUserName.text = _tenKethu;
        OpenTaisan();
        TanCong();
    }
    //public void ShowUserHouseAttack()
    //{
    //    OpenTaisan();
    //}
    int convertTypeToSlot(string type, long receiveChip)
    {
        //lucky, attack,shield,freewheel
        int indexSlot=0;
        switch (type)
        {
            case "lucky":                
                indexSlot = 0;
                ResultWheelText.text = receiveChip.ToString();
                VisibleResultImgage(0);
                break;
            case "attack":
                indexSlot = 1;
                ResultWheelText.text = "Tấn Công";
                VisibleResultImgage(1);
                break;
            case "shield":
                indexSlot = 2;
                ResultWheelText.text = "Khiêng";
                VisibleResultImgage(2);
                break;
            case "freewheel":
                indexSlot = 3;
                ResultWheelText.text = "Thêm Lượt";
                VisibleResultImgage(3);
                break;
        }
        // mỗi loại cách nhau 4 slot nên client tự random 1 trong 3 slot trên vòng quay
        int random = UnityEngine.Random.Range(0, 3);
        if (random == 1)
        {
            indexSlot += 4;
        }else if (random == 2)
        {
            indexSlot += 8;
        }
        //end
        Debug.Log("==========Pirate King====="+ indexSlot);
        return indexSlot;
    }
    public void TurnWheel ()
    {
        if (MyInfo.GEM < MyInfo.GEM_PIRATE)
        {
            AlertController.api.showAlert("Bạn Không Đủ Gem Để Quay!");
            return;
        }

        SendWheelToServer();
        //RspTurnWheelPirate(null);


    }
    void SendWheelToServer()
    {
        if (_isStarted == true) return;
        _isStarted = true;
        GamePacket packet = new GamePacket(COMMAND_WHEEL);
        SFS.Instance.SendZoneRequest(packet);
    }
    public void OnSFSResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case COMMAND_WHEEL:
                RspTurnWheelPirate(param);
                break;
            case COMMAND_ATTACK:
                RspPirateAttack(param);
                break;
            case COMMAND_CHANGE_ATTACK:
                RspPirateChangeAttack(param);
                break;
        }
    }
    private void RspPirateChangeAttack(GamePacket gp)
    {
        //status: -1: lỗi bt chỉ show msg
        int status = gp.GetInt("status");
        if (status == -1)
        {
            AlertController.api.showAlert(gp.GetString("msg"));
            return;
        }
        if (gp.ContainKey("victimid")) curIdBeAttack = gp.GetString("victimid");
        if (gp.ContainKey("gem")) MyInfo.GEM = gp.GetLong("gem");
        if (gp.ContainKey("victimname")) TxtUserName.text = gp.GetString("victimname");

        OpenTaisan();

    }
    long curHealth = 0;
    long curMaxHealth = 0;
    long chipReceive = 0;
    bool isShooting = false;
    private void RspPirateAttack(GamePacket gp)
    {
        //status: -1: lỗi bt chỉ show msg, -2: lỗi nhà chọn tấn công đã bị phá hủy (load lại map của nạn nhân)
        int status = gp.GetInt("status");
        //test Khieng
        //status = -3;
        Debug.LogWarning("status ------- Ban = " + status);
        
        if (status == -1)
        {            
            AlertController.api.showAlert(gp.GetString("msg"));
            IdTaiSan_Click = "";
            return;
        }else if (status == -2)
        {
            AlertController.api.showAlert("Nhà này vừa bị người chơi khác phá hủy! Bạn chậm một nhịp rồi");
            IdTaiSan_Click = "";
            return;
        }else if (status == -3)
        {
            isShooting = true;
            PanelAttack.gameObject.SetActive(false);
            long health_ = curHealth = gp.GetLong("health");
            long maxAssetHealth_ = curMaxHealth = gp.GetLong("maxAssetHealth");
            StartCoroutine(Khieng());
            return;
        }
        isShooting = true;
        PanelAttack.gameObject.SetActive(false);
        long health = curHealth = gp.GetLong("health");
        long maxAssetHealth = curMaxHealth = gp.GetLong("maxAssetHealth");
        long spoilAmount = chipReceive = gp.GetLong("spoilAmount");//chip nhận dc
        long chip = gp.GetLong("chip");//chip user        
        //animation chip nhận được
        Debug.LogWarning("status ----Xuong--- Ban = ");
        StartCoroutine(AnimationWeaponAttack());
        
    }

    private IEnumerator Khieng()
    {
        Weapon.GetComponent<Animator>().Play("WeaponAttack");
        yield return new WaitForSeconds(1f);

        Mad.TruMauNhaBitanCong(IdTaiSan_Click, curHealth, curMaxHealth, true);
    }

    private IEnumerator AnimationWeaponAttack()
    {
        Weapon.GetComponent<Animator>().Play("WeaponAttack");
        yield return new WaitForSeconds(1f);
       
        StartCoroutine(ShowEffectChipWin());
    }
    
    private IEnumerator ShowEffectChipWin()
    {
        Mad.TruMauNhaBitanCong(IdTaiSan_Click, curHealth, curMaxHealth);
        //ReceiveChipEffect.SetActive(true);
        TxtChipReceive.text = "+" + Utilities.GetStringMoneyByLong(chipReceive);//.ToString();
        
        yield return new WaitForSeconds(3f);
        TxtChipReceive.gameObject.SetActive(true);
        iTween.MoveTo(TxtChipReceive.gameObject, iTween.Hash("position", new Vector3(0, 142, 0), "islocal", true, "onupdatetarget", gameObject, "onupdate", "onUpdateDice", "oncompletetarget", gameObject, "oncomplete", "onCompleteAnimationChipReceive", "time", 1.5f));
        Weapon.SetActive(false);
        yield return new WaitForSeconds(3f);
        // ReceiveChipEffect.SetActive(false);
        TxtChipReceive.gameObject.SetActive(false);
        //tru mau nha doi thu sau khi bi tan cong;
        //int health = gp.GetInt("health");
        //int maxAssetHealth = gp.GetInt("maxAssetHealth");
       
        IdTaiSan_Click = "";
    }
    void onCompleteAnimationChipReceive()
    {
        TxtChipReceive.transform.localPosition = new Vector3(0, 42, 0);
        TxtChipReceive.gameObject.SetActive(false);
    }
    public void RspTurnWheelPirate(GamePacket gp)
    {
        ListKethu.SetActive(false);
        // Player has enough money to turn the wheel
        //if (CurrentCoinsAmount >= TurnCost)
        //{
        int status = gp.GetInt("status");
        if (status == -1)
        {
            _isStarted = false;
            AlertController.api.showAlert(gp.GetString("msg"));
            return;
        }
        else
        {
            string result = "";
            long result_chip = 0;
            long chipUser = MyInfo.CHIP;
            string victimid = "";
            int shield = 0;
            long gem = MyInfo.GEM;
            if (gp.ContainKey("result")) result = gp.GetString("result");
            if (gp.ContainKey("result_chip")) result_chip = gp.GetLong("result_chip");
            if (gp.ContainKey("chip")) chipUser = gp.GetLong("chip");
            if (gp.ContainKey("victimid")) curIdBeAttack = victimid = gp.GetString("victimid");
            if (gp.ContainKey("shield")) shield = gp.GetInt("shield");
            if (gp.ContainKey("gem")) gem = gp.GetLong("gem");
            if (gp.ContainKey("victimname")) TxtUserName.text = gp.GetString("victimname");
            if (gp.ContainKey("enemy"))
            {
                //clear all list
                for (int i = 0; i < ContainerKethu.transform.childCount; i++)
                {
                    GameObject.Destroy(ContainerKethu.transform.GetChild(i).gameObject);
                }
                //end
                ISFSArray Enemies = gp.GetSFSArray("enemy");
                foreach (ISFSObject enemy in Enemies)
                {
                    ItemKethu item = Instantiate(InstanceItemKethu, ContainerKethu.transform) as ItemKethu;
                    item.Init(enemy.GetUtfString("id"), enemy.GetUtfString("username"));                    
                }
                
                BtnShowKeThu.SetActive(true);
            }
            else
            {
                BtnShowKeThu.SetActive(false);
            }
            MyInfo.GEM = gem;
            CurrentGemText.text = gem.ToString();
            if (result == "")
            {
                AlertController.api.showAlert("Không thể quay lúc này! Vui lòng liên hệ admin");
                return;
            }
            TxtChipReceive.text = "+" + Utilities.GetStringMoneyByLong(result_chip);
            curResult = result;
            isShooting = false;
            int index = convertTypeToSlot(result, result_chip);
            StartLuckyWheel(index, () =>
            {
                GiveAwardByAngle();
            });

            return;
            
        }
    }
    [SerializeField]
    private Animator animLighting = null;
    [SerializeField]
    Transform trsfLuckyWheel;
    int numPart = 12;//so phan chia tren wheel
    const int angle = 30;//1 phan chiem 45 do
    const int Loop = 10;
    private Ease easeType = Ease.OutQuad;//Ease.InOutQuint;
    private float TimeWheel = 3;

    void StartLuckyWheel(int _index, onCallBack _callBack = null)
    {
        SoundManager.PlaySound(SoundManager.WHEEL_STAR,true);
        Vector3 result = GetRotationZ(_index);
        Debug.LogError("_index GetRotationZ:   " + _index + "    result   " + result);

        animLighting.SetTrigger("ShowWheeling");

        trsfLuckyWheel.DOKill(false);
        //trsfLuckyWheel.DORotate(result, UnityEngine.Random.Range(TimeWheel - 2, TimeWheel + 4), RotateMode.FastBeyond360).SetEase(easeType).OnComplete(() =>
        trsfLuckyWheel.DORotate(result, TimeWheel, RotateMode.FastBeyond360).SetEase(easeType).OnComplete(() =>
        {            
            _callBack();
            animLighting.SetTrigger("ShowWheelComplete");
            ShowResultText();
            SoundManager.StopSound(SoundManager.WHEEL_STAR);
            SoundManager.PlaySound(SoundManager.WHEEL_END);
        });
    }
    
    Vector3 GetRotationZ(int _index)
    {
        //Debug.Log(_index);
        int centerAngle = 360 - _index * angle;
        int deltaAngle = UnityEngine.Random.Range(-angle / 3, angle / 3);
        int rotation = centerAngle + deltaAngle;
        float result = -(rotation - (UnityEngine.Random.Range(Loop, Loop + 6) * 360));
        return new Vector3(0, 0, result);
    }
    private void GiveAwardByAngle ()
    {
        //RewardCoins(1000);
        StartCoroutine(ShowResultAward());

        return;
     
    }
    public void ShowKeThu()
    {
        ListKethu.SetActive(!ListKethu.activeSelf);
    }
    private IEnumerator ShowResultAward()
    {
        yield return new WaitForSeconds(1.7f);
        if (curResult == "freewheel")
        {
            _isStarted = false;
            SendWheelToServer();
        }
        else if (curResult == "attack")
        {            
            //show đổi kẻ thù
            PanelAttack.SetActive(true);
            OpenTaisan();
        }
        else
        {
            _isStarted = false;
        }

       
    }

    private void ShowResultText()
    {
        if (curResult == "lucky")
        {
            //StartCoroutine(ShowEffectChipWin());
        }
        ResultObj.gameObject.SetActive(true);
        ResultObj.transform.localPosition = new Vector3(0, 60, 0);
        iTween.MoveTo(ResultObj.gameObject, iTween.Hash("position", new Vector3(0, 150, 0), "islocal", true, "onupdatetarget", gameObject, "onupdate", "onUpdateDice", "oncompletetarget", gameObject, "oncomplete", "onCompleteAnimationWheel", "time", 1.5f));
    }
    void onCompleteAnimationWheel()
    {
        ResultObj.transform.localPosition = new Vector3(0, 60, 0);
        ResultObj.gameObject.SetActive(false);
    }
    

    private IEnumerator HideCoinsDelta ()
    {
        yield return new WaitForSeconds (1f);
	    
    }
    
}

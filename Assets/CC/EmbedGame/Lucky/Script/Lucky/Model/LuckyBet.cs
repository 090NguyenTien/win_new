﻿using UnityEngine;
using System.Collections;
using System;

public class LuckyBet {
    public double taiChip = 0;
    public double xiuChip = 0;

    public void setData(string[] data) {
        xiuChip = Convert.ToDouble(data[0]);
        taiChip = Convert.ToDouble(data[1]);
    }

    public void Reset()
    {
        xiuChip = 0;
        taiChip = 0;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUserGiaiDau : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI txtName, txtStatus;
    [SerializeField]
    Button BtnJoin, BtnThang, BtnReJoin;    
    string IDUser, Uname, IdCapDau;     
    // Start is called before the first frame update
    void Start()
    {
        BtnJoin.onClick.AddListener(OnJoinGameHandler);
        BtnReJoin.onClick.AddListener(OnReJoinGameHandler);
        BtnThang.onClick.AddListener(OnThangHandler);
        txtStatus.text = "";
    }
    
    private void OnJoinGameHandler()
    {
        GamePacket gp = new GamePacket("getPlayerToPlayboard");
        gp.Put("id", IdCapDau);
        gp.Put("player_id", IDUser);
        gp.Put("is_rejoin", 0);
        SFS.Instance.SendRoomRequest(gp);
    }
    private void OnReJoinGameHandler()
    {
        GamePacket gp = new GamePacket("getPlayerToPlayboard");
        gp.Put("id", IdCapDau);
        gp.Put("player_id", IDUser);
        gp.Put("is_rejoin", 1);
        SFS.Instance.SendRoomRequest(gp);
    }
    private void OnThangHandler()
    {
        GamePacket gp = new GamePacket("setPlayerWin");
        gp.Put("id", IdCapDau);
        gp.Put("player_id", IDUser);
        SFS.Instance.SendRoomRequest(gp);
    }
    public void InitClass(string idCapDau, string idUser, string uname, string winner)
    {
        IdCapDau = idCapDau;
        IDUser = idUser;
        Uname = uname;
        txtName.text = uname;
        if(winner == idUser)
        {
            txtStatus.text = "win";
        }
        
    }
    
}

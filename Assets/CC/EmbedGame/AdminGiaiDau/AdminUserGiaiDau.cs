﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdminUserGiaiDau : MonoBehaviour
{
    [SerializeField]
    ItemUserGiaiDau itemUserGiaiDau;
    
    [SerializeField]
    GameObject contentUserGiaiDau;
    [SerializeField]
    Button btnCloseDuDoan;
    //List<ItemUserThanBai> lstItemUserThanbai;
    
    // Start is called before the first frame update
    void Start()
    {
        btnCloseDuDoan.onClick.AddListener(CloseDuDoanThanBai);
       
        for (int z = 0; z < contentUserGiaiDau.transform.childCount; z++)
        {
            Destroy(contentUserGiaiDau.transform.GetChild(z).gameObject);
        }
        
        GamePacket gp = new GamePacket("getMatchCompetitor");
        SFS.Instance.SendZoneRequest(gp);
    }    
    
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case "getMatchCompetitor":
                ShowListUserThamGia(param);
                break;
            case "getPlayerToPlayboard":
                GetPlayerToPlayboard(param);
                break;
            case "setPlayerWin":
                SetPlayerWin(param);
                break;           
            
        }
    }
    private void SetPlayerWin(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Thành Công");
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }
    private void GetPlayerToPlayboard(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Thành Công");
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }

    private void ShowListUserThamGia(GamePacket param)
    {
        //matchcompetitors
            //users
                //object.putUtfString("id", id.toString());
                //object.putUtfString("player_1_id", player1Id.toString());
                //object.putUtfString("player_1_name", player1Name);
                //object.putUtfString("player_2_id", player1Id.toString());
                //object.putUtfString("player_2_name", player1Name);
                //object.putUtfString("winner", winner != null ? winner.toString() : "");
        //clear old danh sach
        for (int z = 0; z < contentUserGiaiDau.transform.childCount; z++)
        {
            Destroy(contentUserGiaiDau.transform.GetChild(z).gameObject);
        }
        ISFSArray lstUser = param.GetSFSArray("matchcompetitors");

        foreach(ISFSObject capdau in lstUser)
        {            
            ShowUserThamGia(capdau);
        }
    }
    void ShowUserThamGia(ISFSObject user)
    {
       
        ItemUserGiaiDau item = Instantiate(itemUserGiaiDau, contentUserGiaiDau.transform) as ItemUserGiaiDau;
        item.InitClass(user.GetUtfString("id"), user.GetUtfString("player_1_id"), user.GetUtfString("player_1_name"), user.GetUtfString("winner"));
        item.transform.SetAsLastSibling();
        item = Instantiate(itemUserGiaiDau, contentUserGiaiDau.transform) as ItemUserGiaiDau;
        item.InitClass(user.GetUtfString("id"), user.GetUtfString("player_2_id"), user.GetUtfString("player_2_name"), user.GetUtfString("winner"));
        item.transform.SetAsLastSibling();
    }
    
    
    
    
    private void CloseDuDoanThanBai()
    {
        Destroy(gameObject);
    }

    
}

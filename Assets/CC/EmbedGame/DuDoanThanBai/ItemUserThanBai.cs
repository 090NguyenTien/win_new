﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUserThanBai : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI txtName, txttongcuoc, txtSolandoan, txtLoai;
    [SerializeField]
    Button BtnCuoc;
    BaseCallBack.onCallBackString callback;
    string IDUser, Uname; 
    int Solandoan;
    long Tongcuoc;
    // Start is called before the first frame update
    void Start()
    {
        BtnCuoc.onClick.AddListener(OnClickCuocHandler);
        //txtLoai.gameObject.SetActive(false);
    }

    public void InitClass(BaseCallBack.onCallBackString cb, string idUser, string uname, long tongcuoc, int solandoan, int loai, bool showBtnCuoc)
    {
        callback = cb;
        IDUser = idUser;
        txtName.text = Uname = uname;
        
        Tongcuoc = tongcuoc;
        txttongcuoc.text = Utilities.GetStringMoneyByLongBigSmall(tongcuoc);
        Solandoan = solandoan;
        txtSolandoan.text = solandoan.ToString();
        BtnCuoc.gameObject.SetActive(showBtnCuoc);
        if (loai == -1)
        {
            txtLoai.gameObject.SetActive(true);
        }
        else
        {
            txtLoai.gameObject.SetActive(false);
        }
    }
    private void OnClickCuocHandler()
    {
        if(callback!=null) callback(IDUser);
    }
}

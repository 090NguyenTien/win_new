﻿using Sfs2X.Entities.Data;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DangTinController : MonoBehaviour
{
    [SerializeField]
    GameObject ContainerTinDang;
    [SerializeField]
    ItemDangTin itemTinDang;
    int vipRequired, gemRequired, chipRequired;
    [SerializeField]
    InputField txtNoidungtin;
    [SerializeField]
    GameObject PopupTinDang;
    [SerializeField]
    Text chipValue, gemValue;

    int typeDangTin;
    // Start is called before the first frame update
    void Start()
    {
        PopupTinDang.SetActive(false);
        txtNoidungtin.GetComponent<InputField>().characterLimit = 200;
        //get danh sach tin dang
        GamePacket gp = new GamePacket(CommandKey.GET_POST_NEW);
        SFS.Instance.SendZoneRequest(gp);
    }

    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case CommandKey.POST_NEW:
                DangTinRes(param);
                break;
            case CommandKey.GET_POST_NEW:
                GetPostNew(param);
                break;
        }
    }
    private void GetPostNew(GamePacket param)
    {
        for (int z = 0; z < ContainerTinDang.transform.childCount; z++)
        {
            Destroy(ContainerTinDang.transform.GetChild(0).gameObject);
        }
        ISFSArray lstMessage = param.GetSFSArray("Message");
        //created_date, message, vip , username, 
        foreach (ISFSObject Message in lstMessage)
        {
            ItemDangTin itemTin = Instantiate(itemTinDang, ContainerTinDang.transform) as ItemDangTin;
            itemTin.Init(Message.GetUtfString("created_date"), Message.GetUtfString("message"), Message.GetInt("vip"), Message.GetUtfString("username"));
        }
        vipRequired = param.GetInt("vip_required");
        gemRequired = param.GetInt("gem_required");
        chipRequired = param.GetInt("chip_required");

        chipValue.text = Utilities.GetStringMoneyByLong(chipRequired);
        gemValue.text = gemRequired.ToString();
    }
    public void OpenPopupDangTin()
    {
        PopupTinDang.SetActive(true);
        txtNoidungtin.text = "";
    }
    public void ClosePopupDangTin()
    {
        PopupTinDang.SetActive(false);
    }
    public void ClosePanelDangTin()
    {
        //ModuleLoadPrefabScript ModuleParent = gameObject.transform.parent.GetComponent<ModuleLoadPrefabScript>();
        //if (ModuleParent != null)
        //{
        //    ModuleParent.UpdateInfoHome();
        //}        
        Destroy(gameObject);
    }
    public void DangTin(int type)//type : 1 la gem 2 la chip
    {
        typeDangTin = type;
        if (typeDangTin == 2 && MyInfo.CHIP < chipRequired)
        {
            AlertController.api.showAlert("Bạn Không Đủ Chip Để Đăng Tin!");
            return;
        }
        if (typeDangTin == 1 && MyInfo.GEM < gemRequired)
        {
            AlertController.api.showAlert("Bạn Không Đủ Gem Để Đăng Tin!");
            return;
        }
        
        //get danh sach tin dang
        GamePacket gp = new GamePacket(CommandKey.POST_NEW);
        gp.Put("buytype", type);
        gp.Put("Message", txtNoidungtin.text);
        SFS.Instance.SendZoneRequest(gp);
    }
    private void DangTinRes(GamePacket param)
    {
        //ItemTopSuKien itemTop = Instantiate(itemTopSukien, ContainerResult.transform) as ItemTopSuKien;
        int status = param.GetInt("status");
        if (status == -1)
        {
            string msg = param.GetString("Message");
            AlertController.api.showAlert(msg);
            return;
        }
        //Clone Cho nay nha
        int idUserPost = param.GetInt("uid");
        if(idUserPost == MyInfo.SFS_ID)
        {
            AlertController.api.showAlert("Đăng Tin Thành Công!");
        }
        //end
        ISFSObject Message = param.GetSFSObject("Message");
        ItemDangTin itemTin = Instantiate(itemTinDang, ContainerTinDang.transform) as ItemDangTin;
        //int index = itemTin.transform.GetSiblingIndex();
        itemTin.transform.SetSiblingIndex(0);
        itemTin.Init(Message.GetUtfString("created_date"), Message.GetUtfString("message"), Message.GetInt("vip"), Message.GetUtfString("username"));

        if (typeDangTin == 1)
        {
            long gem = param.GetLong("gem");
            MyInfo.GEM = gem;
        }else if (typeDangTin == 2)
        {
            long chip = param.GetLong("chip");
            MyInfo.CHIP = chip;
        }
        ModuleLoadPrefabScript ModuleParent = gameObject.transform.parent.GetComponent<ModuleLoadPrefabScript>();
        if (ModuleParent != null)
        {
            ModuleParent.UpdateInfoHome();
        }
    }
}
